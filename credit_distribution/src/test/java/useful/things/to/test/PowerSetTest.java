package useful.things.to.test;

import java.util.Set;

import org.apache.jena.ext.com.google.common.collect.Sets;

import com.github.jsonldjava.shaded.com.google.common.collect.ImmutableSet;

public class PowerSetTest {
	
	public static void main(String[] args) {
		// test the power set
		ImmutableSet<String> set = ImmutableSet.of("APPLE", "ORANGE", "MANGO");
		Set<Set<String>> powerSet = Sets.powerSet(set);
		for(Set<String> s : powerSet) {
			for(String st: s) {
				System.out.print(st + ",");
			}
			System.out.println();
		}
	}

}
