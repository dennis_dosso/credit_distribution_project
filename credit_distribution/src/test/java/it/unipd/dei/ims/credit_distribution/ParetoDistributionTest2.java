package it.unipd.dei.ims.credit_distribution;

import org.apache.commons.math3.distribution.ParetoDistribution;

/** What I learned with this test:
 * Tge scale brings the first values more on the right. this means that if you put
 * the scale to 1, you will never get values lower than 1, this because the 
 * values lower of 1 will have probability 0. The values closer to 1 will have a very big
 * probability to appear, therefore you will have a lot of values close to 1. 
 * The more you go far away from 1, the lower the probability. So you will see few bigger
 * elements. 
 * 
 * Alpha, the shape of tail parameters, tells you how quickly your probability goes to 
 * 0 going away from the scale. So, the bigger the shape, the quicker you are in the tail. 
 * 
 * */
public class ParetoDistributionTest2 {

	public static void main(String[] args) {
		
		ParetoDistribution distr = new ParetoDistribution(0.9, 0.95);
		
		for (int i = 1; i <= 100; ++i) {
			System.out.print(distr.sample() + "' ");
			if(i % 5 == 0)
				System.out.println();
		}
			
	}
}
