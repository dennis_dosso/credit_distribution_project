package it.unipd.dei.ims.credit_distribution;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import it.unipd.dei.ims.creditdistribution.datastructures.CausalityTuple;
import it.unipd.dei.ims.creditdistribution.provenance.QueryToPolynomialGenerator;

public class ComputeOneShapley {

	public static void main(String[] args) {
		
		Set<String> lineage = new HashSet<String>();
		lineage.add("f1");
		lineage.add("f5");
		lineage.add("c2f1");
		lineage.add("c1");
		lineage.add("c2f2");
		lineage.add("c2f17");
		lineage.add("c2");
		lineage.add("c18");
		
		List<Set<String>> whyProv = new ArrayList<Set<String>>();
		Set<String> witness1 = new HashSet<String>();
		witness1.add("f1");
		witness1.add("c2f1");
		witness1.add("c1");
		whyProv.add(witness1);
		
		Set<String> witness2 = new HashSet<String>();
		witness2.add("f1");
		witness2.add("c2f2");
		witness2.add("c2");
		whyProv.add(witness2);
		
		Set<String> witness3 = new HashSet<String>();
		witness3.add("f5");
		witness3.add("c2f17");
		witness3.add("c18");
		whyProv.add(witness3);
		
		QueryToPolynomialGenerator generator = new QueryToPolynomialGenerator();
		
		List<CausalityTuple> shapleyTuples = generator.computeShapleyValues(lineage, whyProv);
		
		for(CausalityTuple t : shapleyTuples) {
			System.out.println(t.tupleValue + " : " + t.shapleyValue);
		}
		
	}
}
