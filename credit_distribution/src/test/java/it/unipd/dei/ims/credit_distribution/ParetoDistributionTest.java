package it.unipd.dei.ims.credit_distribution;

import java.util.Random;

public class ParetoDistributionTest {

	/* new pseudo random number generator */
	static final Random generator = new Random();

	/* parameters */
	static final double alpha = 3.0;
	static final double c = 3.2;

	public static void main(String[] args) {

		/* number of random numbers to be generated */
		int n = 1000000;

		/* sums */
		double sum1 = 0.0;
		double sum2 = 0.0;

		double[] realise = new double[n];

		for (int i=0; i < n; i++) {
			/* Add up all generated numbers */
			realise[i] = getPareto(alpha,c,generator);
			sum1 += realise[i];
		}

		double samplemean = sum1/(double) n;

		for (int i=0; i < n; i++) {
			/* Add up all generated numbers */
			sum2 += (realise[i] - samplemean) * (realise[i] - samplemean) ;
		}

		double samplevariance = sum2/((double) n + 1.0);

		/* print out the generated variable */
		System.out.println("Xbar in case " + n + ": " + samplemean);
		System.out.println("S^2 in case " + n + ": " + samplevariance);
		System.out.println("Error for Xbar: " + Math.abs(samplemean-getMean(alpha,c)));
		System.out.println("Error for S^2: " + Math.abs(samplevariance-getVariance(alpha,c)));
		for(double i : realise) {
			System.out.println(i);
		}

	}
	/**
	 * This method generates one realization of a Pareto distributed 
	 * random variable.
	 *
	 * @param alpha      the first parameter of the Pareto distribution.
	 * @param c          the second parameter of the Pareto distribution.
	 * @param rgen       the uniform number generator to be used.
	 *
	 * @return          a realization of Pareto-distributed rv.
	 */
	public static double getPareto(double alpha, double c, Random rgen) {

		return c*Math.pow(getUniform(rgen),-1.0/alpha);

	}
	/**
	 * This method generates one realization of a uniformly distributed
	 * random variable within the interval (0,1)
	 *
	 * @param rgen      the random number generator to be used.
	 *
	 * @return          a realization of a uniformly (0,1)-distributed rv.
	 */
	public static double getUniform(Random rgen) {

		/* returns realization of a U(0,1) rv */
		return rgen.nextDouble();
	}

	/**
	 * This method generates one realization of a uniformly distributed
	 * random variable within the interval (min,max)
	 *
	 * @param min       the lower bound of the interval.
	 * @param max       the upper bound of the interval.
	 * @param rgen      the random number generator to be used.
	 *
	 * @return          a realization of a uniformly (0,1)-distributed rv.
	 */
	public static double getUniformInt(double min, double max, Random rgen) {

		/* returns realization of a U(0,1) rv */
		double u = getUniform(rgen);

		/* transformation */
		double value = min + u * (max -min);

		/* deliver */
		return value;
	}
	/**
	 * This method computes the expectation of a Pareto distribution
	 * with parameters <code>alpha</code> and <code>c</code>.g
	 *
	 * @param alpha       the first parameter of the Pareto distribution.
	 * @param c           the second parameter of the Pareto distribution.
	 *
	 * @return            the expectation.
	 */
	public static double getMean(double alpha, double c) {

		/* returns expextation */
		return (alpha*c)/(alpha -1.0);
	}
	/**
	 * This method computes the variance of a Pareto distribution
	 * with parameters <code>alpha</code> and <code>c</code>.g
	 *
	 * @param alpha       the first parameter of the Pareto distribution.
	 * @param c           the second parameter of the Pareto distribution.
	 *
	 * @return            the variance.
	 */
	public static double getVariance(double alpha, double c) {

		/* returns expextation */
		return (alpha*c*c)/((alpha -1.0)*(alpha -1.0)*(alpha -2.0));
	}

}
