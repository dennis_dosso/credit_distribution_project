package it.dei.ims.unipd.creditdistribution.precomputations.scripts;

/** Script to update all the tables with the 3 columns of provenance
 * */
public class UpdateTablesWithColumns {
	
	public static void main(String[] args) {
		UpdateUtilities test = new UpdateUtilities();
		//to add new columns
		test.updateTablesWithCreditColumns();
		System.out.println("All tables updated");
	}

}
