package it.dei.ims.unipd.creditdistribution.rebuttal.shapley;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import it.dei.ims.unipd.creditdistribution.precomputations.scripts.UpdateUtilities;
import it.unipd.dei.ims.creditdistribution.citations.utils.CitationCounter;
import it.unipd.dei.ims.creditdistribution.datastructures.Author;
import it.unipd.dei.ims.creditdistribution.datastructures.CausalityTuple;
import it.unipd.dei.ims.creditdistribution.distributors.CreditDistributor;
import it.unipd.dei.ims.creditdistribution.provenance.QueryToPolynomialGenerator;

public class ReadSyntheticPolynomialsAndDistributeCredit {

	public static void main(String[] args) throws SQLException {

		// VERY IMPORTANT: the quantity of polynomials we generate
		int stopHere = 10000;
		
		// first we reset the database
		UpdateUtilities updateUtilities = new UpdateUtilities();
		String[] tables = {"family", "contributor2family", "contributor"};
		updateUtilities.set0ToTablesAffectedBySyntheticExperiments(tables);

		// path of the file that contains the synthetic polynomials
		String inputCsvFilePath = "/Users/dennisdosso/MEGAsync/Ricerca/progetti_di_ricerca/Credit Distribution/synthetic_polynomials/synthetic_polynomials.csv";

		// read the file and recreate, in-memory, the data structure simulating the provenance polynomials

		List<HashMap<String, String[][]>> polynomialsList = new ArrayList<HashMap<String, String[][]>>();


		try(BufferedReader r = Files.newBufferedReader(Paths.get(inputCsvFilePath))) {
			// each line is a polynomial
			String line = "";
			int queryCounter = 0;
			while((line = r.readLine()) != null) {

				// take the single monomials
				String[] monomials = line.split(",");
				// create the monomial
				String[][] polynomial = new String[monomials.length][];
				// create the map -- originally the map was needed because one String was one query, and a query can have
				// multiple output tuples, and each output tuple has its provenance. In this case we have single polynomials, 
				// so it is like each query has only 1 output tupe with 1 provenance
				HashMap<String, String[][]> polynomialMap = new HashMap<String, String[][]>();

				// now build the monomials making the polynomial
				for(int i = 0; i < monomials.length; ++i) { // for each monomial
					String[] variables = monomials[i].split(" "); // take the variables composing the monomial
					polynomial[i] = variables;
				}
				polynomialMap.put(queryCounter + "", polynomial);
				polynomialsList.add(polynomialMap);
				queryCounter++;
				if(queryCounter == stopHere) 
					break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		// if we are here, we created all polynomials



		// now distributing the credit and count the citations to the authors
		QueryToPolynomialGenerator generator = new QueryToPolynomialGenerator();
		CreditDistributor distributor = new CreditDistributor();

		//maps keeping track of the authors and their citation count
		Map<String, Integer> authorsMap = new HashMap<String, Integer>();
		CitationCounter counter = new CitationCounter();

		int ctr = 0;

		for(HashMap<String, String[][]> polynomials : polynomialsList) {

			distributor.distributeCreditWithHowProvenanceBasicButSmarter(polynomials);


			Map<String, Set<String>> lineageMap = generator.convertPolynomialsToLineage(polynomials);
			distributor.distributeCreditWithLineageBasicButSmarter(lineageMap);

			List<List<Set<String>>> witnessBase = generator.convertPolynomialsToWhyProvenance(polynomials);
			distributor.distributeCreditWithWhyProvenanceBasicButSmarter(witnessBase);


			for(List<Set<String>> whyProv : witnessBase) {
				List<CausalityTuple> responsibleTuples = generator.convertOneWhyProvToOneResponsibilitySet(whyProv);
				generator.computeNormalizedResponsibility(responsibleTuples);
				distributor.distributeCreditViaResponsibility(responsibleTuples);
				distributor.distributeCreditViaNormalizedResponsibility(responsibleTuples);
			}
			
			int i = 0;
			for(Entry<String, Set<String>> linEntry : lineageMap.entrySet()) {
				Set<String> lineage = linEntry.getValue();
				List<Set<String>> whyProv = witnessBase.get(i++);
				List<CausalityTuple> shapleyTuples = generator.computeShapleyValues(lineage, whyProv);
				
				distributor.distributeCreditViaShapleyValue(shapleyTuples);
			}

			//update the citation count for the authors
			counter.countCitationsForAuthorsFromLineage(lineageMap, authorsMap);

			ctr++;
			if(ctr % 100 == 0)
				System.out.println("distributed credit for " + ctr + " polynomials");
		}

		//now that we have citations and credit distributed, we count all of it
		List<Author> authorList = counter.computeTotalCreditForAuthors(authorsMap);

		//now we write this list in a csv file
		String outputPath = counter.getOutputCsvPath() + "/authors_citations_credit.csv";
		Path p = Paths.get(outputPath);
		try(BufferedWriter w = Files.newBufferedWriter(p)){
			// header of the csv
			w.write("author_id,citation_count,lineage,why,how,responsibility,normalized_responsibility,shapley");
			w.newLine();

			for(Author a : authorList) {
				w.write(a.getName() + "," 
						+ a.getCitationCount() 
						+ "," + a.getLineageCredit() 
						+ "," + a.getWhyCredit() 
						+ "," + a.getHowCredit() 
						+ "," + a.getResponsibility() 
						+ "," + a.getNormalizedResponsibility()
						+ "," + a.getShapleyValue());
				w.newLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		distributor.shutDown();

		System.out.println("done");

	}
}
