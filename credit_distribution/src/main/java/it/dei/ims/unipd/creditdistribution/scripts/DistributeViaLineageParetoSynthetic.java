package it.dei.ims.unipd.creditdistribution.scripts;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ext.com.google.common.base.Stopwatch;

import it.unipd.dei.ims.creditdistribution.distributors.CreditDistributor;
import it.unipd.dei.ims.creditdistribution.distributors.DistributionsToolkit;
import it.unipd.dei.ims.creditdistribution.provenance.QueryToPolynomialGenerator;
import it.unipd.dei.ims.rum.utilities.LogManagement;


/** Script to distribute, using the lineage strategy, and counting
 * also more than one citation through a synthetic Pareto distribution.
 * */
public class DistributeViaLineageParetoSynthetic {
	
	public static void main(String[] args) throws IOException {
		System.out.println("starting the redistribution of credit");
		//create the generator
		QueryToPolynomialGenerator generator = new QueryToPolynomialGenerator();
		System.out.println("using query file at path: " + generator.getQueryFile());

		//compute the provenance polynomials for every query in the textual file
		List<HashMap<String, String[][]>> polynomialsList = generator.generateHowProvenancePolynomials(null);
		//reorder this list to get queries grouped via webpage
		DistributionsToolkit toolkit = new DistributionsToolkit();
		polynomialsList = toolkit.reorderQueriesInList(polynomialsList, null);
		
		//get the number of queries making up a webpage
		int webPageQueryNumber = toolkit.getWebPageQueryNumber();
		
		//get the number of pages in this list of queries
		int numberOfWebpages = polynomialsList.size() / webPageQueryNumber;
		
		//get a list of pareto-distributed times we queries each webpage
		List<Integer> samples = toolkit.generateTheseManySamplesFromAParetoDistribution(numberOfWebpages, null, null);
		
		
		//object which performs the distribution
		CreditDistributor distributor = new CreditDistributor();
		int queryCounter = 0;

		//file where we write the times
		String outputDirectory = distributor.getOutputLogDir();
		String fileName = "lineage_" + distributor.getDatabase();
		BufferedWriter writer = LogManagement.getAWriterWithTimestamp(outputDirectory, fileName, "csv");
		
		//we write a csv
		writer.write("QUERY NUM, TIME, # OF CITATIONS");
		writer.newLine();
		
		//for every one of the webpages/group of queries
		for(int i = 0; i < numberOfWebpages; ++i) {
			//we are inside one webpage now 
			
			//number of times this webpage has been cited
			int citations = samples.get(i);
			for(int j = 0; j < webPageQueryNumber; ++j) {
				//get the lineage of this webpage
				HashMap<String, String[][]> polynomials = polynomialsList.get(i * webPageQueryNumber + j);
				
				Map<String, Set<String>> lineageMap = generator.convertPolynomialsToLineage(polynomials);
				//distribute with lineage
				Stopwatch lineageTimer = Stopwatch.createStarted();
				
				for(int k = 0; k < citations; ++k) {
					//distribute the credit as many times needed by the citation count
					distributor.distributeCreditWithLineageBasicButSmarter(lineageMap);					
				}
				
				//write the results
				String log = (queryCounter++) + "," + lineageTimer.stop() + "," + citations;
				writer.write(log);
				writer.newLine();
				System.out.println(log);
			}
		}

		distributor.shutDown();
		writer.close();
		
		System.out.println("done");
		//TODO adesso qui stai facendo una query per volta, ed hai fatto una lista di set di polinomi. 
		//una possibilità è fare un set unico di polinomi così processi tutte le query 'assieme'
	}

}
