package it.dei.ims.unipd.creditdistribution.rebuttal.shapley;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import it.unipd.dei.ims.creditdistribution.datastructures.CausalityTuple;
import it.unipd.dei.ims.creditdistribution.distributors.CreditDistributor;
import it.unipd.dei.ims.creditdistribution.provenance.QueryToPolynomialGenerator;

public class DistributeViaShapley {

	public static void main(String[] args) {
		System.out.println("The credit distribution process via Shapley started...");
		
		// initialize the object containing the necessary variables taken from the properties file
		QueryToPolynomialGenerator generator = new QueryToPolynomialGenerator();
		
		// compute the how-provenance polynomials from the queries in the files
		List<HashMap<String, String[][]>> polynomialsList = generator.generateHowProvenancePolynomials(null);
		
		// now that we have the polynomials, distribute the credit using the different provenances
		CreditDistributor distributor = new CreditDistributor();
		
		int c = 0;
		
		for(HashMap<String, String[][]> polynomials : polynomialsList) {
			// first, we need to generate lineage and why provenance
			Map<String, Set<String>> lineageMap = generator.convertPolynomialsToLineage(polynomials);
			List<List<Set<String>>> witnessBases = generator.convertPolynomialsToWhyProvenance(polynomials);
			
			int i = 0;
			for(Entry<String, Set<String>> linEntry : lineageMap.entrySet()) {
				Set<String> lineage = linEntry.getValue();
				List<Set<String>> whyProv = witnessBases.get(i++);
				List<CausalityTuple> shapleyTuples = generator.computeShapleyValues(lineage, whyProv);
				generator.computeNormalizedShapleyValue(shapleyTuples);
				
				distributor.distributeCreditViaShapleyValue(shapleyTuples);
				distributor.distributeCreditViaNormalizedShapleyValue(shapleyTuples);
			}
			
			
			c++;
			if(c % 1000 == 0)
				System.out.println("distributed credit of  " + c + " queries out of " + polynomialsList.size());
		}
		
		distributor.shutDown();
		System.out.println("done");
		
	}
}
