package it.dei.ims.unipd.creditdistribution.scripts;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import it.unipd.dei.ims.creditdistribution.distributors.CreditDistributor;
import it.unipd.dei.ims.creditdistribution.provenance.QueryToPolynomialGenerator;
import it.unipd.dei.ims.rum.utilities.LogManagement;

public class DistributeAttributeWhyProvenance {

	public static void main(String[] args) throws IOException {
		System.out.println("starting the redistribution of credit");
		//create the generator
		QueryToPolynomialGenerator generator = new QueryToPolynomialGenerator();
		System.out.println("using query file at path: " + generator.getQueryFile());

		//compute the provenance polynomials
		List<HashMap<String, String[][]>> polynomialsList = generator.generateHowProvenancePolynomials(null);

		CreditDistributor distributor = new CreditDistributor();
		int queryCounter = 0;

		//file where we write the times
		String outputDirectory = distributor.getOutputLogDir();
		String fileName = "why_provenance_" + distributor.getDatabase();
		BufferedWriter writer = LogManagement.getAWriterWithTimestamp(outputDirectory, fileName, "csv");
		//we write a csv
		writer.write("QUERY NO., TIME");
		writer.newLine();

		//open the path to the query file, we need the queries to deduce the attribute why provenance
		Path path = Paths.get(generator.getQueryFile());
		BufferedReader reader = Files.newBufferedReader(path);
		String query = "";

		for(HashMap<String, String[][]> polynomials : polynomialsList) {
			//read the query
			query = reader.readLine();
			//compute the why provenances
			List<List<Set<String>>> whitnessBase = generator.convertPolynomialsToWhyProvenance(polynomials);
			List<List<Set<String>>> attrWhitnessBases = generator.convertWhyProvenanceInAttributeWhyProvenance(whitnessBase, query);
		}

	}

}
