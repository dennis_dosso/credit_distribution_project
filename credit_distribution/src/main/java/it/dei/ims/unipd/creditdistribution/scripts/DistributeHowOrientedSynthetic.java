package it.dei.ims.unipd.creditdistribution.scripts;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import it.unipd.dei.ims.creditdistribution.distributors.CreditDistributor;
import it.unipd.dei.ims.creditdistribution.provenance.QueryToPolynomialGenerator;

/** Here I got fed up to find out sensible queries and started to build 
 * polynomials with my own hands. 
 * Let's get dirty.
 * <p>
 * First of all, I perform a query like this:
 * 
 * select distinct f.family_id, f."c||prov" as f_prov, cf."c||prov" as cf_prov, c."c||prov" as c_prov
from "family" f join contributor2family cf on f.family_id = cf.family_id join contributor c on cf.contributor_id = c.contributor_id 
where c.country = 'UK'

 in this way I get a csv file with the variables used by the same join query.


 * */
public class DistributeHowOrientedSynthetic {


	/** this method produces some polynomials by taking some ids from a csv file and
	 * randomly combining them in provenance polynomials
	 * 
	 * */
	public static List<HashMap<String, String[][]>> produceSyntheticPolynomialsSolution1() throws IOException {
		//now we read the polynomials
		//yeah, the path is hardcoded, sorry 
		String uri = "/Users/dennisdosso/MEGAsync/Ricerca/PROGETTI/Credit Distribution/iuphar/database_csv/_select_distinct_f_family_id_f_c_prov_as_f_prov_cf_c_prov_as_cf__202005160237.csv";
		Path p = Paths.get(uri);
		BufferedReader reader = Files.newBufferedReader(p);

		int num_of_tuples = 0;
		String line = "";

		Random rand = new Random();
		List<HashMap<String, String[][]>> polynomialsList = new ArrayList<HashMap<String, String[][]>>();

		//the first line is the header
		line = reader.readLine();

		while((line = reader.readLine()) != null) {
			HashMap<String, String[][]> map = new HashMap<String, String[][]>();

			//the variables that I read from the csv file
			String[] variables = line.split(",");
			String variable_1 = variables[1];
			String variable_2 = variables[2];
			String variable_3 = variables[3];
			//now we have our variables. We get to produce our monomials and polynomials

			int numberOfMonomials = 1 + rand.nextInt(6);

			String[][] polynomial = new String[numberOfMonomials][];
			String[] monomial;

			for(int j = 0; j < numberOfMonomials; ) {

				//I am inside a monomial now
				//decide how many times a variable appears (exponent)
				int times_var_1 = 1 + rand.nextInt(4);
				int times_var_2 = 1 + rand.nextInt(4);
				int times_var_3 = 1 + rand.nextInt(4);

				monomial = new String[times_var_1 + times_var_2 + times_var_3];
				int i = 0;

				//populate the monomial
				for(int k = 0; k < times_var_1; k++) {
					monomial[i] = variable_1;
					i++;
				}

				for(int k = 0; k < times_var_2; k++) {
					monomial[i] = variable_2;
					i++;
				}

				for(int k = 0; k < times_var_3; k++) {
					monomial[i] = variable_3;
					i++;
				}

				//save the monomial
				int repeat_the_monomial = 1 + rand.nextInt(3);
				for(int k = 0; k < repeat_the_monomial; ++k) {
					polynomial[j] = monomial;
					j++;
					if(j == numberOfMonomials)
						break;

				}
			}
			//if we are here, we have our polynomial
			map.put(num_of_tuples + "", polynomial);
			num_of_tuples++;


			polynomialsList.add(map);
		}

		reader.close();
		return polynomialsList;
	}


	/** A second strategy because I wanted to be a little more naughty.
	 * <p>
	 * This is the solution that I used to produce the polynomials 
	 * that I used in my thesis. I only now realize that my thesis is therefore
	 * very naughty, but it is too late to turn back now!
	 * 
	 * */
	public static List<HashMap<String, String[][]>> produceSyntheticPolynomialsSolution2() throws IOException {
		Random rand = new Random();
		List<HashMap<String, String[][]>> polynomialsList = new ArrayList<HashMap<String, String[][]>>();

		//a counter
		int num_of_tuples = 0;

		for(int n = 0; n < 100; ++n) {
			//here I decided to produce 100 polynomials

			//create the polynomial
			HashMap<String, String[][]> map = new HashMap<String, String[][]>();

			// in table family there are 796 different tuples
			String variable_1 = "family|" + rand.nextInt(796);
			

			int numberOfMonomials = 1 + rand.nextInt(6);

			String[][] polynomial = new String[numberOfMonomials][];
			String[] monomial;

			for(int j = 0; j < numberOfMonomials; ) {
				
				//every time I change variables 2 and 3
				String variable_2 = "contributor2family|" + rand.nextInt(973);
				String variable_3 = "contributor|" + rand.nextInt(1146);

				//I am inside a monomial now
				//decide how many times a variable appears (exponent)
				int times_var_1 = 1 + rand.nextInt(4);
				int times_var_2 = 1 + rand.nextInt(4);
				int times_var_3 = 1 + rand.nextInt(4);

				monomial = new String[times_var_1 + times_var_2 + times_var_3];
				int i = 0;

				//populate the monomial
				for(int k = 0; k < times_var_1; k++) {
					monomial[i] = variable_1;
					i++;
				}

				for(int k = 0; k < times_var_2; k++) {
					monomial[i] = variable_2;
					i++;
				}

				for(int k = 0; k < times_var_3; k++) {
					monomial[i] = variable_3;
					i++;
				}

				//save the monomial
				int repeat_the_monomial = 1 + rand.nextInt(3);
				for(int k = 0; k < repeat_the_monomial; ++k) {
					polynomial[j] = monomial;
					j++;
					if(j == numberOfMonomials)
						break;

				}
			}
			//if we are here, we have our polynomial
			map.put(num_of_tuples + "", polynomial);
			num_of_tuples++;


			polynomialsList.add(map);
		}
		
		return polynomialsList;

	}







public static void main(String[] args) throws IOException {
	//create the generator
	QueryToPolynomialGenerator generator = new QueryToPolynomialGenerator();
	System.out.println("using query file at path: " + generator.getQueryFile());

	List<HashMap<String, String[][]>> polynomialsList = produceSyntheticPolynomialsSolution2();



	CreditDistributor distributor = new CreditDistributor();
	int counter = 0;

	for(HashMap<String, String[][]> polynomials : polynomialsList) {

		System.out.println("Distributing via how provenance");
		distributor.distributeCreditWithHowProvenanceBasicButSmarter(polynomials);

		//now distribute with lineage
		Map<String, Set<String>> lineageMap = generator.convertPolynomialsToLineage(polynomials);
		distributor.distributeCreditWithLineageBasicButSmarter(lineageMap);

		List<List<Set<String>>> whitnessBase = generator.convertPolynomialsToWhyProvenance(polynomials);
		distributor.distributeCreditWithWhyProvenanceBasicButSmarter(whitnessBase);
		counter++;
	}


	distributor.shutDown();


	System.out.println("done");



}

}
