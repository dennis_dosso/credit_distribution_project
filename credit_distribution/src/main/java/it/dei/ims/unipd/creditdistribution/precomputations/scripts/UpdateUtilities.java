package it.dei.ims.unipd.creditdistribution.precomputations.scripts;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ThreadLocalRandom;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** Container of useful methods for the update of tables in the context of the credit distribution project.
 * <p>
 * For now the operations hosted here are:
 * 	<ul>
 * 		<li> Addition of 3 double columns for the credit (lineage, why, how) to every table in the indicated database 
 *			(the property database is pecified in the properties file  main.properties)</li>
 *		<li>Addition of two columns about the responsibility (resp) and normalized responsibility (norm_resp) to the tables</li>
 *		<li> Setting to 0 all the credit columns, so you can start your experiments from scratch. (VERY IMPORTANT!!!)
 *  </ul>
 * <p>
 * The columns are actually 3, one for each type of distribution strategy
 * 
 * 
 * @author Dennis D.
 * 
 * */
public class UpdateUtilities {

	/** String to connect to the database */
	private String jdbcString;

	/** Map used to deal with the properties/fields of this class */
	protected Map<String, String> map;

	/** Standard query to obtain all the names of the tables 
	 * (only the ones you created, not the default ones from the system)
	 * and only the tables (the second condition avoid to include the views in the database)
	 * */
	private final String SQL_GET_ALL_TABLES_NAMES = "select table_name from information_schema.tables \n" + 
			"where table_schema=\'public\'\n" + 
			"and \n" + 
			"table_type=\'BASE TABLE\'";

	private String SQL_ADD_COLUMN = "ALTER TABLE ? ADD COLUMN CREDIT real DEFAULT 0";
	private String SQL_UPDATE_TO_0 = "UPDATE replace_here SET lineage_credit = 0, why_credit = 0, how_credit = 0, resp = 0, norm_resp = 0, shapley = 0;";

	private String SQL_ADD_COLUMNS = "ALTER TABLE replace_here ADD COLUMN LINEAGE_CREDIT real DEFAULT 0, "
			+ "ADD COLUMN WHY_CREDIT real DEFAULT 0, "
			+ "ADD COLUMN HOW_CREDIT real DEFAULT 0;";

	private String SQL_ADD_AUTHOR = "ALTER TABLE replace_here "
			+ "ADD COLUMN _author_ varchar(5)";

	private String SQL_ADD_RESP = "ALTER TABLE replace_here "
			+ "ADD COLUMN RESP real DEFAULT 0, "
			+ "ADD COLUMN NORM_RESP real DEFAULT 0;";
	
	private String SQL_ADD_SHAPLEY = "ALTER TABLE replace_here "
			+ "ADD COLUMN SHAPLEY real DEFAULT 0, "
			+ "ADD COLUMN NORM_SHAPLEY real DEFAULT 0;";

	/**String to get all the ids in a table, used to update all the tuples in a table*/
	private String SQL_GET_TUPLES = "SELECT \"c||prov\" FROM replace_here";

	private String SQL_UPDATE_AUTHOR = "UPDATE replace_here "
			+ "SET _author_ = ? "
			+ "WHERE \"c||prov\" = ?";

	private String host;
	private String port;
	private String database;
	private String user;
	private String password;

	public UpdateUtilities() {
		//get the map
		map = PropertiesUsefulMethods.getPropertyMap("properties/main.properties");

		this.host = map.get("host");
		this.port=map.get("port");
		this.database=map.get("database");
		this.user=map.get("user");
		this.password=map.get("password");


		jdbcString = "jdbc:postgresql://" + this.host + ":"+ this.port + "/" + this.database + "?user=" + this.user + "&password=" + this.password;
		//this.jdbcString = map.get("jdbc.string");
	}

	/** Given the database in the jdbc string, it updates every table 
	 * adding a column 'credit' which is a real number with default value 0
	 * 
	 * This method is sort of deprecated, since now we use many columns
	 * */
	@Deprecated
	public void updateTablesWithCreditColumn() {

		//open connection to the database
		Properties props = new Properties();
		props.setProperty("prepareThreshold", "10");
		try {
			Connection c = DriverManager
					.getConnection(this.jdbcString, props);


			//get the list of all the tables
			PreparedStatement ps = c.prepareStatement(SQL_GET_ALL_TABLES_NAMES);
			ResultSet list = ps.executeQuery();

			//now, for every table in the database
			while(list.next()) {
				String tableName = list.getString(1);

				String add = SQL_ADD_COLUMN.replaceAll("\\?", tableName);

				PreparedStatement updateStmt = c.prepareStatement(add);
				try {
					updateStmt.execute();
					System.out.println(tableName + " updated");
				} catch (SQLException e) {
					System.err.println("table " + tableName + " rejected the update. Probably it already has a column called 'credit'");
				}
			}

			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}

		System.out.println("All tables have been updated with the columns 'credit' ");

	}

	/** Updates all the tables in the database by adding a column resp (credit given through responsibility) and
	 * norm_resp (normalized credit given through responsibility) of type double*/
	public void updateTablesAddResponsiblity() {
		Properties props = new Properties();
		props.setProperty("prepareThreshold", "10");


		//open connection to the database
		Connection c;
		try {
			c = DriverManager
					.getConnection(this.jdbcString, props);

			//get the list of all the tables
			PreparedStatement ps = c.prepareStatement(SQL_GET_ALL_TABLES_NAMES);
			ResultSet list = ps.executeQuery();

			//now, for every table in the database
			while(list.next()) {
				String tableName = list.getString(1);
				System.out.println("Now adding responsability to table " + tableName + " ...");

				String add = SQL_ADD_RESP.replaceAll("replace_here", tableName);

				PreparedStatement updateStmt = c.prepareStatement(add);

				try {
					updateStmt.execute();
					System.out.println(tableName + " updated");
				} catch (SQLException e) {
					System.err.println("table " + tableName + " rejected the update. Probably it already has a column that you are trying to insert");
					e.printStackTrace();
				}

				updateStmt.close();
			}
			c.close();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void updateTablesAddShapleyValue() {
		Properties props = new Properties();
		props.setProperty("prepareThreshold", "10");


		//open connection to the database
		Connection c;
		try {
			c = DriverManager
					.getConnection(this.jdbcString, props);

			//get the list of all the tables
			PreparedStatement ps = c.prepareStatement(SQL_GET_ALL_TABLES_NAMES);
			ResultSet list = ps.executeQuery();

			//now, for every table in the database
			while(list.next()) {
				String tableName = list.getString(1);
				System.out.println("Now adding responsability to table " + tableName + " ...");

				String add = SQL_ADD_SHAPLEY.replaceAll("replace_here", tableName);

				PreparedStatement updateStmt = c.prepareStatement(add);

				try {
					updateStmt.execute();
					System.out.println(tableName + " updated");
				} catch (SQLException e) {
					System.err.println("table " + tableName + " rejected the update. Probably it already has a column that you are trying to insert");
					e.printStackTrace();
				}

				updateStmt.close();
			}
			c.close();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}



	/** Updates all the tables in the database by adding a column
	 * _authors_, of type text */
	public void updateTablesAddAuthorsColumns() {
		Properties props = new Properties();
		props.setProperty("prepareThreshold", "10");

		try {
			//open connection to the database
			Connection c = DriverManager
					.getConnection(this.jdbcString, props);

			//get the list of all the tables
			PreparedStatement ps = c.prepareStatement(SQL_GET_ALL_TABLES_NAMES);
			ResultSet list = ps.executeQuery();

			//now, for every table in the database
			while(list.next()) {
				String tableName = list.getString(1);
				System.out.println("Now updating table " + tableName + " ...");

				String add = SQL_ADD_AUTHOR.replaceAll("replace_here", tableName);

				PreparedStatement updateStmt = c.prepareStatement(add);

				try {
					updateStmt.execute();
					System.out.println(tableName + " updated");
				} catch (SQLException e) {
					System.err.println("table " + tableName + " rejected the update. Probably it already has a column called 'credit'");
				}
			}
			c.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**Updates the _author_ column in each table of the database
	 * with an author randomly chosen between a pool of candidates. 
	 * It creates blocks/clusters of tuples connected to the same author. 
	 * 
	 * @param authorNumber the number of different authors that we are using. The 
	 * authors are then chosen randomly from the pool identified by this number
	 * @param maxBlockLength the max length of a block of tuples authored by the same person.
	 * E.g. if it is 10, then blocks of 10 tuples will be authored by the same author
	 * The length of each block is randomly generated. It has to be bigger than 1 otherwise 
	 * we have some problems here, do not hate me but I hate to keep track of special cases.
	 * */
	public void updateTablesAddRandomAuthors(int authorNumber, int maxBlockLength) {
		Properties props = new Properties();
		props.setProperty("prepareThreshold", "10");

		int authorCounter = 0, blockLength = 0;
		int counter = 0;

		try {
			//open connection to the database
			Connection c = DriverManager
					.getConnection(this.jdbcString, props);

			//get the list of all the tables
			PreparedStatement ps = c.prepareStatement(SQL_GET_ALL_TABLES_NAMES);
			ResultSet list = ps.executeQuery();

			//now, for every table in the database
			while(list.next()) {
				//decide the length of this block
				blockLength = ThreadLocalRandom.current().nextInt(1, maxBlockLength + 1);
				authorCounter = ThreadLocalRandom.current().nextInt(0, authorNumber);
				//get the name
				String tableName = list.getString(1);
				System.out.println("Now adding authors to table " + tableName + " ...");

				//get all the tuples, one at the time
				String get_tuples = SQL_GET_TUPLES.replaceAll("replace_here", tableName);
				PreparedStatement getTuples = c.prepareStatement(get_tuples);
				ResultSet tuples = getTuples.executeQuery();
				while(tuples.next()) {
					//if it is necessary, update the author that we are using now and the 
					//length of the next block
					if(counter > blockLength) {
						//update the block length and the next author
						counter = 0;
						blockLength = ThreadLocalRandom.current().nextInt(0, maxBlockLength + 1);
						authorCounter = ThreadLocalRandom.current().nextInt(0, authorNumber);
					}

					//for each tuple
					String tupleId = tuples.getString(1);
					//now update the uple with its author
					String update = SQL_UPDATE_AUTHOR.replaceAll("replace_here", tableName);
					PreparedStatement updateStmt = c.prepareStatement(update);
					updateStmt.setString(1, authorCounter + "");//set the author
					updateStmt.setString(2, tupleId);//set the id of the tuple we are updating
					//perform the update
					updateStmt.execute();
					counter++;
				}
			}
			c.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	/** Given the database in the jdbc string, it updates every table 
	 * adding THREE attributes, one for each different type of way to compute 
	 * the credit: through lineage, why provenance e how provenance.
	 * 
	 * 
	 * */
	public void updateTablesWithCreditColumns() {

		//open connection to the database
		Properties props = new Properties();
		props.setProperty("prepareThreshold", "10");
		try {
			Connection c = DriverManager
					.getConnection(this.jdbcString, props);


			//get the list of all the tables
			PreparedStatement ps = c.prepareStatement(SQL_GET_ALL_TABLES_NAMES);
			ResultSet list = ps.executeQuery();

			//now, for every table in the database
			while(list.next()) {
				String tableName = list.getString(1);
				System.out.println("Now updating table " + tableName + " ...");

				//prepare the SQL string to update the database with the name of the table
				String add = SQL_ADD_COLUMNS.replaceAll("replace_here", tableName);

				PreparedStatement updateStmt = c.prepareStatement(add);
				try {
					updateStmt.execute();
					System.out.println(tableName + " updated");
				} catch (SQLException e) {
					System.err.println("table " + tableName + " rejected the update. Probably it already has a column called 'credit'");
				}
			}

			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}

		System.out.println("All tables have been updated with the credit columns ");

	}


	/** Sets the credit in all the tables of the database to 0. i.e. it updates
	 * all the credit columns to 0 as value
	 * */
	public void setBlankStateOnColumns() {
		//open connection to the database
		Properties props = new Properties();
		props.setProperty("prepareThreshold", "10");
		try {
			//open the connection
			Connection c = DriverManager
					.getConnection(this.jdbcString, props);


			//get the list of all the tables
			PreparedStatement ps = c.prepareStatement(SQL_GET_ALL_TABLES_NAMES);
			ResultSet list = ps.executeQuery();

			//now, for every table in the database
			while(list.next()) {
				String tableName = list.getString(1);

				String add = SQL_UPDATE_TO_0.replaceAll("replace_here", tableName);

				PreparedStatement updateStmt = c.prepareStatement(add);
				try {
					updateStmt.execute();
				} catch (SQLException e) {
					System.err.println("table " + tableName + " rejected the update. Probably it does not have the credit columns");
				}
			}

			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}

	}

	public void set0ToTablesAffectedBySyntheticExperiments(String[] tables) {
		/**
		 * Sets to 0 the credit of the tables indicated in the argument. 
		 * */
		Properties props = new Properties();
		props.setProperty("prepareThreshold", "10");
		try {
			//open the connection
			Connection c = DriverManager
					.getConnection(this.jdbcString, props);


			//now, for every table in the database
			for( String tableName : tables) {

				String add = SQL_UPDATE_TO_0.replaceAll("replace_here", tableName);

				PreparedStatement updateStmt = c.prepareStatement(add);
				try {
					updateStmt.execute();
				} catch (SQLException e) {
					System.err.println("table " + tableName + " rejected the update. Probably it does not have the credit columns");
				}
			}

			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}


	public static void main(String[] args) {
		UpdateUtilities execution = new UpdateUtilities();
		//to add new columns
//		execution.updateTablesWithCreditColumns();
//		execution.updateTablesAddResponsiblity();
		execution.updateTablesAddShapleyValue();
		
		System.out.println("All tables updated");
		//		execution.setBlankStateOnColumns();
		//		System.out.println("All tables cleaned");
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}


}
