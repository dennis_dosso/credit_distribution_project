package it.dei.ims.unipd.creditdistribution.scripts;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import it.unipd.dei.ims.creditdistribution.distributors.CreditDistributor;
import it.unipd.dei.ims.creditdistribution.provenance.QueryToPolynomialGenerator;
import it.unipd.dei.ims.rum.utilities.LogManagement;

public class DistributeAttributeLineage {

	/** We distribute all the queries contained in a query file via attribute lineage*/
	public static void main(String[] args) throws IOException {
		System.out.println("starting the redistribution of credit via attribute lineage");
		
		QueryToPolynomialGenerator generator = new QueryToPolynomialGenerator();
		System.out.println("using query file at path: " + generator.getQueryFile());
		
		//compute the provenance polynomials for every query in the textual file
		List<HashMap<String, String[][]>> polynomialsList = generator.generateHowProvenancePolynomials(null);
		
		//object which performs the distribution
		CreditDistributor distributor = new CreditDistributor();
		int queryCounter = 0;

		//file where we write the times
		String outputDirectory = distributor.getOutputLogDir();
		String fileName = "lineage_" + distributor.getDatabase();
		BufferedWriter writer = LogManagement.getAWriterWithTimestamp(outputDirectory, fileName, "csv");
		
		//we write a csv
		writer.write("QUERY NO, TIME");
		writer.newLine();
		
		//open the path to the query file, we need the queries
		Path path = Paths.get(generator.getQueryFile());
		BufferedReader reader = Files.newBufferedReader(path);
		String query = "";
		for(HashMap<String, String[][]> polynomials : polynomialsList) {//for every (provenance of) every query
			//read the query
			query = reader.readLine();
			//use the polinomial to obtain the tuple lineages 
			Map<String, Set<String>> lineageMap = generator.convertPolynomialsToLineage(polynomials);
			//use the map and the query to get the attribute lineage
			List<Set<String>> attributeLineages = generator.convertTupleLineageToAttributeLineage(lineageMap, query);
			System.out.println(attributeLineages);
		}
		
	}
}
