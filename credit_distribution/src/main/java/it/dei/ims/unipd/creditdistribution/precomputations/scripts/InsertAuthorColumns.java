package it.dei.ims.unipd.creditdistribution.precomputations.scripts;

/** It only adds the authors column in each table*/
public class InsertAuthorColumns {

	public static void main(String[] args) {
		UpdateUtilities util = new UpdateUtilities();
		util.updateTablesAddAuthorsColumns();
		System.out.println("Column _author_ added to the tbales in the database " + util.getDatabase());
	}

}
