package it.dei.ims.unipd.creditdistribution.precomputations.scripts;

/**use this class to 'clean' the tables of your database,
 * that is, to set 0 as value of every attribute column.
 * <p>
 * Remember that you select your database in the main.properties file.
 * Remember that the schema where you are working is always 'public'
 * */
public class Set0ToAllCredit {

	public static void main(String[] args) {
			System.out.println("Cleaning, please stand by...");
			UpdateUtilities test = new UpdateUtilities();
			test.setBlankStateOnColumns();
			System.out.println("All credit tables of database " + test.getDatabase() + " cleaned");
	}
}
