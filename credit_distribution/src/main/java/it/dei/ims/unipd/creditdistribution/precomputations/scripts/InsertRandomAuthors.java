package it.dei.ims.unipd.creditdistribution.precomputations.scripts;

/**Once you have created the column authors, insert random authors
 * in it
 * */
public class InsertRandomAuthors {

	public static void main(String[] args) {
		UpdateUtilities test = new UpdateUtilities();
		//20 different authors, block of maximum 40 tuple
		test.updateTablesAddRandomAuthors(20, 40);
		System.out.println("All tuples in database " + test.getDatabase() + " received an author");
	}
}
