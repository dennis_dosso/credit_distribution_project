package it.dei.ims.unipd.creditdistribution.scripts;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.apache.jena.ext.com.google.common.base.Stopwatch;

import it.unipd.dei.ims.creditdistribution.distributors.CreditDistributor;
import it.unipd.dei.ims.creditdistribution.provenance.QueryToPolynomialGenerator;
import it.unipd.dei.ims.rum.utilities.LogManagement;

public class DistributeViaWhyProvenance {

	public static void main2(String[] args) {
		System.out.println("starting the redistribution of credit");
		//create the generator
		QueryToPolynomialGenerator generator = new QueryToPolynomialGenerator();
		System.out.println("using query file at path: " + generator.getQueryFile());

		//compute the provenance polynomials
		HashMap<String, String[][]> polynomials = generator.generateHowProvenancePolynomial(null);

		CreditDistributor distributor = new CreditDistributor();



		////////////////////////// WHY PROVENANCE ////////////////////
		List<List<Set<String>>> whitnessBase = generator.convertPolynomialsToWhyProvenance(polynomials);
		distributor.distributeCreditWithWhyProvenanceBasicButSmarter(whitnessBase);


		distributor.shutDown();
	}

	/** In this main, we deal with the the problem of multiple queries with a list of 
	 * provenances
	 * @throws IOException 
	 * */
	public static void main(String[] args) throws IOException {
		System.out.println("starting the redistribution of credit");
		//create the generator
		QueryToPolynomialGenerator generator = new QueryToPolynomialGenerator();
		System.out.println("using query file at path: " + generator.getQueryFile());

		//compute the provenance polynomials
		List<HashMap<String, String[][]>> polynomialsList = generator.generateHowProvenancePolynomials(null);

		CreditDistributor distributor = new CreditDistributor();
		int queryCounter = 0;

		//file where we write the times
		String outputDirectory = distributor.getOutputLogDir();
		String fileName = "why_provenance_" + distributor.getDatabase();
		BufferedWriter writer = LogManagement.getAWriterWithTimestamp(outputDirectory, fileName, "csv");
		//we write a csv
		writer.write("QUERY NO., TIME");
		writer.newLine();

		for(HashMap<String, String[][]> polynomials : polynomialsList) {
			//compute the why-provenances of the output tuples 
			List<List<Set<String>>> whitnessBase = generator.convertPolynomialsToWhyProvenance(polynomials);
			//distribute with lineage
			Stopwatch lineageTimer = Stopwatch.createStarted();
			distributor.distributeCreditWithWhyProvenanceBasicButSmarter(whitnessBase);


			//log string
			String log = (queryCounter++) + "," + lineageTimer.stop();
			writer.write(log);
			writer.newLine();
			System.out.println(log);
		}

		distributor.shutDown();
		writer.close();
		System.out.println("done");
		//TODO adesso qui stai facendo una query per volta, ed hai fatto una lista di set di polinomi. 
		//una possibilità è fare un set unico di polinomi così processi tutte le query 'assieme'
	}
}
