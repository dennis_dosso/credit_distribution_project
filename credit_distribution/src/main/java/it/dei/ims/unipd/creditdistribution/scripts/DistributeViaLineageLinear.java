package it.dei.ims.unipd.creditdistribution.scripts;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ext.com.google.common.base.Stopwatch;

import it.unipd.dei.ims.creditdistribution.distributors.CreditDistributor;
import it.unipd.dei.ims.creditdistribution.provenance.QueryToPolynomialGenerator;
import it.unipd.dei.ims.rum.utilities.LogManagement;

/** Script to distribute the credit of a set of queries contained in 
 * the query_file property
 * */
public class DistributeViaLineageLinear {

	public static void main_(String[] args) {
		System.out.println("starting the redistribution of credit");
		//create the generator
		QueryToPolynomialGenerator generator = new QueryToPolynomialGenerator();
		System.out.println("using query file at path: " + generator.getQueryFile());

		//compute the provenance polynomials
		HashMap<String, String[][]> polynomials = generator.generateHowProvenancePolynomial(null);

		CreditDistributor distributor = new CreditDistributor();
		
		String outputDirectory = distributor.getOutputLogDir();
		String fileName = "lineage_" + distributor.getDatabase();
		BufferedWriter writer = LogManagement.getAWriterWithTimestamp(outputDirectory, fileName, "csv");

		//compute the lineage 
		Map<String, Set<String>> lineageMap = generator.convertPolynomialsToLineage(polynomials);
		//distribute with lineage
		distributor.distributeCreditWithLineageBasicButSmarter(lineageMap);

		distributor.shutDown();
		System.out.println("done");
	}

	/** In this main, we deal with the the problem of multiple queries with a list of 
	 * provenances.
	 * 
	 * @throws IOException if the output directory where to write the 
	 * log files does not exists. It is specified in the output.log.directory property
	 * */
	public static void main(String[] args) throws IOException {
		System.out.println("starting the redistribution of credit");
		//create the generator
		QueryToPolynomialGenerator generator = new QueryToPolynomialGenerator();
		System.out.println("using query file at path: " + generator.getQueryFile());

		//compute the provenance polynomials for every query in the textual file
		List<HashMap<String, String[][]>> polynomialsList = generator.generateHowProvenancePolynomials(null);
		//object which performs the distribution
		CreditDistributor distributor = new CreditDistributor();
		int queryCounter = 0;

		//file where we write the times
		String outputDirectory = distributor.getOutputLogDir();
		String fileName = "lineage_" + distributor.getDatabase();
		BufferedWriter writer = LogManagement.getAWriterWithTimestamp(outputDirectory, fileName, "csv");
		
		//we write a csv
		writer.write("QUERY NO, TIME");
		writer.newLine();

		for(HashMap<String, String[][]> polynomials : polynomialsList) {//for every (provenance of) every query 
			//compute the lineages 
			Map<String, Set<String>> lineageMap = generator.convertPolynomialsToLineage(polynomials);
			//distribute with lineage
			Stopwatch lineageTimer = Stopwatch.createStarted();
			distributor.distributeCreditWithLineageBasicButSmarter(lineageMap);
			
			//write the results
			String log = (queryCounter++) + "," + lineageTimer.stop();
			writer.write(log);
			writer.newLine();
			System.out.println(log);
		}

		distributor.shutDown();
		writer.close();
		
		System.out.println("done");
		//TODO adesso qui stai facendo una query per volta, ed hai fatto una lista di set di polinomi. 
		//una possibilità è fare un set unico di polinomi così processi tutte le query 'assieme'
	}

}
