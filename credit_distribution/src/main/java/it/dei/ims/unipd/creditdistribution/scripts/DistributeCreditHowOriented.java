package it.dei.ims.unipd.creditdistribution.scripts;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.jena.ext.com.google.common.base.Stopwatch;

import it.unipd.dei.ims.creditdistribution.distributors.CreditDistributor;
import it.unipd.dei.ims.creditdistribution.provenance.PatchMethods;
import it.unipd.dei.ims.creditdistribution.provenance.QueryToPolynomialGenerator;
import it.unipd.dei.ims.rum.utilities.LogManagement;

/** Pretty confusing name, isn't it? So
 * I had to develop some queries to show how how-prov is different from the
 * other forms of provenance. To do so, I needed to write so nested queries 
 * but, alas, Yinjun's code is not able to deal with nested queries (it deals with 
 * aggregations though, isn't that cool? Give the poor guy a break).
 * Anyway, I really needed to deal with these nested queries. To do so, I
 * developed a complex system of mirrors and pulleys to do so.
 * 
 * Now, this class deals with these queries, and distributes the lineage and
 * distributes the credit. It is a test, so let us see how this goes.
 * */
public class DistributeCreditHowOriented {


	public static void main(String[] args) throws IOException {

		//create the generator
		QueryToPolynomialGenerator generator = new QueryToPolynomialGenerator();
		System.out.println("using query file at path: " + generator.getQueryFile());

		//compute the provenance polynomials for every query in the textual file
		List<HashMap<String, String[][]>> polynomialsList = generator.generateHowProvenancePolynomials(null);

		//now we read the file with the tuple ids
		Path p = Paths.get(generator.getTupleIdsFile());
		BufferedReader reader = Files.newBufferedReader(p);
		
		
		CreditDistributor distributor = new CreditDistributor();
		int queryCounter = 0;

		
		String outputDirectory = distributor.getOutputLogDir();
		
		for(HashMap<String, String[][]> polynomials : polynomialsList) {
			//get the tuple id to multiply
			String tuple_id = reader.readLine();
			//add the id to the polynomial
			HashMap<String, String[][]> newPolynomials = PatchMethods.multiplyPolynomialWithVariable(polynomials, tuple_id);
			//now distribute
			System.out.println("Distributing via how provenance");
			distributor.distributeCreditWithHowProvenanceBasicButSmarter(newPolynomials);
			
			//now distribute with lineage
			Map<String, Set<String>> lineageMap = generator.convertPolynomialsToLineage(newPolynomials);
			distributor.distributeCreditWithLineageBasicButSmarter(lineageMap);
			
			List<List<Set<String>>> whitnessBase = generator.convertPolynomialsToWhyProvenance(newPolynomials);
			distributor.distributeCreditWithWhyProvenanceBasicButSmarter(whitnessBase);
		}
		

		distributor.shutDown();
		reader.close();
		
		System.out.println("done");

	}
}
