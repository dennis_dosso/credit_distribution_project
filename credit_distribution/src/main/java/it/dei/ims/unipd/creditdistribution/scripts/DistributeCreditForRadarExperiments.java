package it.dei.ims.unipd.creditdistribution.scripts;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import it.dei.ims.unipd.creditdistribution.precomputations.scripts.UpdateUtilities;
import it.unipd.dei.ims.creditdistribution.citations.utils.CitationCounter;
import it.unipd.dei.ims.creditdistribution.datastructures.Author;
import it.unipd.dei.ims.creditdistribution.distributors.CreditDistributor;
import it.unipd.dei.ims.creditdistribution.polynomials.utils.PolynomialsProducer;
import it.unipd.dei.ims.creditdistribution.provenance.QueryToPolynomialGenerator;

/** I prepared this class to add some spicy test and for the last
 * experiments of my thesis. Actually, I wrote this on the beach. Ah, the memories
 * (not true, I was bored at the beach, and there is a couple next to me feeding their little 
 * toddler. Lovely. And disgusting. A combination of the two. Losgusting?).
 * <p>
 * This scripts distributes the credit to tables provived with _author_ column,
 * also provides them with a citation count. Later I will be able to use this information
 * to produce some fancy radar plots, I hope.
 * 
 * 
 * */
public class DistributeCreditForRadarExperiments {
	
	public static void main(String[] args) throws SQLException {
		
		// first we reset the database
		UpdateUtilities updateUtilities = new UpdateUtilities();
		String[] tables = {"family", "contributor2family", "contributor"};
		updateUtilities.set0ToTablesAffectedBySyntheticExperiments(tables);
		
		
		QueryToPolynomialGenerator generator = new QueryToPolynomialGenerator();
		System.out.println("using query file at path: " + generator.getQueryFile());
		
		//get the list of my polynomials
		List<HashMap<String, String[][]>> polynomialsList = PolynomialsProducer.produceFancySyntheticPolynomials(10000);
		CreditDistributor distributor = new CreditDistributor();
		
		//maps keeping track of the authors and their citation count
		Map<String, Integer> authorsMap = new HashMap<String, Integer>();
		
		CitationCounter counter = new CitationCounter();
		
		//for each polynomial, distribute the credit and count the citations to
		//each author
		int count = 0;
		
		for(HashMap<String, String[][]> polynomials : polynomialsList) {
			
//			System.out.println("Distributing via how provenance");
			distributor.distributeCreditWithHowProvenanceBasicButSmarter(polynomials);

//			System.out.println("Distributing via lineage");
			Map<String, Set<String>> lineageMap = generator.convertPolynomialsToLineage(polynomials);
			distributor.distributeCreditWithLineageBasicButSmarter(lineageMap);
			
//			System.out.println("Distributing via why provenance");
			List<List<Set<String>>> whitnessBase = generator.convertPolynomialsToWhyProvenance(polynomials);
			distributor.distributeCreditWithWhyProvenanceBasicButSmarter(whitnessBase);
			
			//update the citation count for the authors
			counter.countCitationsForAuthorsFromLineage(lineageMap, authorsMap);
			
			count++;
			if (count % 100 == 0) {
				System.out.println("dealt with " + count + " polynomials");
			}
		}
		
		//now that we have citations and credit distributed, we count all of it
		List<Author> authorList = counter.computeTotalCreditForAuthors(authorsMap);
		
		//now we write this list in a csv file
		String outputPath = counter.getOutputCsvPath() + "/authors_citations_credit.csv";
		Path p = Paths.get(outputPath);
		try(BufferedWriter w = Files.newBufferedWriter(p)){
			// header of the csv
			w.write("author_id,citation_count,lineage,why,how");
			w.newLine();
			
			for(Author a : authorList) {
				w.write(a.getName() + "," + a.getCitationCount() + "," + a.getLineageCredit() +
						"," + a.getWhyCredit() + "," + a.getHowCredit());
				w.newLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("done");
	}
}
