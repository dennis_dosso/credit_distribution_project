package it.dei.ims.unipd.creditdistribution.rebuttal;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import it.unipd.dei.ims.creditdistribution.datastructures.CausalityTuple;
import it.unipd.dei.ims.creditdistribution.distributors.CreditDistributor;
import it.unipd.dei.ims.creditdistribution.provenance.QueryToPolynomialGenerator;

/** Using real-world queries extracted from the BJP, 
 * this class converts those queries into polynomials, and it uses those polynomials
 * to generate and distribute credit into the databases.
 * <p>
 * In this class we use both responsibility and normalized responsibility
 * 
 * */
public class DistributeViaResponsibility {

	
	public static void main(String[] args) {
		System.out.println("The credit distribution process started...");
		
		// initialize the object containing the necessary variables taken from the properties file
		QueryToPolynomialGenerator generator = new QueryToPolynomialGenerator();
		
		// compute the how-provenance polynomials from the queries in the files
		List<HashMap<String, String[][]>> polynomialsList = generator.generateHowProvenancePolynomials(null);
		
		// now that we have the polynomials, distribute the credit using the different provenances
		CreditDistributor distributor = new CreditDistributor();
		
		int c = 0;
		
		for(HashMap<String, String[][]> polynomials : polynomialsList) {
			// first, we need to generate the why provenance
			List<List<Set<String>>> witnessBase = generator.convertPolynomialsToWhyProvenance(polynomials);
			// now, for each why prov, generate the responsibility
			for(List<Set<String>> whyProv : witnessBase) {
				List<CausalityTuple> responsibleTuples = generator.convertOneWhyProvToOneResponsibilitySet(whyProv);
				generator.computeNormalizedResponsibility(responsibleTuples);
				distributor.distributeCreditViaResponsibility(responsibleTuples);
				distributor.distributeCreditViaNormalizedResponsibility(responsibleTuples);
			}
			c++;
			if(c % 1000 == 0)
				System.out.println("distributed credit of  " + c + " queries out of " + polynomialsList.size());
		}
		
		distributor.shutDown();
		System.out.println("done");
		
	}
}
