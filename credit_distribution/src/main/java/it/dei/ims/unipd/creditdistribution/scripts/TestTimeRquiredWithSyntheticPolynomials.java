package it.dei.ims.unipd.creditdistribution.scripts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ext.com.google.common.base.Stopwatch;

import it.dei.ims.unipd.creditdistribution.precomputations.scripts.UpdateUtilities;
import it.unipd.dei.ims.creditdistribution.distributors.CreditDistributor;
import it.unipd.dei.ims.creditdistribution.polynomials.utils.PolynomialsProducer;
import it.unipd.dei.ims.creditdistribution.provenance.QueryToPolynomialGenerator;

/** This class was used as a script to check how much time is required to 
 * compute a credit distribution using different quantities of credit.
 * <p>
 *  It can also be used to compute random  synthetic distributions of credit
 *  with an arbitrary number of "complex" polynomials.
 *  
 *  */
public class TestTimeRquiredWithSyntheticPolynomials {
	
	public static void testTime() {
		
		// first we reset the database
//		UpdateUtilities updateUtilities = new UpdateUtilities();
//		String[] tables = {"family", "contributor2family", "contributor"};

		Stopwatch timer = Stopwatch.createStarted();
		
//		updateUtilities.set0ToTablesAffectedBySyntheticExperiments(tables);
//		System.out.println("tabula rasa in " + timer.stop());
//		timer.reset().start();
		
		// produce the synthetic polynomials
		List<HashMap<String, String[][]>> polynomialsList = PolynomialsProducer.produceFancySyntheticPolynomials(5000);
		System.out.println("polynomials created in " + timer.stop());
		
		QueryToPolynomialGenerator generator = new QueryToPolynomialGenerator();
		CreditDistributor distributor = new CreditDistributor();
		
		
		List<Map<String, Set<String>>> lineageMaps = new ArrayList<Map<String, Set<String>>>();
		timer.reset().start();

		//convert to lineage
		for(HashMap<String, String[][]> polynomials : polynomialsList) {
			Map<String, Set<String>> lineageMap = generator.convertPolynomialsToLineage(polynomials);
			lineageMaps.add(lineageMap);
		}
		System.out.println("convertion to lineage computed in " + timer.stop());
		timer.reset().start();
		
		//distribute via lineage
		for(Map<String, Set<String>> lineageMap : lineageMaps) {	
			distributor.distributeCreditWithLineageBasicButSmarter(lineageMap);
		}
		System.out.println("distribution with lineage computed in " + timer.stop());
		timer.reset().start();
		
		//convert to why provenance 
		List<List<List<Set<String>>>> whitnessBases = new ArrayList<List<List<Set<String>>>>(); // I know, crazy 
		for(HashMap<String, String[][]> polynomials : polynomialsList) {
			List<List<Set<String>>> whitnessBase = generator.convertPolynomialsToWhyProvenance(polynomials);
			whitnessBases.add(whitnessBase);
		}
		System.out.println("convertion to why provenance in " + timer.stop());
		timer.reset().start();
		
		for(List<List<Set<String>>> whitnessBase : whitnessBases) {
			distributor.distributeCreditWithWhyProvenanceBasicButSmarter(whitnessBase);
		}
		System.out.println("distribution with why-provenance computed in " + timer.stop());
		timer.reset().start();
		
		// finally, with how-provenance
		for(HashMap<String, String[][]> polynomials : polynomialsList) {
			distributor.distributeCreditWithHowProvenanceBasicButSmarter(polynomials);
		}
		System.out.println("distribution with how-provenance computed in " + timer.stop());
		
		
		
	}
	
	
	public static void main(String[] args) {
		testTime();
	}

}
