package it.dei.ims.unipd.creditdistribution.scripts;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;

import it.unipd.dei.ims.creditdistribution.distributors.CreditDistributor;
import it.unipd.dei.ims.creditdistribution.provenance.QueryToPolynomialGenerator;
import it.unipd.dei.ims.rum.utilities.LogManagement;

public class DistributeViaAttributeHowProvenance {

	public static void main(String[] args) throws IOException {
		System.out.println("starting the redistribution of credit");
		QueryToPolynomialGenerator generator = new QueryToPolynomialGenerator();
		System.out.println("using query file at path: " + generator.getQueryFile());

		//compute the provenance polynomials
		List<HashMap<String, String[][]>> polynomialsList = generator.generateHowProvenancePolynomials(null);
		CreditDistributor distributor = new CreditDistributor();
		int queryCounter = 0;
		//file where we write the times
		String outputDirectory = distributor.getOutputLogDir();
		String fileName = "how_provenance_" + distributor.getDatabase();
		BufferedWriter writer = LogManagement.getAWriterWithTimestamp(outputDirectory, fileName, "csv");
		//we write a csv
		writer.write("QUERY NO, TIME");
		writer.newLine();

		//open the path to the query file, we need the queries
		Path path = Paths.get(generator.getQueryFile());
		BufferedReader reader = Files.newBufferedReader(path);
		String query = "";
		for(HashMap<String, String[][]> polynomials : polynomialsList) {//for every (provenance of) every query
			//read the query
			query = reader.readLine();
			//get the attribute how provenance
			List<List<List<String>>> attributeHowPolynomials = generator.convertHowProvenanceInAttributeHowProvenance(polynomials, query);
			System.out.println(attributeHowPolynomials);
			//now use it to distribute the credit to the attributes
		}
	}

}
