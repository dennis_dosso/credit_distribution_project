package it.dei.ims.unipd.creditdistribution.rebuttal;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import it.unipd.dei.ims.creditdistribution.polynomials.utils.PolynomialsProducer;

/** Class to print synthetic polynomial created through the use of PolunomialsProduced.java*/

public class PrintPolynomial {

	
	public static void main(String[] args) throws IOException {
		
		// produce a certain number of polynomials with the strategy written in PolunomialsProducer class
//		List<HashMap<String, String[][]>> polynomialsList = PolynomialsProducer.produceFancySyntheticPolynomials(10000);
		List<HashMap<String, String[][]>> polynomialsList = PolynomialsProducer.produceSyntheticPolynomials(10000);
		
		String outputPath = "/Users/dennisdosso/MEGAsync/Ricerca/progetti_di_ricerca/Credit Distribution/synthetic_polynomials/synthetic_polynomials.csv";
		
		BufferedWriter writer = Files.newBufferedWriter(Paths.get(outputPath));
		
		for(HashMap<String, String[][]> polynomialMap : polynomialsList) {
			for(Entry<String, String[][]> en : polynomialMap.entrySet()) {
				
				// a polynomial is a list of lists, each single list is a monomial. Each string of the monomial is a variable
				String[][] polynomial = en.getValue();
				
				// print each polynomial in a line
				int monomials = polynomial.length;
				for (int i = 0; i < monomials; ++i) {
					String[] monomial = polynomial[i];
					int monomialLength = monomial.length;
					for(int j = 0; j < monomialLength; ++ j) {
						writer.write(polynomial[i][j]);
						if(j < monomialLength -1)
							writer.write(" ");// if we did not reach the end of the monomial, we use a space to divide a variable from the other
					}
					
					if(i < monomials - 1)
						writer.write(",");// we have a new monomial after this
					else if(i == monomials - 1)
						writer.newLine();// let us get to a new line
				}
			}
			
		}
		
		
		writer.flush();
		writer.close();
		
	}
}
