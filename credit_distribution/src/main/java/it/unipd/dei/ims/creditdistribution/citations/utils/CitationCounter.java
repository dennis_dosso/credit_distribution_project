package it.unipd.dei.ims.creditdistribution.citations.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import it.unipd.dei.ims.creditdistribution.datastructures.Author;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

public class CitationCounter {

	protected Map<String, String> map;


	private String host;
	private String port;
	private String database;
	private String user;
	private String password;
	private String jdbcString;
	private Connection c;
	
	/** Directory where we write the results of our computations, 
	 * in main.properties */
	private String outputCsvPath;


	public CitationCounter() {
		map = PropertiesUsefulMethods.getPropertyMap("properties/main.properties");

		this.host = map.get("host");
		this.port=map.get("port");
		this.database=map.get("database");
		this.user=map.get("user");
		this.password=map.get("password");
		this.jdbcString = "jdbc:postgresql://" + this.host + ":"+ this.port + "/" + this.database + "?user=" + this.user + "&password=" + this.password;
		
		this.outputCsvPath = map.get("output.csv.path");
		
		Properties props = new Properties();
		try {
			c = DriverManager.getConnection(this.jdbcString, props);
		} catch (SQLException e) {
			System.err.println("error connecting");
			e.printStackTrace();
		}

	}

	/**@param lineageMap the lineage of the polynomial we are dealing with
	 * @param authorsMap a map where, for each authorm we also have the counter of its citations
	 * @throws SQLException 
	 * */
	public void countCitationsForAuthorsFromLineage(Map<String, Set<String>> lineageMap, Map<String, Integer> authorsMap) throws SQLException {
		//since I want to distribute the credit of this query but also 
		//cite each author connected to this query/polynomial, and since each author
		//has to receive exactly one citation, 
		//independently from how many tuple he produced, 
		//I need to count the authors only once
		Set<String> authors = new HashSet<String>();
		//for each polynomial
		for(Entry<String, Set<String>> lineageE: lineageMap.entrySet()) {
			//get the lineage
			Set<String> lineage = lineageE.getValue();
			for(String tupleId: lineage) {
				//get the parts of the id of the tuple
				String[] parts = tupleId.split("\\|");
				String tableName = parts[0];//get the name of the table
				//now we get the author of this tuple
				String sql_query = "SELECT \"_author_\" FROM " + tableName + " WHERE \"c||prov\" = ?";
				
				PreparedStatement pst = c.prepareStatement(sql_query);
//				pst.setString(1, tableName);
				pst.setString(1, tupleId);
				
				ResultSet rst = pst.executeQuery();
				while(rst.next()) {
					String author = rst.getString(1);
					//add the author to the set
					authors.add(author);
				}
			}
		}
		//now we have all the authors of this query, increment their contribution
		for(String author : authors) {
			Integer count = authorsMap.get(author);
			if(count == null) {
				authorsMap.put(author, 1);
			} else {
				count++;
				authorsMap.put(author, count);
			}
			
		}
	}
	
	/** After the credit has been distributed, this method 
	 * takes into input a table with the citation counts of an author and returns a list
	 * of Author objects containing the information
	 * about the citation and credit count associated to each author for each 
	 * type of provenance. 
	 * This method only considers the three tables family, contributor and contributor2family.
	 * It can easily extended in the future to include all the tables. 
	 * 
	 * */
	public List<Author> computeTotalCreditForAuthors(Map<String, Integer> authorsMap) throws SQLException {
		List<Author> authorsList = new ArrayList<Author>();
		
		for(Entry<String, Integer> author_ : authorsMap.entrySet()) {
			Author author = new Author();
			//get the name of the author
			String authorName = author_.getKey();
			int authors_citation = author_.getValue();
			
			author.setName(authorName);
			author.setCitationCount(authors_citation);
			
			//get the total credit of this author in the table family
			String[] provenances = {"lineage_credit", "why_credit", "how_credit", "resp", "norm_resp", "shapley"};
			String[] tables = {"family", "contributor2family", "contributor"};
			
			int provenanceCount = 0;
			for(String provenance : provenances) {
				double credit = 0;
				for(String table : tables) {
					
					String sql_query = "SELECT SUM(" + provenance + ") FROM " + table + " WHERE \"_author_\" = ?";
					PreparedStatement ps = c.prepareStatement(sql_query);
					ps.setString(1, authorName);
					
					ResultSet rs = ps.executeQuery();
					rs.next();
					credit +=  rs.getDouble(1); //update the credit

				}
				//now you have the credit for this author for this provenance, update it
				if(provenanceCount == 0)
					author.setLineageCredit(credit);
				else if(provenanceCount == 1)
					author.setWhyCredit(credit);
				else if(provenanceCount == 2)
					author.setHowCredit(credit);
				else if(provenanceCount == 3)
					author.setResponsibility(credit);
				else if(provenanceCount == 4)
					author.setNormalizedResponsibility(credit);
				else if(provenanceCount == 5)
					author.setShapleyValue(credit);
				
				
				provenanceCount++;
				
			}//end of the provenances
			authorsList.add(author);
		}//end of this author
		return authorsList;
	}

	public String getOutputCsvPath() {
		return outputCsvPath;
	}

	public void setOutputCsvPath(String outputCsvPath) {
		this.outputCsvPath = outputCsvPath;
	}
	
	public void shutDown() {
		try {
			this.c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
