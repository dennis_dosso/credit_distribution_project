package it.unipd.dei.ims.creditdistribution.provenance;

import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.commons.lang3.ArrayUtils;

/** So, this class contains method(s) that are used by other classes.
 * These are not full fledged methods, in the sense
 * that I did not envision them in with a general purpose,
 * but tailored around pretty specific instances. 
 * Thus, I am a little ashamed of them. Shhh, don't tell anyone
 * 
 * @author dennisdosso
 * */
public class PatchMethods {

	/** Given the provenance polynomial of a query 
	 * (potentially many output tuples) multiplies them with the same
	 * string variable
	 * */
	public static HashMap<String, String[][]> multiplyPolynomialWithVariable(
			HashMap<String, String[][]> polynomials, 
			String variable) {
		HashMap<String, String[][]> newPolynomials = new HashMap<String, String[][]>(); 
		//for each output tuple
		for(Entry<String, String[][]> e : polynomials.entrySet()) {
			String key = e.getKey();
			String[][] polynomial = e.getValue();
			int numOfMonomials = polynomial.length;
			int numOfVariables = polynomial[0].length;
			//the new polynomial has the same number of monomials, but each monomial has
			//one more variable
			String[][] newPolynomial = new String[numOfMonomials][numOfVariables + 1];
			for(int i = 0; i < numOfMonomials; ++i) {
				//build a new monomial where you multiply with the variable
				String[] newMonomial = ArrayUtils.add(polynomial[i], variable);
				newPolynomial[i] = newMonomial;
			}
			newPolynomials.put(key, newPolynomial);
		}
		return newPolynomials;
		
	}
}
