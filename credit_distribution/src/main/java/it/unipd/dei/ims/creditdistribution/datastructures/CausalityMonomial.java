package it.unipd.dei.ims.creditdistribution.datastructures;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/** This class represents a "monomial", a container of tuples as they appear in 
 * the how-provenance of an output tuple. It contains useful information
 * used to compute causality and responsibility
 * 
 * */
public class CausalityMonomial implements Comparable<CausalityMonomial> {

	public List<CausalityTuple> variables;
	
	/** This is the sum of the frequencies of the tuples in the monomial*/
	public int weight;
	
	public boolean blocked;
	
	public CausalityMonomial() {
		this.variables = new ArrayList<CausalityTuple>();
		weight = 0;
		blocked = false;
	}
	
	public void reset() {
		blocked = false;
		for(CausalityTuple t : this.variables) {
			t.blocked = false;
		}
	}
	
	public static void resetListOfMonomials(List<CausalityMonomial> list) {
		for(CausalityMonomial cm : list) {
			cm.reset();
		}
	}
	
	/** Find the tuple and block it. If the tuple is not present in the witness, nothing happens.
	 * 
	 * Also, performs a check. If all tuples are now blocked, it gets blocked in its entirety 
	 * */
	public void blockTuple(CausalityTuple t) {
		
		for(CausalityTuple ct : this.variables) {
			if(ct.tupleValue.equals(t.tupleValue)) {
				ct.blocked = true;
			}
		}
		boolean allBlocked = true;
		for(CausalityTuple ct : this.variables) {
			if(!ct.blocked) {
				allBlocked = false;
				break;
			}
		}
		
		if(allBlocked)
			this.blocked = true;
	}
	
	public void blockTheMonomialIfItContainsThisTuple(CausalityTuple t) {
		for(CausalityTuple ct : this.variables) {
			if(ct.tupleValue.equals(t.tupleValue)) {
				this.blocked = true;
				return;
			}
		}
	}
	
	
	public void addTuple(String t) {
		// check if this tuple is already present
		for(CausalityTuple ct : this.variables) {
			// if already present, increase its exponent
			if(ct.tupleValue.equals(t)) {
				ct.exponent++;
				return;
			} 
		}
		
		// if we are here, it means that this is a new tuple to add to this monomial
		CausalityTuple c = new CausalityTuple(t);
		this.variables.add(c);
	}
	
	/** Returns from this monomial the tuple with the highest exponent and which has not already been used*/
	public CausalityTuple getHighestUnusedVariable() {
		CausalityTuple toReturn = null;
		for(CausalityTuple ct : this.variables) {
			if(!ct.used) { // if this tuple has not been used
				if(toReturn == null)
					toReturn = ct;
				else if(toReturn.exponent < ct.exponent)
					toReturn = ct;
			}
		}
		toReturn.used = true;
		return toReturn;
	}
	
	/** Given an array of strings, representing the variables constituting a monomial, 
	 * inserts them in this instance of monomial
	 * 
	 * */
	public void importListOfStringsAsMonomial(String[] ms) {
		
		for(String tuple : ms) {
			this.addTuple(tuple);
		}
	}
	
	/** Checks if the monomial contains a specific tuple based on an tuple string value*/
	public boolean containsThisTuple(String tuple) {
		// jolly value
		if(tuple.equals(""))
			return true;
		
		for(CausalityTuple ct : this.variables) {
			if(ct.tupleValue.equals(tuple))
				return true;
		}
		return false;
	}
	
	/** From a set of monomials, extracts a list of tuples representing a new level for a particular node.
	 * 
	 * @param tupleValue set to a value different from empty string to only work with monomials that contain this tuple*/
	public static List<CausalityTuple> getLevelOfHierarchy(List<CausalityMonomial> monomials, String tupleValue) {
		List<CausalityTuple> level = new ArrayList<>();
		for(int i = 0; i < monomials.size(); ++i) {
			CausalityMonomial monomial = monomials.get(i);
			if(!monomial.containsThisTuple(tupleValue))
				continue; // we do not need to consider this monomial
			
			CausalityTuple ct = monomial.getHighestUnusedVariable();
			level.add(ct);
		}
		return level;
	}
	
	
	/** given a list of monomials, or better, a list of witnesses, compute the frequency of each tuple in them
	 * It updates the value of the field .frequency in each tuple in the monomials of the witness basis
	 * */
	public static void enrichTuplesWithTheirFrequency(List<CausalityMonomial> witnessBasis) {
		for(int i = 0; i < witnessBasis.size(); ++i) { // for each witness
			CausalityMonomial monomial = witnessBasis.get(i);
			List<CausalityTuple> tuples = monomial.variables;
			
			for(int j = 0; j < tuples.size(); ++j) { // for each tuple in the witness
				CausalityTuple tuple = tuples.get(j);
				
				// now that we have the tuple, check its presence in each other monomials
				for(CausalityMonomial m : witnessBasis) {
					if(m.containsThisTuple(tuple.tupleValue)) {
						// if present, increase the frequency of the tuple
						tuple.frequency++;
					}
				}
				// update the weight of this monomial 
				monomial.weight += tuple.frequency;
			}
			
		}
	}
	
	public Set<String> getAllTuplesOfThisMonomial() {
		Set<String> set = new HashSet<String>();
		for(CausalityTuple t : this.variables) {
			set.add(t.tupleValue);
		}
		return set;
	}
	
	public void addTuplesToTheSet(CausalityMonomial mnm) {
		for(CausalityTuple t : mnm.variables) {
			// check if this tuple can be inserted in the current monomial
			for(int i = 0; i < this.variables.size(); ++i) {
				if(t.tupleValue.equals(this.variables.get(i).tupleValue)) {
					if(t.frequency > this.variables.get(i).frequency) {
						this.variables.get(i).frequency = t.frequency;
					}
					break; // tuple already present in the monomial
				}
			}
		
			// if we are here, the tuple needs to be inserted
			this.variables.add(t);
		}
	}


	/** I want an DESC sorting, the highest on top*/
	@Override
	public int compareTo(CausalityMonomial o) {
		if(this.weight == o.weight)
			return 0;
		else if(this.weight < o.weight)
			return 1;
		else // if(this.weight > o.weight)
			return -1;
	}
	
	public String toString() {
		return this.weight + ",blocked:" + this.blocked;
	}
	
}
