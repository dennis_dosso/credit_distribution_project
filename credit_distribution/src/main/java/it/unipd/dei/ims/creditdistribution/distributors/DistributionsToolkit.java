package it.unipd.dei.ims.creditdistribution.distributors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.distribution.ParetoDistribution;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** This class contains methods to perform distributions
 * of credit using different kinds of multiplicity to the 
 * queries
 * 
 * */
public class DistributionsToolkit {

	/** Map used to deal with the properties/fields of this class */
	protected Map<String, String> map;
	
	private double scale_, shape_;
	

	/** Number of queries that make up a web page in IUPHAR */
	private int webPageQueryNumber;

	public DistributionsToolkit() {
		map = PropertiesUsefulMethods.getPropertyMap("properties/main.properties");

		this.webPageQueryNumber = Integer.parseInt(map.get("web.page.query.number"));
		this.scale_ = Double.parseDouble(map.get("scale"));
		this.shape_ = Double.parseDouble(map.get("shape"));

	}


	/**Given a list of queries in a certain order, it re-orders them.
	 * This method was thought because I needed a quick way to re-order the list of polynomials
	 * generated from the original method. 
	 * This was the situation: I has a list of queries for each class of families of iuphar.
	 * Every list is composed by all the queries that generate the first snippet of 
	 * the web page one after the other, then all the queries that generate the second snippet,
	 * then all the queries that generate the third one etc.
	 * What I want instead is a list of all th e queries that generate the web-page of the family #1,
	 * then all the queries that generate the webpage of family #2 etc.
	 * 
	 * @param polynomialList a list of polynomials of the provenances of our queries.
	 * Set this parameter to null if you want to use the field webPageQueryNumber of the class.
	 * @param queryNumber the number of queries that make up an entity (in IUPHAR's case, the number
	 * of queries making up a webpage)
	 * 
	 * */
	public List<HashMap<String, String[][]>> reorderQueriesInList(List<HashMap<String, String[][]>> polynomialsList, Integer queryNumber) {
		if(queryNumber == null) {
			queryNumber = this.webPageQueryNumber;
		}

		//get the list of the queries
		int length = polynomialsList.size();
		int numberOfFamilies = length / queryNumber;

		//now we have the number of families. We are ready to populate the new list
		List<HashMap<String, String[][]>> orderedList = new ArrayList<HashMap<String, String[][]>>();

		for(int i = 0; i < numberOfFamilies; ++i) {
			for(int j = 0; j < queryNumber; ++j) {
				orderedList.add(polynomialsList.get(i + numberOfFamilies*j));
			}
		}
		return orderedList;

	}

	
	/** 
	 * @param scale the scale of the pareto distribution. It tells you the minimum value that can be sampled
	 * (and the which will be sampled more frequently). If null it is set using the property file
	 * @param shape of the Pareto distribution. The same goes for this one if you use null.
	 *  */
	public List<Integer> generateTheseManySamplesFromAParetoDistribution(int n, Double scale, Double shape) {
		
		if(scale == null)
			scale = this.scale_;
		if(shape == null)
			shape = this.shape_;
		
		List<Integer> list = new ArrayList<Integer>();
		
		//with some test I found that this scale and this shape seems to represent 
		//quite well how things go in academia dealing with citations. 
		ParetoDistribution distribution = new ParetoDistribution(scale, shape);
		for (int i = 0; i < n; ++i) {
			double d = distribution.sample();
			int dup = (int) Math.ceil(d);
			
			//XXX here I put a maximum height otherwise we will get too many citations
			if(dup > 30)
				dup = 30;
			list.add(dup);
		}
		
		return list;
	}


	public double getScale_() {
		return scale_;
	}


	public void setScale_(double scale_) {
		this.scale_ = scale_;
	}


	public double getShape_() {
		return shape_;
	}


	public void setShape_(double shape_) {
		this.shape_ = shape_;
	}


	public int getWebPageQueryNumber() {
		return webPageQueryNumber;
	}


	public void setWebPageQueryNumber(int webPageQueryNumber) {
		this.webPageQueryNumber = webPageQueryNumber;
	}
}
