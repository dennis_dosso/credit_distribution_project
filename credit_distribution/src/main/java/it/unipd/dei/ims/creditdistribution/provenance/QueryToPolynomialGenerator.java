package it.unipd.dei.ims.creditdistribution.provenance;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Queue;
import java.util.Set;
import java.util.Vector;

import org.apache.commons.math3.util.CombinatoricsUtils;
import org.apache.jena.ext.com.google.common.collect.Sets;

import edu.upenn.cis.citation.Corecover.Query;
import edu.upenn.cis.citation.citation_view1.Head_strs;
import edu.upenn.cis.citation.query.Query_provenance_2;
import it.unipd.dei.ims.creditdistribution.datastructures.CausalityMonomial;
import it.unipd.dei.ims.creditdistribution.datastructures.CausalityTuple;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import main.Load_views_and_citation_queries;

/**This class takes in input a query for a databases and processes it to produce the 
 * how provenance polynomial.
 * 
 * <p>
 * Prepare a file with all the queries that you intend to submit
 * (in the correct format used by these methods) in the query.file property
 * (set its path)
 * 
 * In the main.properties file you then need also to set the data to connect
 * to the database.
 * 
 * 
 * <p>
 * NB: before you start generating polynomials, remind to initialize the tables with the init method in the
 * package how_provenance_simple
 * 
 * */
public class QueryToPolynomialGenerator {

	/** Query file the system is analyzing*/
	private String queryFile;

	/** This is a file that contains id. It is used in certain methods that need ids with the queries
	 * */
	private String tupleIdsFile;

	/** String to connect to the postgres database*/
	private String jdbcString;

	/**Maps representing the polynomial. Every key is populated with one output tuple.
	 * Every output tuple is associated to its polynomial. 
	 * The polynomial is represented by a list of lists. Every list in the sequence is a monomial
	 * in the polinomial. Different lists represent different monomials. They are all to 
	 * be intended as connected by a '+' sign. Every element in every list in one
	 * variable. */
	private HashMap<String, String[][]> provenancePolynomials;

	/** Map used to deal with the properties/fields of this class */
	protected Map<String, String> map;


	private String host;
	private String port;
	private String database;
	private String user;
	private String password;

	/** Builder of the class. Reads the properties/main.properties
	 * file to populate its fields.
	 * 
	 * */
	public QueryToPolynomialGenerator() {
		map = PropertiesUsefulMethods.getPropertyMap("properties/main.properties");

		queryFile = map.get("query.file");
		this.tupleIdsFile = map.get("tuple.ids.file");

		this.host = map.get("host");
		this.port=map.get("port");
		this.database=map.get("database");
		this.user=map.get("user");
		this.password=map.get("password");


		jdbcString = "jdbc:postgresql://" + this.host + ":"+ this.port + "/" + this.database + "?user=" + this.user + "&password=" + this.password;
		//		jdbcString = map.get("jdbc.string");

		provenancePolynomials = new HashMap<String, String[][]>();
	}

	/** Generates the provenance polynomial for every output of the provided query.
	 * NB: if null is given as parameter, the method sets as query_file the path
	 * contained in the property file (query.file in the main.properties file).
	 * <p>
	 * The query must be in this form:
	 * ''name of the query'' | 
	 * projection attributes, separated by commas, in the form 'table name'.'attribute name' |
	 * FROM clause. In this clause you can perform join using commas |
	 * WHERE clause. Where you write the parameters in the same form of 'table name'.'attribute name' |
	 * LAMBDA clause (never used it, so I cannot explain it to you, sorry pal)
	 * 
	 * @param query a string containing the query to submit to the how_provenance engine.
	 * @returns the list of provenance polynomials
	 * 
	 * 
	 * TODO modifica questo metodo in maniera che invece di gestire 1 sola query te ne gestisca tante, 
	 * tutte nello stesso file
	 * 
	 * */
	public HashMap<String, String[][]> generateHowProvenancePolynomial(String query_file) {
		if(query_file != null)
			this.queryFile = query_file;

		//open connection to the database
		Properties props = new Properties();
		props.setProperty("prepareThreshold", "10");
		try {
			Class.forName("org.postgresql.Driver");

			Connection c = DriverManager
					.getConnection(this.jdbcString, props);

			//now we are connected to the database. We can issue our query
			PreparedStatement pst = null;
			HashMap<String, Head_strs> query_instance = new HashMap<String, Head_strs>();
			Vector<Query> query = Load_views_and_citation_queries.get_query_test_case(this.queryFile, c, pst);




			ResultSet rs = Query_provenance_2.get_query_provenance(query.get(0), c, pst);
			Query_provenance_2.retrieve_query_instance_with_pure_provenance(rs, query.get(0), provenancePolynomials, query_instance);

			//we have our polynomials in the table, can return them
			System.out.println("provenances computed");

			c.close();


		} catch (ClassNotFoundException e) {
			e.printStackTrace(); 
		} catch (SQLException e) {
			System.err.println("Errors with connection string: " + 
					this.jdbcString);
			e.printStackTrace();
		}

		return provenancePolynomials;

	}

	/**Like {@link generateHowProvenancePolynomial}, only it considers all the queries inside a 
	 * query file and not only the query of the first line*/
	public List<HashMap<String, String[][]>> generateHowProvenancePolynomials(String query_file) {
		if(query_file != null)
			this.queryFile = query_file;

		//open connection to the database
		Properties props = new Properties();
		props.setProperty("prepareThreshold", "10");
		//list containing our polynomial
		List<HashMap<String, String[][]>> list = new ArrayList<HashMap<String, String[][]>>();

		try {
			Class.forName("org.postgresql.Driver");

			Connection c = DriverManager
					.getConnection(this.jdbcString, props);

			//now we are connected to the database. We can issue our query
			PreparedStatement pst = null;
			Vector<Query> query = Load_views_and_citation_queries.get_query_test_case(this.queryFile, c, pst);

			for(int i = 0; i < query.size(); i++ ) {
				HashMap<String, Head_strs> query_instance = new HashMap<String, Head_strs>();
				//create a provenance map which deals with this query
				HashMap<String, String[][]> provMap = new HashMap<String, String[][]>();;
				ResultSet rs = Query_provenance_2.get_query_provenance(query.get(i), c, pst);
				Query_provenance_2.retrieve_query_instance_with_pure_provenance(rs, query.get(i), provMap, query_instance);
				list.add(provMap);
			}

			//we have our polynomials in the table, can return them
			System.out.println("provenances computed");

			c.close();

		} catch (ClassNotFoundException e) {
			e.printStackTrace(); 
		} catch (SQLException e) {
			System.err.println("Errors with connection string: " + 
					this.jdbcString);
			e.printStackTrace();
		}

		return list;

	}


	/** Convert how provenance into attribute how provenance*/
	public List<List<List<String>>> convertHowProvenanceInAttributeHowProvenance(HashMap<String, String[][]> polynomials, String query) {
		//parse the query
		String[] parts = query.split("\\|");//now we have the parts of the query
		String selects = parts[1];//get the select element
		String wheres = parts[3];//get the where part
		String as = parts[5];

		//now split the different elements
		String[] asArray = as.split(",");

		//now work a little bit with the same as, to convert the abbrevitions in the full names of the tables
		for(int i = 0; i < asArray.length; ++i) {
			asArray[i] = asArray[i].trim();
			//in the 'as' part, we gave new names to the tables. Here we re-name the tables that we used 
			String[] names = asArray[i].split(":");
			String abbreviation = names[0].trim();
			String tableName = names[1].trim();
			//now we substitute the abbreviations with the names of the tables
			selects = this.substitutePrefixWithAnotherPrefix(selects, abbreviation, tableName);
			wheres = this.substitutePrefixWithAnotherPrefix(wheres, abbreviation, tableName);
		}
		//now we generate a set with the attributes used in this query
		Set<String> attributeSet = new HashSet<String>();
		//we insert in the set the list of attributes used
		this.takeCountOfUsedSttributes(attributeSet, selects, wheres);

		//crazy, right? The most external list is a list of polynomials, one for each output tuple
		//the internal list is a list of monomials, creating each a polynomial
		//every internal list is a list o tokens, building up a monomial
		List<List<List<String>>> attrProvPolynomials = new ArrayList<List<List<String>>>();

		for(Entry<String, String[][]> entry : polynomials.entrySet()) {
			//get one polynomial
			String outputTuple = entry.getKey();
			String[][] polynomial = entry.getValue();
			//list of lists representing this new polynomial
			List<List<String>> attrPolynomial = new ArrayList<List<String>>();

			//for every monomial in the polynomial
			for(String[] monomial : polynomial) {
				//list representing this new monomial
				List<String> attrMonomial = new ArrayList<String>();
				for(String token : monomial) {
					//we fixed one token in the monomial, we check if there is a table with its same name
					//generate the list of attribute tokens from this monomial
					List<String> attrTokens = this.checkForAttributesInThisToken(token, attributeSet);
					//add the tokens to this monomial
					attrMonomial.addAll(attrTokens);
				}
				//if we are here, this monomial is ready, add it to this polynomial
				attrPolynomial.add(attrMonomial);
			}
			//if we are here, this polunomial is ready, we can add it to the list of polynomials
			attrProvPolynomials.add(attrPolynomial);
		}
		//if we are here, the polynomials are all ready. Return them, folks
		return attrProvPolynomials;
	}


	private List<String> checkForAttributesInThisToken(String token, Set<String> attributesSet) {
		List<String> attrList = new ArrayList<String>();
		for(String attr : attributesSet) {
			//for each attribute used in the query
			String[] parts = attr.split("\\|");
			String tableName = parts[0];
			String attrName = parts[1];

			//get the name of the table and the id of the tuple of the token
			parts = token.split("\\|");
			String tokenTableName = parts[0];
			String tokenId = parts[1]; 

			//now check if the table is the same
			if(tableName.equals(tokenTableName)) {
				//if this is the case, add this token to the list of tokens of this polynomial
				attrList.add(tableName + "|" + tokenId + "|" + attrName ); 
			}
		}
		//if we are here, we checked all the possible attributes used in the query, time to return the list
		return attrList;
	}



	/** Given in input a set of strings and strings containing the select attributes
	 * and the where attributes in the fashion of the ones used in the query in the
	 * provenance format, populates the set with these attributes in the form
	 * name_of_the_table.name_of_the_attribute.
	 * 
	 * */
	public void takeCountOfUsedSttributes(Set<String> usedAttributes, String selects, String wheres) {
		String[] selectArray = selects.split(",");
		for(int j = 0; j < selectArray.length; ++j) {
			//for every attribute used
			String[] elements = selectArray[j].trim().split("\\.");
			String tableName = elements[0].trim();
			String attributeName = elements[1].trim();
			usedAttributes.add(tableName + "|" + attributeName);
		}

		if(!wheres.equals("")) {
			String[] whereArray = wheres.split(",");
			for(int k = 0; k < whereArray.length; ++k) {

				//get the attribute used at the left hand side of the where clauses
				String[] elements = whereArray[k].split("=")[0].trim().split("\\.");
				String tableName = elements[0].trim();
				String attributeName = elements[1].trim();
				usedAttributes.add(tableName + "|" + attributeName);

				//now get the ones on the right hand side
				String elem = whereArray[k].split("=")[1].trim();

				//if this is not a join condition, and thus these elements are not 
				//a value in a table but a 'real' value, there are no attributes to use here
				if(elem.contains("'"))
					continue;
				else {
					elements = elem.split("\\.");
				}

				tableName = elements[0];
				attributeName = elements[1];
				usedAttributes.add(tableName + "|" + attributeName);
			}

		}
	}


	/**Given the provenance polynomials as generated by {@link generateHowProvenancePolynomial}, builds
	 * the lineage sets, one per each output tuple, and returns them in a map. The keys are
	 * the tuples, the values are sets of strings, where the strings are the tokens.
	 * <p>
	 * Note that the polynomials correspond to those of one query. One query has one polynomiasl
	 * for each tuple in the output
	 * 
	 * @param polynomials a map that, for each output tuple, contains a list of lists
	 * representing a polynomial. This method converts them in the lineage
	 * 
	 * */
	public Map<String, Set<String>> convertPolynomialsToLineage(HashMap<String, String[][]> polynomials) {
		//map for the informations about the lineage. Every key is one tuple
		//every value is the set of provenance tokens extracted from the polynomial

		Map<String, Set<String>> lineageMap = new HashMap<String, Set<String>>();
		//		List<Set<String>> lineageList = new ArrayList<Set<String>>();

		for(Entry<String, String[][]> entity : polynomials.entrySet()) {
			String outputTuple = entity.getKey();
			//			System.out.println("[DEBUG]: converting polynomial of tuple " + outputTuple);

			String[][] polynomial = entity.getValue();
			//with the lineage, we are only interested in the set of tuples
			//without the molteplicity or exponentials
			//simply convert them into one set
			Set<String> lineage = new HashSet<String>();

			//for every monomial
			for(String[] monomial : polynomial) {
				//and for every variable in the monomial
				for(String variable : monomial) {
					lineage.add(variable);
				}
			}
			//add this lineage to the list of lineages
			lineageMap.put(outputTuple, lineage);
			//			lineageList.add(lineage);
		}
		//and now you can return the set
		return lineageMap;
	}



	/** method that I needed to convert certain words inside a query. 
	 * For example, if you have something like f.attribute = value, 
	 * this method converts the f in family. There are a couple of cases, so
	 * I needed a method to deal with this, ok? Don't judge, please.
	 * 
	 * */
	public String substitutePrefixWithAnotherPrefix(String longString, String oldPrefix, String newPrefix) {
		String returningString = "";

		String[] parts = longString.split(",");
		for(String s : parts) {
			//the two parts of a selection
			String[] pp = s.split("=");
			if(pp.length == 1) {
				//get the prefix and suffix
				String[] ps = s.split("\\.");
				String prefix = ps[0].trim();
				String suffix = ps[1].trim();
				if(prefix.equals(oldPrefix))//if necessary, update the prefix, else leave it alone
					prefix = newPrefix;
				returningString += prefix + "." + suffix + ",";				
			} else if (pp.length == 2){
				//it is a where clause
				s = pp[0];
				String[] ps = s.split("\\.");
				String prefix = ps[0].trim();
				String suffix = ps[1].trim();
				if(prefix.equals(oldPrefix))
					prefix = newPrefix;
				returningString += prefix + "." + suffix + "=";

				s = pp[1];
				ps = s.split("\\.");
				if(ps.length == 2) {
					prefix = ps[0].trim();
					suffix = ps[1].trim();
					if(prefix.equals(oldPrefix))
						prefix = newPrefix;
					returningString += prefix + "." + suffix + ",";					
				} else {
					returningString += s + ",";
				}

			}
			//there should be no other cases
		}

		return returningString.substring(0, returningString.length() - 1);
	}

	/** Given a set containing the lineages of the output tuples of a query and the query,
	 * we compute the corresponding attribute lineages. 
	 *  
	 *  
	 *  @return a list of sets of strings, e.e. the attribute lineages
	 * */
	public List<Set<String>> convertTupleLineageToAttributeLineage(Map<String, Set<String>> lineages, String query) {
		//parse the query
		String[] parts = query.split("\\|");//now we have the parts of the query
		String selects = parts[1];//get the select element
		String wheres = parts[3];//get the where part
		String as = parts[5];

		//now split the different elements
		String[] asArray = as.split(",");

		//now work a little bit with the same as
		for(int i = 0; i < asArray.length; ++i) {
			asArray[i] = asArray[i].trim();
			//in the 'as' part, we gave new names to the tables. Here we re-name the tables that we used 
			String[] names = asArray[i].split(":");
			String abbreviation = names[0].trim();
			String tableName = names[1].trim();
			//now we substitute the abbreviations with the names of the tables
			selects = this.substitutePrefixWithAnotherPrefix(selects, abbreviation, tableName);
			wheres = this.substitutePrefixWithAnotherPrefix(wheres, abbreviation, tableName);
		}
		//list of attribute lineages to return
		List<Set<String>> lineageList = new ArrayList<Set<String>>(); 


		//now, we need to isolate one lineage at the time
		for(Entry<String, Set<String>> lineageEntry : lineages.entrySet()) {
			Set<String> lineage = lineageEntry.getValue();
			//new set of strings, the attribute lineage set
			Set<String> attributeLineage = new HashSet<String>(); 

			//now we take the different attributes that we used. We start with the select attributes
			String[] selectArray = selects.split(",");
			for(int j = 0; j < selectArray.length; ++j) {
				//for every attribute used
				String[] elements = selectArray[j].trim().split("\\.");
				String tableName = elements[0];
				String attributeName = elements[1];
				attributeLineage.addAll(this.enrichLineageWithAttributes(lineage, tableName, attributeName));	
			}

			//do the same, but with the attributes used in the where clause
			if(!wheres.equals("")) {
				String[] whereArray = wheres.split(",");
				for(int k = 0; k < whereArray.length; ++k) {

					//get the attribute used at the left hand side of the where clauses
					String[] elements = whereArray[k].split("=")[0].trim().split("\\.");
					String tableName = elements[0];
					String attributeName = elements[1];
					attributeLineage.addAll(this.enrichLineageWithAttributes(lineage, tableName, attributeName));

					//now get the ones on the right hand side
					elements = whereArray[k].split("=")[1].trim().split("\\.");

					//if this is not a join condition, and thus these elements are not 
					//a value in a table but a 'real' value, there are no attributes to use here
					if(elements.length < 2)
						continue;

					tableName = elements[0];
					attributeName = elements[1];
					attributeLineage.addAll(this.enrichLineageWithAttributes(lineage, tableName, attributeName));


				}

			}

			//if we are here, it means we have populated the whole attribute lineage, and we can add it to the list
			lineageList.add(attributeLineage);
		}
		return lineageList;

	}

	public List<Set<String>> convertTupleLineageToWhereProvenance(Map<String, Set<String>> lineages, String query) {
		//parse the query
		String[] parts = query.split("\\|");//now we have the parts of the query
		String selects = parts[1];//get the select element
		//get the as parts
		String as = parts[5];

		//now split the different elements
		String[] asArray = as.split(",");

		//now work a little bit with the same as
		for(int i = 0; i < asArray.length; ++i) {
			asArray[i] = asArray[i].trim();
			//in the 'as' part, we gave new names to the tables. Here we re-name the tables that we used 
			String[] names = asArray[i].split(":");
			String abbreviation = names[0].trim();
			String tableName = names[1].trim();
			//now we substitute the abbreviations with the names of the tables
			selects = this.substitutePrefixWithAnotherPrefix(selects, abbreviation, tableName);
		}
		//list of attribute lineages to return
		List<Set<String>> whereList = new ArrayList<Set<String>>(); 


		//now, we need to isolate one lineage at the time
		for(Entry<String, Set<String>> lineageEntry : lineages.entrySet()) {
			Set<String> lineage = lineageEntry.getValue();
			//new set of strings, the attribute lineage set
			Set<String> attributeLineage = new HashSet<String>(); 

			//now we take the different attributes that we used. We start with the select attributes
			String[] selectArray = selects.split(",");
			for(int j = 0; j < selectArray.length; ++j) {
				//for every attribute used
				String[] elements = selectArray[j].trim().split("\\.");
				String tableName = elements[0];
				String attributeName = elements[1];
				attributeLineage.addAll(this.enrichLineageWithAttributes(lineage, tableName, attributeName));	
			}

			//if we are here, it means we have populated the whole attribute lineage, and we can add it to the list
			whereList.add(attributeLineage);
		}
		return whereList;
	}

	public Set<String> enrichLineageWithAttributes(Set<String> tupleLineage, String tableName, String attributeName) {
		Set<String> attributeLineage = new HashSet<String>(); 
		for(String lineageEntry : tupleLineage) {
			String[] parts = lineageEntry.split("\\|");
			String tableNameInLineage = parts[0];
			String tupleId = parts[1];
			if(tableNameInLineage.equals(tableName)) {
				//one attribute is used by this table, thus we enrich the lineage
				//build a provenance token 
				String token = tableName + "|" + tupleId + "|" + attributeName;
				attributeLineage.add(token);
			}
		}
		return attributeLineage;
	}


	public List<List<Set<String>>> convertPolynomialsToWhyProvenance(HashMap<String, String[][]> polynomials) {

		//the list with all the witness sets for every output tuple (so a list of sets
		//of sets -- I know, complex. Let's not think too much about it) 
		List<List<Set<String>>> manyWitnessBasis = new ArrayList<List<Set<String>>>(); 
		//		Map<String, List<Set<String>>> witnessBasisMap = new HashMap<String, List<Set<String>>>();

		for(Entry<String, String[][]> entity : polynomials.entrySet()) {
			String outputTuple = entity.getKey();
			//			System.out.println("[DEBUG]: converting polynomial of tuple in why prov.:" + outputTuple);


			List<Set<String>> witnessBasis = new ArrayList<Set<String>>();

			String[][] polynomial = entity.getValue();
			//for every monomial
			for(String[] monomial : polynomial) {
				//one monomial can be converted to a witness set
				Set<String> witnesses = new HashSet<String>();
				for(String variable : monomial) {
					//add the witness to the witness set
					witnesses.add(variable);
				}

				//now we check if this witness set can be added to the list of witnesses
				this.addWitnessSetToWitnessBasis(witnessBasis, witnesses);
			}
			//if we are here, we have populated this witness basis, add this basis
			//to our list
			manyWitnessBasis.add(witnessBasis);
		}
		return manyWitnessBasis;
	}


	/**
	 * 
	 * @param whyProvenances a list of why provenances. Every element is a witness basis for one of the output tuples produced by
	 * a query
	 * */
	public List<List<Set<String>>> convertWhyProvenanceInAttributeWhyProvenance(List<List<Set<String>>> whyProvenances, String query) {
		//parse the query
		String[] parts = query.split("\\|");//now we have the parts of the query
		String selects = parts[1];//get the select element
		String wheres = parts[3];//get the where part
		String as = parts[5];//get the as part
		//now split the different elements
		String[] asArray = as.split(",");
		//convert certain names in the query since we need them equal to the names of the tables
		for(int i = 0; i < asArray.length; ++i) {
			asArray[i] = asArray[i].trim();
			//in the 'as' part, we gave new names to the tables. Here we re-name the tables that we used 
			String[] names = asArray[i].split(":");
			String abbreviation = names[0].trim();
			String tableName = names[1].trim();
			//now we substitute the abbreviations with the names of the tables
			selects = this.substitutePrefixWithAnotherPrefix(selects, abbreviation, tableName);
			wheres = this.substitutePrefixWithAnotherPrefix(wheres, abbreviation, tableName);
		}

		List<List<Set<String>>> returningWhyProvenances = new ArrayList<List<Set<String>>>();

		//now, one witness basis at the time
		for(List<Set<String>> whyProv : whyProvenances) {
			//the new attribute why provenance
			List<Set<String>> attrWhyProv = new ArrayList<Set<String>>();

			//now, for every witness inside the list of witnesses
			for(Set<String> witness : whyProv) {
				//new witness set for attributes
				Set<String> attrWitness = new HashSet<String>();

				//now we take the different attributes that we used. We start with the select attributes
				String[] selectArray = selects.split(",");
				for(int j = 0; j < selectArray.length; ++j) {
					//for every attribute used
					String[] elements = selectArray[j].trim().split("\\.");
					String tableName = elements[0];
					String attributeName = elements[1];
					//since lineage is a kind of witness, we can use the same method we used for the lineage to create the attr. 
					//why provenance at this level
					//so we add to the attribute witness all the elements extracted from the normal witness
					attrWitness.addAll(this.enrichLineageWithAttributes(witness, tableName, attributeName));
				}

				if(!wheres.equals("")) {
					String[] whereArray = wheres.split(",");
					for(int k = 0; k < whereArray.length; ++k) {

						//get the attribute used at the left hand side of the where clauses
						String[] elements = whereArray[k].split("=")[0].trim().split("\\.");
						String tableName = elements[0];
						String attributeName = elements[1];
						attrWitness.addAll(this.enrichLineageWithAttributes(witness, tableName, attributeName));

						//now get the ones on the right hand side of the =
						elements = whereArray[k].split("=")[1].trim().split("\\.");

						//if this is not a join condition, and thus these elements are not 
						//a value in a table but a 'real' value, there are no attributes to use here
						if(elements.length < 2)
							continue;

						tableName = elements[0];
						attributeName = elements[1];
						attrWitness.addAll(this.enrichLineageWithAttributes(witness, tableName, attributeName));
					}
				}//end if

				//if we are here, it means that this witness is ready, we can add it to the witness basis
				attrWhyProv.add(attrWitness);
			}
			//if we are here, we finished one why provenance for one attribute tuple. Time to add it to the list of provenances
			returningWhyProvenances.add(attrWhyProv);
		}

		return returningWhyProvenances;
	}


	/** Adds the parameter witness set to the witness basis
	 * if the basis does not already contain it.
	 * 
	 * */
	private void addWitnessSetToWitnessBasis(List<Set<String>> witnessBasis,
			Set<String> witnesses) {

		//check if some other witness is equal to the one we are trying to add
		for(Set<String> witnessSet : witnessBasis) {
			//the assumption is at the beginning that the 
			//witness set is already inside the witness basis and is equal to this set
			boolean equals = true;
			for(String w: witnesses) {
				if(witnessSet.contains(w))
					continue;
				else {
					//if we found an element not present in the 
					//witnessSet, then the witnesses is a new witness set
					equals = false;
					break;
				}
			}


			if(equals)//if we found a witness that is equal to the
				//one we want to insert, we do not add it (it
				//would be a copy) and return
				return;
		}

		witnessBasis.add(witnesses);
	}



	/** Given a why provenance for one output tuple, computes the responsibility set of that tuple. 
	 * First we need to compute the why provenance of a tuple, then we can give that
	 * provenance as imput of this method. 
	 * <p>
	 * THE METHOD TO COMPUTE RESPONSIBILITY in this project
	 * </p>
	 * */
	public List<CausalityTuple> convertOneWhyProvToOneResponsibilitySet(List<Set<String>> whyProv) {
		// a list containing the monomials
		List<CausalityMonomial> monomialsList = new ArrayList<CausalityMonomial>();

		// convert the witness basis in a list of CausalityMonomials, our witnesses
		for(Set<String> witness : whyProv) {
			// create a new monomial
			CausalityMonomial mnm = new CausalityMonomial();
			// convert the witness to a monomial
			mnm.importListOfStringsAsMonomial(this.convertSetToArrayOfStrings(witness));
			// add this monomial to our list of monomials
			monomialsList.add(mnm);
		}

		// now enrich the monomials with the frequency of the tuples
		CausalityMonomial.enrichTuplesWithTheirFrequency(monomialsList);

		// now let us run the algorithm that computes the responsibility of each tuple
		return this.computeTuplesResponsibility(monomialsList);

	}

	/** computes and sets the fields 'normalizedResponsibility' of the tuples that are annotated with their responsibility*/
	public void computeNormalizedResponsibility(List<CausalityTuple> listOfResponsibleTuples) {
		double totalResponsibility = 0.0;
		for(CausalityTuple t : listOfResponsibleTuples) {
			totalResponsibility += t.responsibility;
		}

		if(totalResponsibility == 0)
			return;

		for(CausalityTuple t: listOfResponsibleTuples) {
			t.normalizedResponsibility = (double) t.responsibility / totalResponsibility;
		}
	}
	
	public void computeNormalizedShapleyValue(List<CausalityTuple> listOfResponsibleTuples) {
		double totalShapleyValue = 0.0;
		for(CausalityTuple t : listOfResponsibleTuples) {
			totalShapleyValue += t.shapleyValue;
		}
		
		if(totalShapleyValue == 0)
			return;
		
		for(CausalityTuple t: listOfResponsibleTuples) {
			t.normalizedShapleyValue = (double) t.shapleyValue / totalShapleyValue;
		}
				
		
	}


	public List<CausalityTuple> computeTuplesResponsibility(List<CausalityMonomial> witnessBasis) {
		// order the monomials in their order of weight
		Collections.sort(witnessBasis);
		// compute the responsibilities of the tuples
		return this.computeResponsibilityOfThisWitnessBasis(witnessBasis);
	}

	public List<CausalityTuple> computeResponsibilityOfThisWitnessBasis(List<CausalityMonomial> witnessBasis) {
		List<CausalityTuple> responsibleTuples = new ArrayList<CausalityTuple>();
		for(int i = 0; i < witnessBasis.size(); i++) {
			CausalityMonomial monomial = witnessBasis.get(i);
			for(int j = 0; j < monomial.variables.size(); ++j) {
				CausalityTuple t = monomial.variables.get(j);
				this.computeResponsibilityWithThisTupleAndThisMonomial(t, monomial, witnessBasis);
				CausalityTuple newT = new CausalityTuple(t);
				CausalityTuple.addTupleToList(responsibleTuples, newT);
			}
		}
		return responsibleTuples;

	}
	
	
	/** For each tuple in the provided lineage, it computes its corresponding Shapley value
	 * The witness basis of the corresponding output tuple is necessary to compute the result
	 * of function v. The assunction is that we are able to distinguish when a subset
	 * of the lineage is a witness or not. 
	 * 
	 * */
	public List<CausalityTuple> computeShapleyValues(Set<String> lineage, List<Set<String>> witnessBasis) {
		//first, get the power set of the lineage
		Set<Set<String>> lineagePowerSet = Sets.powerSet(lineage);
		List<CausalityTuple> shapleyTuples = new ArrayList<CausalityTuple>();
		
		// now that we have this, we can compute the shapley value for each tuple
		for(String t : lineage) {
			double tupleShapleyValue = 0;
			for(Set<String> B : lineagePowerSet) {
				if(B.contains(t))
					continue; // we want a B that does not contain the tuple t
				
				int v = this.computeShapleyDifference(B, t, witnessBasis);
				if(v == 1) {
					// we compute the second value for the shapley value
					// |B|!
					double firstVal = (double) CombinatoricsUtils.factorial(B.size()); 
					double secondVal = (double) CombinatoricsUtils.factorial(lineage.size() - B.size() - 1);
					double denominator = (double) CombinatoricsUtils.factorial(lineage.size());
					
					tupleShapleyValue += (double) (firstVal * secondVal) / denominator;
				}
			}
			// if we are here, we computed the shapley value for tuple t
			CausalityTuple sT = new CausalityTuple(t);
			sT.shapleyValue = tupleShapleyValue;
			shapleyTuples.add(sT);
		}
		return shapleyTuples;
	}

	/** Computes the value v(B \cup {t}) - v(B) used to compute the shapley value. It returns, at the end of the day,
	 * 0 or 1
	 * */
	private int computeShapleyDifference(Set<String> B, String t, List<Set<String>> witnessBasis) {
		Set<String> B1 = new HashSet<String>();
		B1.addAll(B);
		B1.add(t);
		
		// compute v(B \cup {t})
		int v1 = this.computeV(B1, witnessBasis);
		// compute v(B)
		int v2 = this.computeV(B, witnessBasis);
		
		return v1 - v2;
	}



	/** The method computes the function v that appears in the Shapley value. 
	 * This function, in this peculiar case, is 1 when the argument b generates the output,
	 * i.e., it is a witness, and 0 otherwise
	 * 
	 * <p>
	 * This is the STRONG assumption here, that makes this case easy (but it is not true in general)
	 * if the set B that we have here is one of the witnesses, then the 
	 * function v(B) return 1, since the output tuple is guaranteed to be in the output. 
	 * The problem is that I do not know if the parameter witnessBasis contains all possible witnesses,
	 * in particular I do not know if it contains also the minimal witnesses, thus
	 * there may be cases where B does not appear to be a witness while, in fact, it is a minimal witness. 
	 * I do not think, though, that in the cases considered i my experiment I can have a witness basis that is not 
	 * minimal 
	 * <p>
	 * */
	private int computeV(Set<String> b, List<Set<String>> witnessBasis) {
		if(b.size() == 0)
			return 0; // special case of the empty set, it can never be a witness
		for(Set<String> witness : witnessBasis) {
			if(witness.containsAll(b) && b.containsAll(witness)) // if B IS exactly equal to a witness 
				return 1;
			
			if(b.containsAll(witness)) // if B contains all the tuples of a witness, then it is still a witness
				return 1;
		}
		return 0;
		
	}

	public void computeResponsibilityWithThisTupleAndThisMonomialWrongOne(CausalityTuple t, CausalityMonomial monomial, List<CausalityMonomial> witnessBasis) {
		// first, reset the monomial
		CausalityMonomial.resetListOfMonomials(witnessBasis);
		// block it the monomial that contains this tuple
		monomial.blocked = true;
		t.blocked = true;

		// now, block the  other monomials that contain the tuple 
		// these monomials represent paths that pass through the tuple, thus they do not offer any 
		// node that can work as contingency
		for(CausalityMonomial cm : witnessBasis) {
			if(cm.containsThisTuple(t.tupleValue))
				cm.blocked = true;

		}

		// also, in the other monomials, block the tuples that are contained in the starting monomial
		for(CausalityTuple ct : monomial.variables) {
			for(CausalityMonomial cm : witnessBasis) {
				// if the monomial is already blocked, we don't need to do anything
				if(cm.blocked)
					continue;
				// else, we block the tuple
				cm.blockTuple(ct);
			}
		}

		// counter equal to the cardinality of the contingency
		int counter = 0;

		// now the iterative part of the algorithm

		// populate the list of monomials that are still to analyze
		List<CausalityMonomial> monomials = new ArrayList<CausalityMonomial>();
		for(CausalityMonomial cm : witnessBasis) {
			if(!cm.blocked)
				monomials.add(cm);
		}

		Collections.sort(monomials);

		while(!monomials.isEmpty()) {// while there are still monomials that can be exploited
			// get the first "free" monomial
			CausalityMonomial mnm = monomials.get(0);
			// block it
			mnm.blocked = true;
			// order it
			Collections.sort(mnm.variables);

			// look for the first tuple in this monomial non-blocked
			// since we ordered, we also get the tuple that is used the most, i.e. potentially it is at a 
			// crossroad in the join tree
			for(int i = 0; i < mnm.variables.size(); i++) {
				CausalityTuple mct = mnm.variables.get(i);
				if(mct.blocked)
					continue;
				mct.blocked = true;

				// block this tuple in the other monomials that contain it
				for(int j = 0; j < monomials.size(); ++j) {
					CausalityMonomial examinedMonomial = monomials.get(j);
					if(!examinedMonomial.blocked) {
						// block the tuple in that monomial
						examinedMonomial.blockTuple(mct);
					}
				}
				// increment the counter (we reach here only if we found a non-blocked tuple in some other witness
				// that can be inserted in the contingency)
				counter++;
				break; // since we found a tuple, this witness/monomial has done its part, now it is blocked and we
				// can look for other tuples somewhere else
			}

			// if we are here, we can update the list of monomials with only the monomials that are not blocked
			List<CausalityMonomial> newMonomials = new ArrayList<CausalityMonomial>();
			for(CausalityMonomial mm : monomials) {
				if(!mm.blocked)
					newMonomials.add(mm);
			}
			monomials = newMonomials;

		}

		// if we are here, the have the responsibility of the tuples
		t.cardinalityOfContingency = counter;
		t.responsibility = (double) 1 / (1 + counter);


	}

	public void computeResponsibilityWithThisTupleAndThisMonomial(CausalityTuple t, CausalityMonomial monomial, List<CausalityMonomial> witnessBasis) {
		// first, reset the monomial
		CausalityMonomial.resetListOfMonomials(witnessBasis);
		Collections.sort(witnessBasis);
		//get the list of all tuples contained in a witness together with this tuple. These are the tuples to block in all the witnesses
		List<CausalityTuple> tuplesToBlock = new ArrayList<CausalityTuple>();

		for(CausalityMonomial m : witnessBasis) {
			if(m.containsThisTuple(t.tupleValue)) {
				for(CausalityTuple addingTuple : m.variables) {
					tuplesToBlock.add(addingTuple);
				}
			}
		}

		// block these tuples in all witnesses
		for(CausalityMonomial m : witnessBasis) {
			for(CausalityTuple toBlock : tuplesToBlock) {
				m.blockTuple(toBlock);
			}
		}

		// now the iterative part of the algorithm
		// counter equal to the cardinality of the contingency
		int counter = 0;

		// list of monomials still not blocked
		List<CausalityMonomial> monomials = new ArrayList<CausalityMonomial>();
		for(CausalityMonomial cm : witnessBasis) {
			if(!cm.blocked) {
				monomials.add(cm);
			}
		}

		Collections.sort(monomials);

		while(!monomials.isEmpty()) {
			// get the first non-blocked monomial
			CausalityMonomial mnm = monomials.get(0);

			// order it
			Collections.sort(mnm.variables);

			// take the first non-blocked tuple in the monomial. It is also the non-blocked tuple that is used the most
			// block it, and the other tuples appearing together with it in all monomials in all monomials
			// the reason is that, if I remove it from the database, all the other non-blocked tuples won't be able to 
			// create the result. In this way I make sure that the contingency is minimal
			for(int i = 0; i < mnm.variables.size(); i++) {
				CausalityTuple mct = mnm.variables.get(i);
				if(mct.blocked)
					continue;
				// if we are here, we have our tuple. Repeat the process
				tuplesToBlock.clear();

				for(CausalityMonomial m : monomials) {
					if(m.containsThisTuple(mct.tupleValue)) {
						for(CausalityTuple addingTuple : m.variables) {
							if(!addingTuple.blocked)
								tuplesToBlock.add(addingTuple);
						}
					}
				}

				// block these tuples in all witnesses
				for(CausalityMonomial m : monomials) {
					for(CausalityTuple toBlock : tuplesToBlock) {
						m.blockTuple(toBlock);
					}
				}
				// if we are here, we found one tuple that can go into the contingency. Increase the counter, and re-start the process
				counter++;
				break;

			}
			// if we are here, we can update the list of monomials with only the monomials that are not blocked
			List<CausalityMonomial> newMonomials = new ArrayList<CausalityMonomial>();
			for(CausalityMonomial mm : monomials) {
				if(!mm.blocked)
					newMonomials.add(mm);
			}
			monomials = newMonomials;
		}
		
		// if we are here, the have the responsibility of the tuples
		t.cardinalityOfContingency = counter;
		t.responsibility = (double) 1 / (1 + counter);
	}



	public String[] convertSetToArrayOfStrings(Set<String> witness) {
		String[] toReturn = new String[witness.size()];
		int i = 0;
		for(String s : witness) {
			toReturn[i] = s;
			i++;
		}
		return toReturn;
	}

	/** This method converts one provenance polynomial in a set of tuples, a list of objects that I called
	 * causality tuples, that contain their respective responsibility. 
	 * 
	 * @deprecated not correct as method since it does not remove the coefficients from the monomials
	 * */
	public List<CausalityTuple> convertOneHowProvenanceToOneCausality(String[][] provenancePolynomial) {
		// a list containing each monomial
		List<CausalityMonomial> monomials = new ArrayList<CausalityMonomial>();

		// convert these strings to CausalityMonomials
		for(String[] monomial : provenancePolynomial) {
			CausalityMonomial mnm = new CausalityMonomial();
			mnm.importListOfStringsAsMonomial(monomial);
			monomials.add(mnm);
		}

		// now convert them to a tree
		CausalityTuple root = this.convertMonomialsToTreeStructure(monomials);
		List<CausalityTuple> listOfTuples = this.computeResponsibility(root);
		return listOfTuples;
	}

	public List<CausalityTuple> computeResponsibility(CausalityTuple root) {
		Queue<CausalityTuple> queue = new LinkedList<CausalityTuple>();
		List<CausalityTuple> listOfTuples = new ArrayList<>();
		queue.add(root);

		while(!queue.isEmpty()) {
			CausalityTuple node = queue.poll();
			node.computeMyResponsibility();
			listOfTuples.add(node);
		}

		return listOfTuples;

	}

	public CausalityTuple convertMonomialsToTreeStructure(List<CausalityMonomial> monomials) {
		// create the root and add it to the queue
		CausalityTuple root = new CausalityTuple("");
		Queue<CausalityTuple> queue = new LinkedList<CausalityTuple>();
		queue.add(root);

		while(!queue.isEmpty()) {// while we still have nodes to insert
			CausalityTuple node = queue.poll();
			List<CausalityTuple> level = CausalityMonomial.getLevelOfHierarchy(monomials, node.tupleValue);
			if(level.size() > 0) {
				node.children = level;
				for(CausalityTuple n : level) {
					// TODO check if this is really what happens, i.e. if we have a tree or a lattice
					n.parent = node;
				}
				queue.addAll(level);				
			}
			// else do nothing, this is a leaf
		}

		return root;

	}


	/** Test main*/
	public static void main(String[] args) {
		QueryToPolynomialGenerator generator = new QueryToPolynomialGenerator();
		//compute the provenance polynomials
		HashMap<String, String[][]> polynomials = generator.generateHowProvenancePolynomial(null);
		System.out.print(polynomials);

	}

	public String getQueryFile() {
		return queryFile;
	}

	public void setQueryFile(String queryFile) {
		this.queryFile = queryFile;
	}

	public HashMap<String, String[][]> getProvenancePolynomials() {
		return provenancePolynomials;
	}

	public void setProvenancePolynomials(HashMap<String, String[][]> provenancePolynomials) {
		this.provenancePolynomials = provenancePolynomials;
	}

	public String getTupleIdsFile() {
		return tupleIdsFile;
	}

	public void setTupleIdsFile(String tupleIdsFile) {
		this.tupleIdsFile = tupleIdsFile;
	}
}
