package it.unipd.dei.ims.creditdistribution.datastructures;

public class Author {
	
	private String name;
	private int citationCount;
	private double lineageCredit;
	private double whyCredit;
	private double howCredit;
	private double responsibility;
	private double normalizedResponsibility;
	private double shapleyValue;
		

	public Author() {
		
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getCitationCount() {
		return citationCount;
	}


	public void setCitationCount(int citationCount) {
		this.citationCount = citationCount;
	}


	public double getLineageCredit() {
		return lineageCredit;
	}


	public void setLineageCredit(double lineageCredit) {
		this.lineageCredit = lineageCredit;
	}


	public double getWhyCredit() {
		return whyCredit;
	}


	public void setWhyCredit(double whyCredit) {
		this.whyCredit = whyCredit;
	}


	public double getHowCredit() {
		return howCredit;
	}


	public void setHowCredit(double howCredit) {
		this.howCredit = howCredit;
	}


	@Override
	public String toString() {
		return "Author [name=" + name + ", citationCount=" + citationCount + ", lineageCredit=" + lineageCredit
				+ ", whyCredit=" + whyCredit + ", howCredit=" + howCredit + "]";
	}


	public double getResponsibility() {
		return responsibility;
	}


	public void setResponsibility(double responsibility) {
		this.responsibility = responsibility;
	}


	public double getNormalizedResponsibility() {
		return normalizedResponsibility;
	}


	public void setNormalizedResponsibility(double normalizedResponsibility) {
		this.normalizedResponsibility = normalizedResponsibility;
	}


	public double getShapleyValue() {
		return shapleyValue;
	}


	public void setShapleyValue(double shapleyValue) {
		this.shapleyValue = shapleyValue;
	}
	
	
}
