package it.unipd.dei.ims.creditdistribution.distributors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.jena.ext.com.google.common.base.Stopwatch;

import it.unipd.dei.ims.creditdistribution.datastructures.CausalityTuple;
import it.unipd.dei.ims.creditdistribution.datastructures.TableAtlas;
import it.unipd.dei.ims.creditdistribution.provenance.QueryToPolynomialGenerator;
import it.unipd.dei.ims.datastructure.ConnectionHandler;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** Given the information on the provenance, this class distributes the credit
 * with the different strategies. 
 * 
 * */
public class CreditDistributor {


	private String jdbcString;

	/** Map used to deal with the properties/fields of this class */
	protected Map<String, String> map;

	public static String separator = "|";

	public static String provenance_column_suffix = separator + "prov";

	public static String prov_col = "\"c" + separator + provenance_column_suffix + "\"";

	private String SQL_ADD_CREDIT = "UPDATE poffare SET lineage_credit = lineage_credit + ? WHERE " +
			"\"c||prov\" = ?";

	private String SQL_ADD_CREDIT_WHY_PROV = "UPDATE poffare SET why_credit = why_credit + ? WHERE " +
			"\"c||prov\" = ?";

	private String SQL_ADD_CREDIT_HOW_PROV = "UPDATE poffare SET how_credit = how_credit + ? WHERE " +
			"\"c||prov\" = ?";
	
	private String SQL_ADD_RESP = "UPDATE poffare SET resp = resp + ? WHERE " +
			"\"c||prov\" = ?";
	
	private String SQL_ADD_NORM_RESP = "UPDATE poffare SET norm_resp = norm_resp + ? WHERE " +
			"\"c||prov\" = ?";
	
	private String SQL_ADD_SHAPLEY = "UPDATE poffare SET shapley = shapley + ? WHERE " +
			"\"c||prov\" = ?";
	
	private String SQL_ADD_NORM_SHAPLEY = "UPDATE poffare SET norm_shapley = norm_shapley + ? WHERE " +
			"\"c||prov\" = ?";

	private Connection connection;
	
	/** a string which can be initialized through the property
	 * output.log.file, on which you can build a file writer if you want*/
	private String outputLogDir;
	
	
	private String host;
	private String port;
	private String database;
	private String user;
	private String password;
	

	public CreditDistributor() {
		map = PropertiesUsefulMethods.getPropertyMap("properties/main.properties");
		
		this.host = map.get("host");
		this.port=map.get("port");
		this.database=map.get("database");
		this.user=map.get("user");
		this.password=map.get("password");
		
		
		jdbcString = "jdbc:postgresql://" + this.host + ":"+ this.port + "/" + this.database + "?user=" + this.user + "&password=" + this.password;
		this.outputLogDir = map.get("output.log.directory");

		try {
			connection = ConnectionHandler.createConnectionAsOwner(this.jdbcString, this.getClass().getName());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/** You should call this when you are done, to close the connection to the DB
	 * */
	public void shutDown() {
		try {
			ConnectionHandler.closeConnectionIfOwner(this.getClass().getName());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	/** Given the information contained in the TupleAtlas (the total credit to give
	 * to each tuple for each table) we perform the update.
	 * 
	 * @param strategy the distribution strategy that we used (you need this information to 
	 * update the correct column). You can chose one between 'lineage', 'why' and 'how', corresponding
	 * to the three strategies
	 * @throws SQLException if your update is incorrect
	 * */
	public void updateDatabaseFromAtlas(TableAtlas atlas, String strategy) throws SQLException {
		String sql_update = "";
		if(strategy.equals("lineage")) {
			sql_update = this.SQL_ADD_CREDIT;
		} else if(strategy.equals("why")) {
			sql_update = this.SQL_ADD_CREDIT_WHY_PROV;
		} else if(strategy.equals("how")) {
			sql_update = this.SQL_ADD_CREDIT_HOW_PROV;
		} else if(strategy.equals("resp")) {
			sql_update = this.SQL_ADD_RESP;
		} else if(strategy.equals("norm_resp")) {
			sql_update = this.SQL_ADD_NORM_RESP;
		} else if(strategy.equals("shapley")) {
			sql_update = this.SQL_ADD_SHAPLEY;
		} else if(strategy.equals("norm_shapley")) {
			sql_update = this.SQL_ADD_NORM_SHAPLEY;
		}

		PreparedStatement pS = null;
		//for every table in the atlas
		for(Entry<String, Map<String, Double>> table : atlas.getAtlas().entrySet()) {
			String tableName = table.getKey();
			//set the correct name of the table
			String sql_update_ = sql_update.replaceAll("poffare", tableName);
			pS = this.connection.prepareStatement(sql_update_);
			//now for every tuple in the table
			for(Entry<String, Double> tuple : table.getValue().entrySet()) {
				//get the name of the tuple
				String tupleId = tuple.getKey();
				//get the total quantity of credit for this tuple
				double credit = tuple.getValue();

				pS.setDouble(1, credit);//set the quantity of credit to give
				pS.setString(2, tableName + "|" + tupleId);//set the id of the tuple to update

				pS.addBatch();
			}
			//now we can update everything regarding this table
			int[] affectedRecords = pS.executeBatch();
//			System.out.println("[DEBUG] updated " + affectedRecords.length + " records");

		}//end of the updates
		if(pS != null) {
			pS.close();
		}
	}



	/** Given the lineages of a bunch of tuples and a map containing the credit for each one of
	 * those tuples, this method distributes the credit into the database.
	 * <p>
	 * This is a basic 'dummy' method, which distributes a quantity of credit equal
	 * to 1 from every output tuple. In the future (wonderful place, you should visit it sometimes)
	 * we will define the quantity k of credit in some more sophisticated way.
	 * 
	 * @param lineages a map of lineages as produced by {@link QueryToPolynomialGenerator.convertPolynomialsToLineage}
	 * */
	@Deprecated
	public void distributeCreditWithLineageBasic(Map<String, Set<String>> lineages) {
		Stopwatch lineageTimer = Stopwatch.createStarted();
		try {

			//for every tuple, we need to process the distribution
			for(Entry<String, Set<String>> e : lineages.entrySet()) {
				//get the tuple
				String tuple = e.getKey();
				//get the set of provenance tokens
				Set<String> lineage = e.getValue();
				//distrubte the credit. As you see, we use 1 as the credit to distribute for this tuple
				this.distributeCreditWithLineageBasicOneTuple(lineage, this.connection, 1);

			}
			//we have finished the distribution
			System.out.println("ended the lineage distribution in " + lineageTimer.stop());



		} catch (SQLException e) {
			System.err.println("error in dealing with the database. jdbc string: " + this.jdbcString);
			e.printStackTrace();
		}
	}

	/** Similar to {@link distributeCreditWithLineageBasic}, but here we are using a smarter
	 * way to update the database, so it should be faster.
	 * 
	 * */
	public void distributeCreditWithLineageBasicButSmarter(Map<String, Set<String>> lineages) {
		
		//map to keep track of the distribution of the credit
		TableAtlas atlas = new TableAtlas();

		//here we decide how to distribute the credit
		//for every output tuple, we need to process the distribution
		for(Entry<String, Set<String>> e : lineages.entrySet()) {
			//get the tuple
			String tuple = e.getKey();
			//get the set of provenance tokens
			Set<String> lineage = e.getValue();
			//distrubte the credit. As you see, we use 1 as the credit to distribute for this tuple
			this.distributeCreditWithLineageAndAtlas(lineage, 1, atlas);

		}
		try {
			//update now the database
			this.updateDatabaseFromAtlas(atlas, "lineage");
			//we have finished the distribution

		} catch (SQLException e) {
			System.err.println("error in dealing with the database. jdbc string: " + this.jdbcString);
			e.printStackTrace();
		}
	}

	/** Performs the update of the lineage for one output tuple
	 * @throws SQLException */
	@Deprecated
	public void distributeCreditWithLineageBasicOneTuple(Set<String> lineage, Connection connection, double k) throws SQLException {
		//the lineage set contains strings, we distribute the credit k equally among the sets
		//first, compute the portion of credit which goes to each tuple 
		double creditPortion = (double) k / lineage.size();
		PreparedStatement stmt = null;
		for(String token : lineage) {
			String[] parts = token.split("\\|");
			String table = parts[0];//name of the table

			//now update every tuple in the lineage with the quantity of credit
			//since we work on different tables, we need to set the name of the table first in the SQL command
			String current_SQL = SQL_ADD_CREDIT.replaceAll("poffare", table);

			stmt = connection.prepareStatement(current_SQL);//prepare the statement
			stmt.setDouble(1, creditPortion);//set the quantity of credit to give
			stmt.setString(2, token);//set the id of the tuple to update
			stmt.executeUpdate();
		}

	}

	/** This method utilizes a TableAtlas to distribute the credit of one output tuple using the lineage strategy.
	 * 
	 * @param k the total quantity of credit associated to this output tuple*/
	public void distributeCreditWithLineageAndAtlas(Set<String> lineage, 
			double k, 
			TableAtlas atlas)  {
		//the lineage set contains strings, we distribute the credit k equally among the sets

		//first, compute the portion of credit which goes to each tuple 
		double creditPortion = (double) k / lineage.size();

		for(String token : lineage) {//for every tuple in this lineage
			String[] parts = token.split("\\|");
			String tableName = parts[0];//name of the table
			String tupleId = parts[1];//id of the tuple
			//add the credit to the atlas
			atlas.addCredit(tableName, tupleId, creditPortion);

		}
	}


	/** Distributes credit with the why-provenance-based strategy.
	 * This is the version where every output tuple has a recognized credit of 1.
	 * 
	 * @param base a list of lists of sets representing the witness base as defined by the query
	 * */
	@Deprecated
	public void distributeCreditWithWhyProvenanceBasic(List<List<Set<String>>> base) {
		//start the clock and hurry
		Stopwatch whyTimer = Stopwatch.createStarted();
		for(List<Set<String>> witnessBasis : base) {
			//for every tuple of the output
			try {
				this.distributeCreditWithWhyProvenanceForOneTuple(witnessBasis, this.connection, 1);
			} catch (SQLException e) {
				System.err.println("error with witness basis " + witnessBasis);
				e.printStackTrace();
			}
		}
		//we have finished the distribution
		System.out.println("ended the why-provenance-based distribution in " + whyTimer.stop());
	}

	/** See {@link  distributeCreditWithWhyProvenanceBasic}, but smarter since we 
	 * update things to the database via batch
	 * */
	public void distributeCreditWithWhyProvenanceBasicButSmarter(List<List<Set<String>>> base) {
		
		//prepare the atlas for the credit
		TableAtlas atlas = new TableAtlas();

		for(List<Set<String>> witnessBasis : base) {
			this.distributeCreditWithWhyProvAndAtlas(witnessBasis, 1, atlas);
		}
		//for every tuple of the output
		try {
			//we have finished the distribution
			//update now the database
			this.updateDatabaseFromAtlas(atlas, "why");
			//we have finished the distribution
		} catch (SQLException e) {
			System.err.println("error with the update");
			e.printStackTrace();
		}
	}

	/** Distribute the credit k to the atlas using the why provenance */
	public void distributeCreditWithWhyProvAndAtlas(List<Set<String>> witnessBasis, double k, TableAtlas atlas) {
		//get the portion that goes to every witness
		double firstPortion = (double) k / witnessBasis.size();

		for(Set<String> witness : witnessBasis) {
			//now we are with one specific witness set. To every tuple of this set goes a sub-portion
			double creditPortion = (double) firstPortion / witness.size();
			//now, for every witness in this witness set
			for(String token : witness) {
				String[] parts = token.split("\\|");
				String tableName = parts[0];//name of the table
				String tupleId = parts[1];//id of the tuple
				atlas.addCredit(tableName, tupleId, creditPortion);//update the credit to this tuple
			}
		}
	}
	
	public void distributeCreditViaResponsibility(List<CausalityTuple> tuples) {
		TableAtlas atlas = new TableAtlas();
		// for every tuple, take notice of the credit
		for(CausalityTuple t : tuples) {
			this.distributeCreditWithResponsibilityAndAtlas(t, atlas);
		}
		
		try {
			//we have finished the distribution
			//update now the database
			this.updateDatabaseFromAtlas(atlas, "resp");
			//we have finished the distribution
		} catch (SQLException e) {
			System.err.println("error with the update");
			e.printStackTrace();
		}
		
	}
	
	public void distributeCreditViaNormalizedResponsibility(List<CausalityTuple> tuples) {
		TableAtlas atlas = new TableAtlas();
		// for every tuple, take notice of the credit
		for(CausalityTuple t : tuples) {
			this.distributeCreditWithNormalizedResponsibilityAndAtlas(t, atlas);
		}
		
		try {
			//we have finished the distribution
			//update now the database
			this.updateDatabaseFromAtlas(atlas, "norm_resp");
			//we have finished the distribution
		} catch (SQLException e) {
			System.err.println("error with the update");
			e.printStackTrace();
		}
		
	}
	
	public void distributeCreditViaNormalizedShapleyValue(List<CausalityTuple> tuples) {
		TableAtlas atlas = new TableAtlas();
		
		for(CausalityTuple t : tuples) {
			this.distributeCreditWithNormalizedShapleyValueAndAtlas(t, atlas);
		}
		
		try {
			//we have finished the distribution
			//update now the database
			this.updateDatabaseFromAtlas(atlas, "norm_shapley");
			//we have finished the distribution
		} catch (SQLException e) {
			System.err.println("error with the update");
			e.printStackTrace();
		}
		
	}
	
	public void distributeCreditViaShapleyValue(List<CausalityTuple> tuples) {
		TableAtlas atlas = new TableAtlas();
		
		for(CausalityTuple t : tuples) {
			this.distributeCreditWithShapleyValueAndAtlas(t, atlas);
		}
		
		try {
			//we have finished the distribution
			//update now the database
			this.updateDatabaseFromAtlas(atlas, "shapley");
			//we have finished the distribution
		} catch (SQLException e) {
			System.err.println("error with the update");
			e.printStackTrace();
		}
		
	}
	
	public void distributeCreditWithResponsibilityAndAtlas(CausalityTuple t, TableAtlas atlas) {
		String[] parts = t.tupleValue.split("\\|");
		String tableName = parts[0];
		String tupleId = parts[1];
		atlas.addCredit(tableName, tupleId, t.responsibility);
 	}
	
	public void distributeCreditWithNormalizedResponsibilityAndAtlas(CausalityTuple t, TableAtlas atlas) {
		String[] parts = t.tupleValue.split("\\|");
		String tableName = parts[0];
		String tupleId = parts[1];
		atlas.addCredit(tableName, tupleId, t.normalizedResponsibility);
 	}
	
	public void distributeCreditWithNormalizedShapleyValueAndAtlas(CausalityTuple t, TableAtlas atlas) {
		String[] parts = t.tupleValue.split("\\|");
		String tableName = parts[0];
		String tupleId = parts[1];
		atlas.addCredit(tableName, tupleId, t.normalizedShapleyValue);
 	}
	
	public void distributeCreditWithShapleyValueAndAtlas(CausalityTuple t, TableAtlas atlas) {
		String[] parts = t.tupleValue.split("\\|");
		String tableName = parts[0];
		String tupleId = parts[1];
		atlas.addCredit(tableName, tupleId, t.shapleyValue);
 	}


	@Deprecated
	public void distributeCreditWithWhyProvenanceForOneTuple(List<Set<String>> witnessBasis, Connection connection, double k) throws SQLException {
		//get the portion that goes to every witness
		double firstPortion = (double) k / witnessBasis.size();
		PreparedStatement stmt = null;
		for(Set<String> witness : witnessBasis) {
			//now we are with one specific witness set. To every tuple of this set goes a sub-portion
			double subPortion = (double) firstPortion / witness.size();
			//now, for every witness in this witness set
			for(String token : witness) {
				String[] parts = token.split("\\|");
				String table = parts[0];//name of the table

				//now update every tuple in the lineage with the quantity of credit
				//since we work on different tables, we need to set the name of the table first in the SQL command
				String current_SQL = SQL_ADD_CREDIT_WHY_PROV.replaceAll("poffare", table);

				stmt = connection.prepareStatement(current_SQL);//prepare the statement
				stmt.setDouble(1, subPortion);//set the quantity of credit to give
				stmt.setString(2, token);//set the id of the tuple to update
				stmt.executeUpdate();
			}
		}
	}


	/**Performs the distribution of a basic credit of 1 for every tuple contained in the polynomials 
	 * 
	 * 
	 * @param polynomials The provenance polynomials generated through the how provenance algorithm.
	 * One for each output tuple
	 * */
	public void distributeCreditWithHowProvenanceBasicButSmarter(HashMap<String, String[][]> polynomials) {
		//run, fools!
		TableAtlas atlas = new TableAtlas();

		for(Entry<String, String[][]> en : polynomials.entrySet()) {
			//for each output tuple
			String tuple = en.getKey();//name of the tuple
			String[][] polynomial = en.getValue();//polynomial of this tuple
			this.distributeCreditWithHowProvAndAtlas(polynomial, 1, atlas);
		}
		//we have finished the distribution

		try {
			//update now the database
			this.updateDatabaseFromAtlas(atlas, "how");
		} catch (SQLException e) {
			System.err.println("Errors in the how-distribution");
			e.printStackTrace();
		}
	}

	public void distributeCreditWithHowProvAndAtlas(String[][] polynomial, double k, TableAtlas atlas) {
		//divide the credit among the monomials
		double firtsPortion = (double) k / polynomial.length;
		//now, for every monomial
		for(String[] monomial : polynomial) {
			//take the monomial size
			double creditPortion = (double) firtsPortion / monomial.length;
			//now, for every token in the monomial
			for(String token : monomial) {
				String[] parts = token.split("\\|");
				String tableName = parts[0];//name of the table

				String tupleId = parts[1];//id of the tuple
				atlas.addCredit(tableName, tupleId, creditPortion);//update the credit to this tuple
			}
		}
	}

	/** @param polynomials The provenance polynomials generated through the how provenance algorithm.
	 * */
	@Deprecated
	public void distributeCreditWithHowProvenanceBasic(HashMap<String, String[][]> polynomials) {
		//run, fools!
		Stopwatch howTimer = Stopwatch.createStarted();
		//for each output tuple
		for(Entry<String, String[][]> en : polynomials.entrySet()) {
			String tuple = en.getKey();
			String[][] polynomial = en.getValue();
			try {
				this.distributeCreditWithHowProvenanceForOneTuple(polynomial, this.connection, 1);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		//we have finished the distribution
		System.out.println("ended the how-provenance-based distribution in " + howTimer.stop());

	}

	@Deprecated
	public void distributeCreditWithHowProvenanceForOneTuple(String[][] polynomial, 
			Connection connection, 
			double k) 
					throws SQLException {
		//divide the credit among the monomials
		double firtsPortion = (double) k / polynomial.length;
		PreparedStatement stmt = null;
		//now, for every monomial
		for(String[] monomial : polynomial) {
			//take the monomial size
			double realPortion = (double) firtsPortion / monomial.length;
			//now, for every token in the monomial
			for(String token : monomial) {
				String[] parts = token.split("\\|");
				String table = parts[0];//name of the table

				//now update every tuple in the lineage with the quantity of credit
				//since we work on different tables, we need to set the name of the table first in the SQL command
				String current_SQL = SQL_ADD_CREDIT_HOW_PROV.replaceAll("poffare", table);

				stmt = connection.prepareStatement(current_SQL);//prepare the statement
				stmt.setDouble(1, realPortion);//set the quantity of credit to give
				stmt.setString(2, token);//set the id of the tuple to update
				stmt.executeUpdate();
			}
		}
	}



	/** Test main for my private debugging
	 * 
	 * */
	public static void main(String[] args) {
		System.out.println("starting the redistribution of credit");
		//create the generator
		QueryToPolynomialGenerator generator = new QueryToPolynomialGenerator();
		System.out.println("using query file at path: " + generator.getQueryFile());

		//compute the provenance polynomials
		HashMap<String, String[][]> polynomials = generator.generateHowProvenancePolynomial(null);

		CreditDistributor distributor = new CreditDistributor();


		//////////////////////////// HOW PROVENANCE ////////////////////
		//		distributor.distributeCreditWithHowProvenanceBasic(polynomials);

		//////////////////////////// LINEAGE ////////////////////
		//compute the lineage 
		Map<String, Set<String>> lineageMap = generator.convertPolynomialsToLineage(polynomials);
		//distribute with lineage
		distributor.distributeCreditWithLineageBasic(lineageMap);

		////////////////////////// WHY PROVENANCE ////////////////////
		//		List<List<Set<String>>> whitnessBase = generator.convertPolynomialsToWhyProvenance(polynomials);
		//		distributor.distributeCreditWithWhyProvenanceBasic(whitnessBase);


		distributor.shutDown();
	}

	public String getOutputLogDir() {
		return outputLogDir;
	}

	public void setOutputLogDir(String outputLogDir) {
		this.outputLogDir = outputLogDir;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

}
