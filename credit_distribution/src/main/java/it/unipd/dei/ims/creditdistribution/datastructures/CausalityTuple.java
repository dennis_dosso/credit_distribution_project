package it.unipd.dei.ims.creditdistribution.datastructures;

import java.util.ArrayList;
import java.util.List;

/** Represents a tuple with the necessary information that can be used 
 * to compute causality and responsibility
 * 
 * 
 * */
public class CausalityTuple implements Comparable<CausalityTuple> {

	public CausalityTuple parent;
	
	public List<CausalityTuple> children;
	
	public double responsibility;
	
	public double normalizedResponsibility;
	
	/** represents the exponent of this tuple as a variable in a monomial*/
	public int exponent;
	
	/** represents the number of times a tuple appears in a witness basis */
	public int frequency;
	
	/** the provenance token of the tuple being represented
	 * */
	public String tupleValue;
	
	public boolean used;
	
	public int numnberOfSiblings;
	
	public int cardinalityOfContingency;
	
	public boolean blocked;
	
	public double shapleyValue, normalizedShapleyValue;
	
	public CausalityTuple(String tupleValue) {
		responsibility = 0;
		this.children = new ArrayList<CausalityTuple>();
		exponent = 1;
		this.tupleValue = tupleValue;
		used = false;
		numnberOfSiblings = 0;
		cardinalityOfContingency = 0;
		frequency = 0;
		blocked = false;
		normalizedResponsibility = 0;
		
		shapleyValue = 0;
		normalizedShapleyValue = 0;
	}
	
	public CausalityTuple(CausalityTuple t) {
		this.responsibility = t.responsibility;
		exponent = t.exponent;
		this.tupleValue = t.tupleValue;
		used = t.used;
		numnberOfSiblings = t.numnberOfSiblings;
		cardinalityOfContingency = t.cardinalityOfContingency;
		frequency = t.frequency;
		blocked = t.blocked;
		normalizedResponsibility = t.normalizedResponsibility;
		shapleyValue = t.shapleyValue;
	}
	
	public void computeMyResponsibility() {
		if(this.parent == null) {
			// we are the root
			this.numnberOfSiblings = 0;
			this.responsibility = 0;
		}
		else {
			this.numnberOfSiblings = this.parent.getNumberOfChildren() - 1;
			int contingency = this.numnberOfSiblings + this.parent.numnberOfSiblings;
			this.responsibility = (double) 1 / (1 + contingency);
		}
	}
	
	
	public int getNumberOfChildren() {
		return this.children.size();
	}

	/**The compareTo() method will return a negative int if called with a Person having a greater last name than this, zero if the same last name, and positive otherwise.
	 * 
	 * BUT I want a DESC sort, so I will do the opposite*/
	@Override
	public int compareTo(CausalityTuple o) {
		if(this.frequency == o.frequency)
			return 0;
		else if(this.frequency > o.frequency)
			return -1;
		else 
			return 1;
	}
	
	/** Add a tuple to a list of tuples only if it is not already present
	 * */
	public static void addTupleToList(List<CausalityTuple> list, CausalityTuple t) {
		for(CausalityTuple tt : list) {
			if(tt.tupleValue.equals(t.tupleValue))
				return;
		}
		list.add(t);
	}
	
	public String toString() {
		return this.tupleValue + ",f:" + this.frequency + ",blocked:" + this.blocked;
	}
}
