package it.unipd.dei.ims.creditdistribution.datastructures;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

/** This class manages the efficient distribution of credit among tuples. It deals with the 
 * tables in a database, the tuples of each table and the credit associated to every tuple. 
 * This class was necessary because we have to deal with many updates. To update every tuple */
public class TableAtlas {

	/** this is the Atlas, i.e. a map of names of tables. Every key is a name
	 * of a table. The values are other maps, representing the table. The keys of these
	 * maps are the ids of the tuples. The values are the credit associated to those tuples*/
	private Map<String, Map<String, Double>> atlas;
	
	public TableAtlas() {
		this.atlas = new HashMap<String, Map<String, Double>>();
	}
	
	
	/** Given the name of a table, the id of a tuple and the quantity of credit, it adds that
	 * credit to the tuple in the table
	 * */
	public void addCredit(String tableName, String tupleId, double credit_) {
		//first, check if the table is already present in the atlas
		Map<String, Double> table = this.atlas.get(tableName);
		if(table == null) {
			//this table was never seen before, create it
			table = new HashMap<String, Double>();
			//put the credit in the table
			table.put(tupleId, credit_);
		} else {
			//we have the table, let's see if we have the tuple
			if(table.containsKey(tupleId)) {
				//we have the tuple, let us update the credit
				double credit = table.get(tupleId);
				credit = credit + credit_;
				table.put(tupleId, credit);
			} else {
				//we are seeing this tuple for the first time
				table.put(tupleId, credit_);
			}
		}
		//add/update to the atlas
		atlas.put(tableName, table);
	}
	


	public Map<String, Map<String, Double>> getAtlas() {
		return atlas;
	}


	public void setAtlas(Map<String, Map<String, Double>> atlas) {
		this.atlas = atlas;
	}
	
}
