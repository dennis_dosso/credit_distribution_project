package it.unipd.dei.ims.creditdistribution.polynomials.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class PolynomialsProducer {

	/** A second strategy because I wanted to create more complex polynomials (still tentative).
	 * <p>
	 * 
	 * */
	public static List<HashMap<String, String[][]>> produceFancySyntheticPolynomials(int numOfPolynomialsYouWant) {
		Random rand = new Random();
		List<HashMap<String, String[][]>> polynomialsList = new ArrayList<HashMap<String, String[][]>>();

		//a counter
		int num_of_tuples = 0;

		for(int n = 0; n < numOfPolynomialsYouWant; ++n) {

			//create the polynomial
			HashMap<String, String[][]> map = new HashMap<String, String[][]>();

			String variable_1 = "family|" + rand.nextInt(796);

			int numberOfMonomials = 1 + rand.nextInt(6);

			String[][] polynomial = new String[numberOfMonomials][];
			String[] monomial;
			
			for(int j = 0; j < numberOfMonomials; ) {
				
				//every time I change variables 2 and 3
				int var_2 = rand.nextInt(973);
				String variable_2 = "contributor2family|" + var_2;
				
				
				int var_3 = rand.nextInt(1146);
				String variable_3 = "contributor|" + var_3;

				//I am inside a monomial now
				//decide how many times a variable appears (exponent)
				int times_var_1 = 1 + rand.nextInt(4);
				int times_var_2 = 1 + rand.nextInt(4);
				int times_var_3 = 1 + rand.nextInt(4);

				monomial = new String[times_var_1 + times_var_2 + times_var_3];
				int i = 0;

				//populate the monomial
				for(int k = 0; k < times_var_1; k++) {
					monomial[i] = variable_1;
					i++;
				}

				for(int k = 0; k < times_var_2; k++) {
					monomial[i] = variable_2;
					i++;
				}

				for(int k = 0; k < times_var_3; k++) {
					monomial[i] = variable_3;
					i++;
				}

				//save the monomial
				int repeat_the_monomial = 1 + rand.nextInt(3);
				for(int k = 0; k < repeat_the_monomial; ++k) {
					polynomial[j] = monomial;
					j++;
					if(j == numberOfMonomials)
						break;

				}
			} // end for
			//if we are here, we have our polynomial
			map.put(num_of_tuples + "", polynomial);
			num_of_tuples++;


			polynomialsList.add(map);
		}
		
		return polynomialsList;

	}
	
	/** This is the method that we used at the end.
	 * <p>
	 * 
	 * */
	public static List<HashMap<String, String[][]>> produceSyntheticPolynomials(int numOfPolynomialsYouWant) {
		Random rand = new Random();
		List<HashMap<String, String[][]>> polynomialsList = new ArrayList<HashMap<String, String[][]>>();

		//a counter
		int num_of_tuples = 0;

		for(int n = 0; n < numOfPolynomialsYouWant; ++n) {
			//create the polynomial
			HashMap<String, String[][]> map = new HashMap<String, String[][]>();
			// decide how many monomials we want
			int numberOfMonomials = 1 + rand.nextInt(6);
			
			String[][] polynomial = new String[numberOfMonomials][];
			// decide the first family tuple
			String variable_1 = "family|" + rand.nextInt(796);
			
			for(int j = 0; j < numberOfMonomials; ) {
				// create the monomial
				String[] monomial;
				// decide if we want to keep the family tuple
				if(Math.random() > 0.2)
					variable_1 = "family|" + rand.nextInt(796);
				
				String variable_2 = "contributor2family|" + rand.nextInt(973);
				
				String variable_3 = "contributor|" + rand.nextInt(1146);;
				
				//I am inside a monomial now
				//decide how many times a variable appears (exponent)
				int times_var_1 = 1 + rand.nextInt(4);
				int times_var_2 = 1 + rand.nextInt(4);
				int times_var_3 = 1 + rand.nextInt(4);
				
				monomial = new String[times_var_1 + times_var_2 + times_var_3];
				int i = 0;
				
				//populate the monomial
				for(int k = 0; k < times_var_1; k++) {
					monomial[i] = variable_1;
					i++;
				}

				for(int k = 0; k < times_var_2; k++) {
					monomial[i] = variable_2;
					i++;
				}

				for(int k = 0; k < times_var_3; k++) {
					monomial[i] = variable_3;
					i++;
				}

				//repeat the monomial (thus, we are obtaining a coefficient)
				int repeat_the_monomial = 1 + rand.nextInt(3);
				for(int k = 0; k < repeat_the_monomial; ++k) {
					polynomial[j] = monomial;
					j++;
					if(j == numberOfMonomials)
						break;
				}
			} // end for
			
			//if we are here, we have our polynomial
			map.put(num_of_tuples + "", polynomial);
			num_of_tuples++;


			polynomialsList.add(map);
			
		}
		
		return polynomialsList;
	}
	

}
