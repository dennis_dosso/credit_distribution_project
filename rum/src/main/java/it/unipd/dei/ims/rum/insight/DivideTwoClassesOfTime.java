package it.unipd.dei.ims.rum.insight;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** This class was necessary since we had TSA+BM25 and TSA+VDP in the same 
 * log file (about time when this class was created). 
 * This class provides a method to take these information and divide them in 2 
 * different log files
 * 
 * */
public class DivideTwoClassesOfTime {
	
	private Map<String, String> map;
	
	private String inputPath;
	
	private String outputPath;
	
	private String keyString;
	
	public DivideTwoClassesOfTime() {
		try {
			this.map = PropertiesUsefulMethods.
					getSinglePropertyFileMap("properties/DivideTwoClassesOfTime.properties");
			
			this.inputPath = map.get("input.path");
			
			this.outputPath = map.get("output.path");
			
			this.keyString = map.get("key.string");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Given a file in input, the method extrapolates
	 * the lines inside the input files containing the provided string
	 * and writes them in a different file
	 * 
	 * @param keyString every line containing this string will be isolated and written in 
	 * the other file
	 * */
	public void extrapolateDataFromFile(String inputFile, String outputFile, String keyString) {
		Path inPath = Paths.get(inputFile);
		Path outPath = Paths.get(outputFile);
		
		//reader
		try {
			BufferedReader reader = Files.newBufferedReader(inPath);
			BufferedWriter writer = Files.newBufferedWriter(outPath);
			
			String line = "";
			while((line = reader.readLine()) != null) {
				//if the line contains the string
				if(line.contains(keyString)) {
					writer.write(line);
					writer.newLine();
				}
			}
			writer.flush();
			writer.close();
			
			reader.close();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) {
		
		//create the object, it will take the information from properties
		DivideTwoClassesOfTime execution = new DivideTwoClassesOfTime();
		//do the division
		execution.extrapolateDataFromFile(execution.inputPath, execution.outputPath, execution.keyString);
	}
	
	
	
}
