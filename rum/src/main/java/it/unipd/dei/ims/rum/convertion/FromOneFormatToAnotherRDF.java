package it.unipd.dei.ims.rum.convertion;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.openrdf.model.Model;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;

import it.unipd.dei.ims.rum.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PathUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** Offers method to convert a file RDF writte in one way to a file written 
 * in another way. E.G: from XML to Turtle
 * 
 * */
public class FromOneFormatToAnotherRDF {

	private String originalPath;

	private String outputPath;

	private String originalLanguage;

	private String outputLanguage;

	private RDFFormat inputFormat;
	private RDFFormat outputFormat;

	private String inputDirectory;

	private boolean onlyOneFile;

	Map<String, String> map;

	public FromOneFormatToAnotherRDF() {
		map = PropertiesUsefulMethods.
				getPropertyMap("properties/FromOneFormatToAnotherRDF.properties");

		this.originalPath = map.get("original.path");
		this.outputPath = map.get("output.path");
		this.originalLanguage = map.get("original.language");
		this.outputLanguage = map.get("output.language");
		this.inputDirectory = map.get("input.directory");

		this.onlyOneFile = Boolean.parseBoolean(map.get("only.one.file"));

		if(originalLanguage.equals("TURTLE")) {
			inputFormat = RDFFormat.TURTLE;
		} else if(originalLanguage.equals("N3")) {
			inputFormat = RDFFormat.N3;
		} else if(originalLanguage.equals("RDFXML")) {
			inputFormat = RDFFormat.RDFXML;
		}

		if(outputLanguage.equals("TURTLE")) {
			outputFormat = RDFFormat.TURTLE;
		} else if(outputLanguage.equals("N3")) {
			outputFormat = RDFFormat.N3;
		} else if(outputLanguage.equals("RDFXML")) {
			outputFormat = RDFFormat.RDFXML;
		}
	}


	public void convertLanguageOfOneRDFFile() {

		try {
			//read the graph
			Model model = new TreeModel(BlazegraphUsefulMethods.readOneGraph(this.originalPath, inputFormat));

			//now we reprint it
			BlazegraphUsefulMethods.printTheDamnGraph(model, outputPath, outputFormat);
		} catch (RDFParseException e) {
			e.printStackTrace();
		} catch (RDFHandlerException e) {
			e.printStackTrace();
		}

	}


	public void convertLanguageOfBunchOfRDFFile() {
		Model model = new TreeModel();

		List<String> list = PathUsefulMethods.getListOfFiles(inputDirectory);

		try {
			//read every graph in the directory and add it to the mix
			//blazegraph deals with repetitions
			for(String s : list) {
				File f = new File(s);
				System.out.println("doing now file " + f.getName());
				model.addAll(BlazegraphUsefulMethods.readOneGraph(s, inputFormat));
			}
			//at the end, print the whole graph
			BlazegraphUsefulMethods.printTheDamnGraph(model, outputPath, outputFormat);

		} catch (RDFParseException | RDFHandlerException e) {
			e.printStackTrace();
		}

	}


	public static void main(String[] args) {
		FromOneFormatToAnotherRDF execution = new FromOneFormatToAnotherRDF();

		if(!execution.onlyOneFile) {
			execution.convertLanguageOfBunchOfRDFFile();
		} else {
			execution.convertLanguageOfOneRDFFile();
		}
		
		System.out.println("done");
	}


	public boolean isOnlyOneLanguage() {
		return onlyOneFile;
	}


	public void setOnlyOneLanguage(boolean onlyOneLanguage) {
		this.onlyOneFile = onlyOneLanguage;
	}
}
