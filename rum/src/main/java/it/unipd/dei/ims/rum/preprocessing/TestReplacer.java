package it.unipd.dei.ims.rum.preprocessing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import it.unipd.dei.ims.rum.utilities.PathUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** Replace text strings inside a file with others
 * */
public class TestReplacer {
	
	/** input file*/
	private String inputFile;
	/** path of the output file*/
	private String outputFile;
	
	private String stringToReplace;
	
	/** string to be put instead of the original string*/
	private String replacingString;
	
	private String inputDirectory;
	
	private String outputDirectory;
	
	/** set to true if you are working on a single file, to false if you
	 * are working on a directory*/
	private boolean singleFile;
	
	private Map<String, String> map;
	
	public TestReplacer() {
		try {
			map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/TestReplacer.properties");
			
			this.inputFile = map.get("input.file");
			this.outputFile = map.get("output.file");
			this.stringToReplace = map.get("string.to.replace");
			this.replacingString = map.get("replacing.string");
			this.inputDirectory = map.get("input.directory");
			this.outputDirectory = map.get("output.directory");
			this.singleFile = Boolean.parseBoolean(map.get("is.single.file"));
		} catch (IOException e) {
			System.out.println("missing property file");
			e.printStackTrace();
		}
	}
	
	
	public void replaceInFile() {
		Path inPath = Paths.get(this.inputFile);
		Path outPath = Paths.get(this.outputFile);
		int lineCounter = 0;
		try {
			BufferedReader reader = Files.newBufferedReader(inPath);
			BufferedWriter writer = Files.newBufferedWriter(outPath);
			
			String line = "";
			while((line = reader.readLine()) != null) {
				String newLine = line.replaceAll(stringToReplace, replacingString);
				writer.write(newLine);
				writer.newLine();
				lineCounter++;
				if(lineCounter%1000==0) {
					System.out.println("printed " + lineCounter + " lines");
				}
			}
			
			writer.close();
			reader.close();
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void replaceAllFileInDirectory() {
		List<String> paths = PathUsefulMethods.getListOfFiles(this.inputDirectory);
		for(String path : paths) {
			this.inputFile = path;
			
			File f = new File(path);
			String name = f.getName();
			String newPath = this.outputDirectory + "/" + name;
			//just to be sure with subdirectories
			new File(this.outputDirectory).mkdirs();
			this.outputFile = newPath;
			//go with the swing
			this.replaceInFile();
		}
	}
	
	
	public static void main(String[] args) {
		TestReplacer execution = new TestReplacer();
		
		if(execution.singleFile)
			execution.replaceInFile();
		else
			execution.replaceAllFileInDirectory();
		
		System.out.println("done");
	}
}
