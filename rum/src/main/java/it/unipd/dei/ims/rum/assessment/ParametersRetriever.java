package it.unipd.dei.ims.rum.assessment;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** This is a script class that recuperates the
 * DCG in the different experiments and finds out the better parameters
 * */
public class ParametersRetriever {

	/** Main directory for the queries*/
	private String mainQueriesDirectory;
	
	private Map<String, String> map;
	
	/** THe key is the combination percentage,lookahead,
	 * the Double is the average of DCG among all the training queries
	 * */
	private Map<String, Double> valueMap;
	
	
	public ParametersRetriever() {
		try {
			map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/parameters.properties");
			this.mainQueriesDirectory = map.get("main.queries.directory");
			this.valueMap = new HashMap<String, Double>();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** We need to operate for every combination of lookahead and precision
	 * */
	public void computeBestCombination() {
		for(int lookahead = 5; lookahead<=30; lookahead+=5) {
			for(double percentage = 0.1; percentage<= 0.9; percentage += 0.1) {
				this.executeOneCombination(lookahead, percentage);
			}
		}
		
		//now we have all the combinations. Find out the best one already!
		double max = 0;//vintage. SO many memories
		String answer = "";
		for(Entry<String, Double> entry : this.valueMap.entrySet()) {
			double score = entry.getValue();
			System.out.println(entry.getKey() + " with score: " + entry.getValue());
			if(score > max) {
				max = score;
				answer = entry.getKey();
			} 
		}
		System.out.print("and the answer is: " + answer);
		
	}
	
	private void executeOneCombination(int lookahead, double percentage) {
		double score = 0;
		
		//for every query, retrieve the TB-DCG connected to this combination
		//XXX
		for(int queryId = 51; queryId <= 100; queryId++) {
			score += this.computeOneCombinationOnQuery(queryId, lookahead, percentage);
		}
		//now get the average
		score = (double) score/50;
		//populate the map
		this.valueMap.put(percentage + "," + lookahead, score);
	}
	
	private double computeOneCombinationOnQuery(int queryId, int lookahead, double percentage) {
		//take the path with the pool file
		String poolPath = this.mainQueriesDirectory + "/" + queryId + 
				"/collections/collection_" + percentage + "_" + lookahead + "/IR/evaluation/pool_0.1.txt";
		//TODO yeah yeah, I know, no hardwritten strings should be allowed. It is also 01:23 in the morning
		
		//read the file until the last line, where the info resides
		Path in = Paths.get(poolPath);
		try(BufferedReader reader = Files.newBufferedReader(in);) {
			String line = "";
			String previousLine = "";
			while((line=reader.readLine())!=null) {
				previousLine = line;
			}
			//reached the end of the file, we have in previousLine the last line
			String[] infos = previousLine.split(": ");
			Double score = Double.parseDouble(infos[1]);
			reader.close();
			return score;
			
		} catch (IOException e) {
			System.err.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAARGH! It seems that "
					+ "the combination " + lookahead + "  "+ percentage 
					+ " created some problem. \n But you already knew that.\n");
			e.printStackTrace();
		} catch (ArrayIndexOutOfBoundsException e ){
			//case in which a file is corrupted or something like that
			//had to cover it up
			return 0;
		}
		return 0;
	}
	
	/** Test main
	 * */
	public static void main(String[] args) {
		ParametersRetriever execution = new ParametersRetriever();
		execution.computeBestCombination();
	}
}
