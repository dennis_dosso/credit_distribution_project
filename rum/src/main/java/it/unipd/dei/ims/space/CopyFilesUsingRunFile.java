package it.unipd.dei.ims.space;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;

/** Offers a method to copy a answers file (originally
 * for answer graphs) from one directory to another following a run.txt file
 * 
 * */
public class CopyFilesUsingRunFile {
	
	/**
	 * 
	 * @param folding set to true if you mean that we are dealing with an 
	 * algorithm who put its answers in sub-directories
	 * 
	 * */
	public static void copyAnswers(String runFile, String originDirectory, String finalDirectory,
			boolean folding) {
		File fDir = new File(finalDirectory);
		if(!fDir.exists()) 
			fDir.mkdirs();
		
		//take the run file and open it with buffered reader
		Path path = Paths.get(runFile);
		try(BufferedReader reader = Files.newBufferedReader(path)) {
			String line = "";
			while( (line = reader.readLine()) != null ) {
				//read lines
				String[] parts = line.split(" ");
				//the id of the answer is contained in the third element of the line
				String answerId = parts[2];
				int id = Integer.parseInt(answerId);
				//now, with this id, we re-build the path of the answer
				int offset = (int) Math.ceil((double) id / 2048);
				
				//path of the origin file
				String originFile = "";
				if(folding) {
					originFile = originDirectory + "/" + offset + "/" + id + ".ttl";
				} else {
					originFile = originDirectory + "/" + id + ".ttl";
				}
				File srcFile = new File(originFile);
				
				//create the output file string
				String outputFile = finalDirectory + "/" + id + ".ttl";
				File outpF = new File(outputFile);
				
				//now let's copy the file
				FileUtils.copyFile(srcFile, outpF);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
