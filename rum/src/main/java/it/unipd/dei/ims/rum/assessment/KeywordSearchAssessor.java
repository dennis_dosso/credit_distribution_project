package it.unipd.dei.ims.rum.assessment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.NoSuchFileException;
import java.util.Collection;
import java.util.Map;

import org.openrdf.model.Model;
import org.openrdf.model.Statement;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.RDFParser;
import org.openrdf.rio.Rio;
import org.openrdf.rio.helpers.StatementCollector;

import it.unipd.dei.ims.rum.utilities.PathUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** This class represents the process
 * of the assessment of a performance of a system 
 * (recorded in a file similar to the .res files
 * of Terrier). This class presents the objects and methods 
 * necessary to perform the assessment and obtain a file with the 
 * resulting values.
 * */
public class KeywordSearchAssessor {

	/** The graph ground truth,
	 * the answer to the structured query
	 * */
	private Collection<Statement> groundTruth;

	private Map<String, String> map; 

	/** Path to the result file
	 * */
	private String resultFilePath;

	/** Path of the directory where we want to save the .eval file
	 * */
	private String outputEvaluationDirectoryPath;

	/** Directory where to find the graphs used to answer.
	 * This is particularly useful when you have more than 1 query
	 * */
	private String answerGraphsDirectory;

	/** Set to true if we are dealing with more than 1 query.
	 * i.e. you want to evaluate more than 1 query
	 * */
	private boolean multipleQueries;
	
	/** set to true if you want the results for all possible lambdas*/
	private boolean multipleLambdas, multipleMethods;
	
	private String[] pipelines = {"TSA+BM25", "TSA+VDP", "BLANCO", "YOSI", "SUMM"};

	/** set to true if the answer graphs are organized in
	 * sub-directories. This was originally the case
	 * with the SLM method. In the future I did not
	 * used the same file system disposition.
	 * */
	private boolean folding;

	/** threshold to be used in the computation of the
	 * signal to noise ration in order to understand if a graph is relevant or not.
	 * Bigger lambdas obtain lower positive results. 
	 * A nice value to start testing, in order to understand if a method is good or not,
	 * is empirically 0.1
	 * */
	private double lambda;

	/** threshold to be used to begin to discount the triple based
	 * discounted gain. Usually we put this to 2.
	 * */
	private int thresholdRank;

	/** PAth of the directory where to find the sub-directories with 
	 * all the queries. This directory de facto represents a system and a 
	 * way to generate answers.
	 * */
	private String queriesDirectory;

	/** The total number of queries we are using.
	 * */
	private int numberOfQueries;

	/** An array of string that will be initialized with the 
	 * paths of the sub-directories representing a query inside the main
	 * directory. NB: be careful that the directories that you
	 * insert in the queriesDirectory only represents queries and not other things
	 * */
	private File[] subDirectories;

	/** part of string to be added to the main query directory together with 
	 * the number of the query in order to reach the run file
	 * obtained by the system with the ranking of the answer graphs.
	 * */
	private String resultFilePathAddOn;

	/** The pipeline we are dealing with*/
	private String pipeline;

	/** Builder. 
	 * 
	 * */
	public KeywordSearchAssessor() {

		this.setup2();
	}

	/** This is the second setup method I implemented after
	 * returning on the project some time later. This setup 
	 * method considers the fact that we can have multiple 
	 * queries reading from the properties file and uses 
	 * the main directory class to setup the correct strings*/
	private void setup2() {

		System.out.println("setup of the KeywordSearchAssessor");
		try {
			this.map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/KeywordSearchAssessor.properties");

			this.queriesDirectory = map.get("main.pipeline.query.directory");
			this.multipleQueries = Boolean.parseBoolean(map.get("multiple.queries"));
			this.lambda = Double.parseDouble(map.get("lambda"));
			this.thresholdRank = Integer.parseInt(map.get("threshold.rank"));
			//get the number of query directories
//			this.subDirectories = PathUsefulMethods.getListOfSubDirectoriesInADirectory(this.queriesDirectory);
//			this.numberOfQueries = this.subDirectories.length;

			this.folding = false;

			this.pipeline = map.get("pipeline");
			if(this.pipeline.equals("TSA+BM25")) {
				this.resultFilePathAddOn = "/BM25/answer/run.txt";
			} else if(this.pipeline.equals("TSA+BLANCO")) {
				this.resultFilePathAddOn = "/BLANCO/rank/run.txt";
			} else if(this.pipeline.equals("TSA+YOSI")) {
				this.resultFilePathAddOn = "/YOSI/rank/run.txt";
			} else if(this.pipeline.equals("TSA+VDP")) {
				this.resultFilePathAddOn = "/VDP/answer/pruning_run.txt";
			} else if(this.pipeline.equals("BLANCO")) {
				this.folding = true;
				this.resultFilePathAddOn = "/rank/blanco.res";
			} else if(this.pipeline.equals("YOSI")) {
				this.resultFilePathAddOn = "/answer/ranking.txt";
			} else if(this.pipeline.equals("SUMM")) {
				this.resultFilePathAddOn = "/result/result.txt";
				this.folding = true;
			}
			
			this.multipleLambdas = Boolean.parseBoolean(map.get("multiple.lambdas"));
			this.multipleMethods = Boolean.parseBoolean(map.get("multiple.methods"));
			this.pipelines = map.get("pipelines").split(",");
			
		} catch (IOException e) {
			System.err.println("error in reading information from the KeywordSearchAssessor.properties file");
			e.printStackTrace();
		}
	}

	private void setup() throws IOException, RDFParseException, RDFHandlerException {
		System.out.println("setup of the KeywordSearchAssessor");
		this.map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/assessment.properties");

		//take in the ground truth
		String groundTruthPath = map.get("ground.truth.path");
		this.readGroundTruth(groundTruthPath);

		//take the result file path
		this.resultFilePath = map.get("result.file.path");

		//where to read the answers in order to confront them
		this.answerGraphsDirectory = map.get("answer.graph.directory");

		//take the file where we want to save
		this.outputEvaluationDirectoryPath = map.get("output.evaluation.directory.path");
		File f = new File(this.outputEvaluationDirectoryPath);
		if(!f.exists()) {
			f.mkdirs();
		}

		this.multipleQueries = Boolean.parseBoolean(map.get("multiple.queries"));
		this.folding = Boolean.parseBoolean(map.get("folding"));

		this.lambda = Double.parseDouble(map.get("lambda"));
		this.thresholdRank = Integer.parseInt(map.get("threshold.rank"));

	}



	private void readGroundTruth(String groundTruthPath) {
		System.out.println("reading the ground truth...");
		InputStream inputStream;
		try {
			inputStream = new FileInputStream(new File(groundTruthPath));
			//prepare a collector to contain the triples
			StatementCollector collector = new StatementCollector();
			RDFParser rdfParser = Rio.createParser(RDFFormat.TURTLE);
			//link the collector to the parser
			rdfParser.setRDFHandler(collector);
			//parse the file
			rdfParser.parse(inputStream, "");
			//now get the statements composing the graph
			this.groundTruth = collector.getStatements();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (RDFParseException e) {
			e.printStackTrace();
		} catch (RDFHandlerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("ground truth of file " + groundTruthPath + " read");
	}

	/** The method executes the assessment of the .res file with the ground truth*/
	public void executeAssessment() {
		//XXX
		String poolPath = this.outputEvaluationDirectoryPath + "/pool_" + lambda + ".txt";

		//setup the assessor
		RunAssessor runAssessor = new RunAssessor();
		Model groundTruthCopy = new TreeModel(groundTruth);
		runAssessor.setGroundTruth(groundTruthCopy);
		runAssessor.setLambda(lambda);
		runAssessor.setLimitRank(thresholdRank);
		runAssessor.setPipeline(this.pipeline);

		//evaluate the run
		runAssessor.assessRunWithTBDCG(this.answerGraphsDirectory, this.resultFilePath, poolPath, 2048, folding);			

		groundTruthCopy.clear();
		//XXX old method of evaluation
		//		RunAssessor.assessRun(groundTruth, 2048, this.answerGraphsDirectory, this.resultFilePath, writingFilePath, poolPath, folding);
	}

	/** Executes the assessment for all the queries of a pipeline given 
	 * our File System structure*/
	public void executeAssessmentMultipleTimes() {
		for(int counter = 1; counter <= this.numberOfQueries; counter++) {
			this.executeAssessmentForQueryNumber(counter);
		}
	}
	
	public void executeAssessmentForMultipleMethods() {
		String databaseDir = this.map.get("database.directory");
		for(String s : pipelines) {
			//prepare the coordinates based on the pipeline
			this.pipeline = s;
			this.folding = false;
			if(this.pipeline.equals("TSA+BM25")) {
				this.resultFilePathAddOn = "/BM25/answer/run.txt";
				this.queriesDirectory = databaseDir + "/TSA/queries";
				this.subDirectories = PathUsefulMethods.getListOfSubDirectoriesInADirectory(this.queriesDirectory);
				this.numberOfQueries = this.subDirectories.length;
			} else if(this.pipeline.equals("TSA+VDP")) {
				this.resultFilePathAddOn = "/VDP/answer/pruning_run.txt";
				this.queriesDirectory = databaseDir + "/TSA/queries";
			} else if(this.pipeline.equals("TSA+VDP+BACK")) {
				this.resultFilePathAddOn = "/TSA_VDP_BACK/answer/run.txt";
				this.queriesDirectory = databaseDir + "/TSA/queries";
			} else if(this.pipeline.equals("TSA+BACK")) {
				this.resultFilePathAddOn = "/TSA_BACK/answer/run.txt";
				this.queriesDirectory = databaseDir + "/TSA/queries";
			} else if(this.pipeline.equals("BLANCO")) {
				this.folding = true;
				this.resultFilePathAddOn = "/rank/blanco.res";
				this.queriesDirectory = databaseDir + "/SLM/queries";
			} else if(this.pipeline.equals("YOSI")) {
				this.resultFilePathAddOn = "/answer/ranking.txt";
				this.queriesDirectory = databaseDir + "/MRFKS/queries";
			} else if(this.pipeline.equals("SUMM")) {
				this.resultFilePathAddOn = "/result/result.txt";
				this.queriesDirectory = databaseDir + "/SUMM/queries";
				this.folding = true;
			}
			this.subDirectories = PathUsefulMethods.getListOfSubDirectoriesInADirectory(this.queriesDirectory);
			this.numberOfQueries = this.subDirectories.length;
			//System.out.println("DEBUG: number of queries " + numberOfQueries +
			//		"\nqueriesDirectory: " + this.queriesDirectory);
			this.executeAssessmentMultipleTimesWithAllLambdas();
			
		}//end of for
	}

	/** Executes the assessments for the algorithm selected 
	 * in the properties file with all lambdas from 0 to 0.9 with
	 * step 0.1 and on all the queries from 1 to 50.
	 * Because automation is the way man
	 * */
	public void executeAssessmentMultipleTimesWithAllLambdas() {
		double[] lambdas = {0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1};
		for(double lambda : lambdas) {
			this.executeAssessmentForLambda(lambda);
		}
	}

	public void executeAssessmentForLambda(double lambda) {
		this.lambda = lambda;
		for(int counter = 1; counter <= this.numberOfQueries; counter++) {
			this.executeAssessmentForQueryNumber(counter);
		}
	}

	/** In order to estimate a couple of parameters, this method executes
	 * the evaluation for multiple queries, with every query
	 * different candidate collections generated from the merger algorithm.
	 * */
	public void executeAssessmentMultipleTimesForMultipleExecutions() {
		//XXX
		for(int counter = 51; counter <= 100; counter++) {
			this.executeAssessmentForMultipleExecutionsForQueryNumber(counter);
		}
	}

	public void executeAssessmentForQueryNumber(int counter) {
		//get the required informations
		this.setupForQueryNumber(counter);
		this.executeAssessment();
	}

	/** Execute the assessment multiple times for all the collections 
	 * that has been created for the query.
	 * <p>
	 * This method has been created to evaluate the results of multiple execution of
	 * the pipeline TSA+IR with different parameters as
	 * overlapping percentage and lookahead. 
	 * */
	private void executeAssessmentForMultipleExecutionsForQueryNumber(int queryNo) {

		this.setupForMultipleExecutionsForQueryNumber(queryNo);

		//for this query, get the directory of collections
		String collectionsDirectoryPath = map.get("main.pipeline.query.directory") + "/" 
				+ queryNo + "/collections";
		//list of all the directories
		File f = new File(collectionsDirectoryPath);
		File[] files = f.listFiles();
		for(File colDir : files) {
			if(colDir.isDirectory()) {
				//in this way avoid .DS_Store and what not

				//by my own convention, the collections directories are called
				//collection_overlapping%_lookadeag
				String[] names = colDir.getName().split("_");
				String overlappingPercentage = names[1];
				String lookAhead = names[2];

				//take the path of the run file
				//XXX
				this.resultFilePath = colDir.getAbsolutePath() + "/IR/run/ultimate_run.txt" ;

				//take the directory where the answer graphs are stored
				this.answerGraphsDirectory = colDir.getAbsolutePath() + "/graphs";

				//take the directory where to write the output files
				this.outputEvaluationDirectoryPath = colDir.getAbsolutePath() + "/IR/evaluation";

				//build the directory if necessary
				File gigi = new File(this.outputEvaluationDirectoryPath);
				if(!gigi.exists()) {
					gigi.mkdirs();
				}

				//now we can do the assessment
				this.executeAssessment();
			}
		}
	}

	private void setupForMultipleExecutionsForQueryNumber(int queryNo) {
		//take the path of the ground truth
		String groundTruthPath = map.get("ground.truths.directory") + "/" + queryNo + ".txt";
		//take in the ground truth
		this.readGroundTruth(groundTruthPath);

	}

	private void setupForQueryNumber(int counter) {
		//take the path of the ground truth file
		String groundTruthPath = map.get("ground.truths.directory") + "/" + counter + "." + map.get("extension");
		//take in the ground truth
		this.readGroundTruth(groundTruthPath);

		//take in the file with the ranking of the graphs/documents
		this.resultFilePath =  this.queriesDirectory + "/" + counter + this.resultFilePathAddOn;

		//take the directory where the answer graphs produced by the system are stored
//		this.answerGraphsDirectory = map.get("main.pipeline.query.directory") + "/" + counter;
		this.answerGraphsDirectory = this.queriesDirectory + "/" + counter;
		if(this.pipeline.equals("TSA+BM25")) {
			this.answerGraphsDirectory += "/BM25/merged_graphs";
		} else if(this.pipeline.equals("TSA+BLANCO")) {
			this.answerGraphsDirectory += "/BM25/merged_graphs";
		} else if(this.pipeline.equals("TSA+YOSI")) {
			this.answerGraphsDirectory += "/BM25/merged_graphs";
		} else if(this.pipeline.equals("TSA+VDP")) {
			this.answerGraphsDirectory += "/VDP/pruning_answer_graphs";
		} else if(this.pipeline.equals("TSA+VDP+BACK")) {
			this.answerGraphsDirectory += "/TSA_VDP_BACK/answer_graphs";
		} else if(this.pipeline.equals("TSA+BACK")) {
			this.answerGraphsDirectory += "/TSA_BACK/answer_graphs";
		} else if(this.pipeline.equals("BLANCO")) {
			this.answerGraphsDirectory += "/answer_subgraphs";
		} else if(this.pipeline.equals("YOSI")) {
			this.answerGraphsDirectory += "/answer/extended_answer_graphs";
		} else if(this.pipeline.equals("SUMM")) {
			this.answerGraphsDirectory += "/answerGraphs";
		}

//	
		//take the directory where to write the output files
		//		this.outputEvaluationDirectoryPath = map.get("main.pipeline.query.directory") + "/" + counter + map.get("evaluation.directory.path.add.on");
		if(this.pipeline.equals("TSA+BM25"))
			this.outputEvaluationDirectoryPath = this.queriesDirectory + "/" + counter + "/BM25/evaluation";
		else if (this.pipeline.equals("TSA+VDP"))
			this.outputEvaluationDirectoryPath = this.queriesDirectory + "/" + counter + "/VDP/evaluation";
		else if (this.pipeline.equals("TSA+VDP+BACK"))
			this.outputEvaluationDirectoryPath = this.queriesDirectory + "/" + counter + "/TSA_VDP_BACK/evaluation";
		else if (this.pipeline.equals("TSA+BACK"))
			this.outputEvaluationDirectoryPath = this.queriesDirectory + "/" + counter + "/TSA_BACK/evaluation";
		else 
			this.outputEvaluationDirectoryPath = this.queriesDirectory + "/" + counter + "/evaluation";

//		System.out.println("DEBUG: queriesDirectory: " + this.queriesDirectory +
//				"\noutputDirectory " + this.outputEvaluationDirectoryPath);
		//build the directory if necessary
		File f = new File(this.outputEvaluationDirectoryPath);
		if(!f.exists()) {
			f.mkdirs();
		}
	}


	/** Test main.
	 * <p>
	 * 
	 * NB: The blanco function to find the answer graphs is 
	 * <p>
	 * (int) Math.floor((double)docId / step) + 1;
	 * <p>
	 * for everyone else it is:
	 * <p>
	 * (int) Math.ceil((double)docId / step)
	 * <p>
	 * peace.
	 * */
	public static void main(String[] args) {
		KeywordSearchAssessor assessor = new KeywordSearchAssessor();
		
		if(assessor.multipleMethods) {
			assessor.executeAssessmentForMultipleMethods();
		}
		if(assessor.multipleLambdas) {
			assessor.executeAssessmentMultipleTimesWithAllLambdas();
		}

		if(assessor.multipleQueries==true) {
			//for a single query
			//			assessor.executeAssessmentForQueryNumber(44);
			//for multiple executions and multiple queries (grid of points)
			//			assessor.executeAssessmentMultipleTimesForMultipleExecutions();

			//for multiple queries on a single lambda
			assessor.executeAssessmentMultipleTimes();
		} else
			assessor.executeAssessment();
	}


}
