package it.unipd.dei.ims.rum.zip;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import it.unipd.dei.ims.rum.utilities.PathUsefulMethods;


/** This class zips one single file
 * */
public class ZipAFile {

	/** The file to be zipped */
	private String fileToZip;

	/** Path of the zip file to be written */
	private String zippedFile;

	/** Test main */
	public static void test(String[] args) throws IOException {
		//path of the file to be zipped
		String sourceFile = "/Users/dennisdosso/Desktop/zip_test/1.ttl";
		//output path of the zipped file
		FileOutputStream fos = new FileOutputStream("/Users/dennisdosso/Desktop/zip_test/1.zip");
		//zip stream to write the file
		ZipOutputStream zipOut = new ZipOutputStream(fos);
		//open the file to be zipped
		File fileToZip = new File(sourceFile);
		FileInputStream fis = new FileInputStream(fileToZip);
		//create the entry that will zip the file
		ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
		zipOut.putNextEntry(zipEntry);
		byte[] bytes = new byte[1024];
		int length;
		while((length = fis.read(bytes)) >= 0) {
			//while there is data to read (the read method returns -1 when it has finished)
			//the method read takes the data from the fis object and put them in the bytes array
			zipOut.write(bytes, 0, length);
		}
		zipOut.close();
		fis.close();
		fos.close();
		System.out.print("the file has been zipped");
	}

	/** Zip one file in another
	 * */
	public void zipOneFile() throws FileNotFoundException {
		//path of the file to be zipped
		String sourceFile = this.fileToZip;
		//output path of the zipped file
		FileOutputStream fos = new FileOutputStream(this.zippedFile);
		
		System.out.print("now zipping file:\n" + this.fileToZip + "\ninto:\n" + this.zippedFile);
		//zip stream to write the file
		ZipOutputStream zipOut = new ZipOutputStream(fos);
		//open the file to be zipped
		File fileToZip = new File(sourceFile);
		FileInputStream fis = new FileInputStream(fileToZip);
		//create the entry that will zip the file
		ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
		try {
			zipOut.putNextEntry(zipEntry);
			byte[] bytes = new byte[1024];
			int length;
			while((length = fis.read(bytes)) >= 0) {
				//while there is data to read (the read method returns -1 when it has finished)
				//the method read takes the data from the fis object and put them in the bytes array
				zipOut.write(bytes, 0, length);
			}
			zipOut.close();
			fis.close();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.print("the file has been zipped");
	}
	
	/** Zips all the file contained in a directory.
	 * 
	 * @param dirPath path of the directory 
	 * @param outputPath the path of the zip file to create
	 * */
	public void zipMultipleFiles(String dirPath, String outputPath) {
		//array with the files to zip
		List<String> srcFiles = PathUsefulMethods.getListOfFiles(dirPath);
		try {
			//output stream 
			FileOutputStream fos = new FileOutputStream(outputPath);
			ZipOutputStream zipOut = new ZipOutputStream(fos);
			//zip one file at the time
			for (String srcFile : srcFiles) {
	            File fileToZip = new File(srcFile);
	            FileInputStream fis = new FileInputStream(fileToZip);
	            ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
	            zipOut.putNextEntry(zipEntry);
	 
	            byte[] bytes = new byte[1024];
	            int length;
	            while((length = fis.read(bytes)) >= 0) {
	                zipOut.write(bytes, 0, length);
	            }
	            fis.close();
	        }
	        zipOut.close();
	        fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	/** Test main.
	 * This test method tests the zipMultipleFiles.
	 * Takes a directory and zips all the files inside of it.
	 * */
	public static void main(String[] args) {
		ZipAFile execution = new ZipAFile();
		String directoryToZip = "/Users/dennisdosso/Desktop/zip_test/1";
		String zipFile = "/Users/dennisdosso/Desktop/zip_test/multiple_files.zip";
		
		execution.zipMultipleFiles(directoryToZip, zipFile);
		System.out.println("done");
	}
	

	public String getFileToZip() {
		return fileToZip;
	}

	public void setFileToZip(String fileToZip) {
		this.fileToZip = fileToZip;
	}

	public String getZippedFile() {
		return zippedFile;
	}

	public void setZippedFile(String zippedFile) {
		this.zippedFile = zippedFile;
	}
}
