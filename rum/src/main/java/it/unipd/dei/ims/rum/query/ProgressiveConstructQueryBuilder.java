package it.unipd.dei.ims.rum.query;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import org.openrdf.model.Statement;
import org.openrdf.query.GraphQuery;
import org.openrdf.query.GraphQueryResult;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFWriter;
import org.openrdf.rio.Rio;

import it.unipd.dei.ims.rum.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** This class was developed to build more complex classes of construct queries.
 * These are necessary in order to perform our queries over DBpedia.
 * <p>
 * The clas build a CONSTRUCT query contained in a queryPartsFile file. In this way 
 * you can build more complex construct query spanning multiple 
 * lines and concerning multiple entities.
 * */
public class ProgressiveConstructQueryBuilder {

	/** Path of the jnl database to open*/
	private String databasePath;

	/** the string with the prefixes for the SPARQL*/
	private String prefixString;

	/** Path of the file where we write the result*/
	private String outputFile;

	/** file where the parts of our query are conserved*/
	private String queryPartsFile;
	
	/** the beginning of the query*/
	private String queryBeginning;
	
	/** the ending part of the query*/
	private String queryEnding;

	private Map<String, String> map;

	public ProgressiveConstructQueryBuilder() {
		try {
			map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/ProgressiveConstructQueryBuilder.properties");

			this.databasePath = map.get("database.path");
			this.prefixString = map.get("prefix.string");
			this.outputFile = map.get("output.file");
			this.queryPartsFile = map.get("query.parts.file");
			
			this.queryBeginning = map.get("query.beginning");
			this.queryEnding = map.get("query.ending");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void buildAndEecuteTheQuery() {
		String query = this.obtainFullQuery();
		//open the connection
		//open the connection to the database
		Repository repo = BlazegraphUsefulMethods.getRepositoryFromPath(this.databasePath);
		try {
			repo.initialize();
			RepositoryConnection cxn = BlazegraphUsefulMethods.getRepositoryConnection(repo);
			
			//query to create a graph
			GraphQuery graphQuery = cxn.prepareGraphQuery(QueryLanguage.SPARQL, prefixString + " " + query);
			//evaluate the query
			GraphQueryResult result = graphQuery.evaluate();

			//write the file in output
			File f = new File(this.outputFile);
			OutputStream output = new FileOutputStream(f);
			RDFWriter writer = Rio.createWriter(RDFFormat.TURTLE, output);

			writer.startRDF();
			while(result.hasNext()) {
				Statement t = result.next();
				writer.handleStatement(t);
			}
			writer.endRDF();
			output.close();
			System.out.println(query);

		} catch (RepositoryException e) {
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RDFHandlerException e) {
			e.printStackTrace();
		}
	}
	
	/** Builds a construct query */
	private String obtainFullQuery() {
//		String query = this.queryBeginning;
		String query = "";
		String heart = "";
		String body = "";
		//open the file with the properties
		Path inPath = Paths.get(this.queryPartsFile);
		int counter = 0;
		try {
			BufferedReader reader = Files.newBufferedReader(inPath);
			String line = "";
			while((line = reader.readLine()) != null) {
				heart += line;
				if(counter == 0) {
					body += " { " + line + " } ";
					counter++;
				} else {
					body += " UNION { " + line + " } ";
				}
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		query = "CONSTRUCT {" + heart + " } WHERE { " + body + " }"; 
		System.out.println("SPARQL query: " + query);
		return query;
	}
	
	
	
	
	public static void main(String[] args) {
		QueryPartsBuilder exec = new QueryPartsBuilder();
		exec.buildTheQueryBody();
		ProgressiveConstructQueryBuilder execution = new ProgressiveConstructQueryBuilder();
		execution.buildAndEecuteTheQuery();
		System.out.println("done");
	}
	
	









}
