package it.unipd.dei.ims.rum.treceval;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import it.unipd.dei.ims.rum.utilities.PathUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** Implements the trec_eval performance
 * */
public class TrecEvaluation {

	/**The resDirectory is the path of the directory containing one 
	 * or more run file. It is intended that every file must
	 * be a run file. Don't put there garbage, please.
	 * */
	private String resDirectory;

	/** Object representing the qRels
	 * */
	private QRelsFile qRels;

	/** Path of the qRel file.*/
	private String qRelsPath;

	/** Directory where we are going to save the result files with all the
	 * information that you may need (or also not, who knows)*/
	private String outputEvaluationDirectory;

	/** Map of the properties.
	 * The map reads the properties from the 
	 * properties/TrecEvaluation.properties file*/
	private Map<String, String> map;
	
	/** The stringsToConvert and coveringStrings are two twin arrays.
	 * One contains strings that we want to convert in other
	 * strings inside the id of the documents, the other the corresponding
	 * string to be put instead. This was done for a particular case:
	 * the discrepancies found between the qrels and the
	 * run files in the DBpedia evaluation collection 2013.
	 * This was due to the fact that the documents id were the 
	 * urls of the entities, sometimes represented with the full url, sometime
	 * represented with a smaller representation in RDF style 
	 * (e.g. dbpedia: instead of a full url).
	 * What I usually do is put http://dbpedia.org/resource/ as url to be changes
	 * and dbpedia: as string to be put instead.*/
	private String[] stringsToConvert, coveringStrings;

	public TrecEvaluation() {
		this.map = PropertiesUsefulMethods.getPropertyMap("properties/TrecEvaluation.properties");

		qRels = new QRelsFile();
		
		this.resDirectory = map.get("res.directory");
		this.qRelsPath = map.get("qrels.path");
		this.outputEvaluationDirectory = map.get("output.evaluation.directory");
		new File(outputEvaluationDirectory).mkdirs();
		
		stringsToConvert = map.get("strings.to.convert").split(",");
		coveringStrings = map.get("covering.strings").split(",");
		

	}

	/** Run the evaluation of the runs*/
	public void run() {
		System.out.println("Starting the evaluation. Reading the ground truth...");
		this.prepareQrels();
		System.out.println("Ground truth read. Now reading the run files...");

		//read the list of files inside the res directory
		List<String> resPaths = PathUsefulMethods.getListOfFiles(this.resDirectory);
		for(String p : resPaths) {
			//for each run path
			System.out.println("Reading run file:\n " + p + "\n");
			this.executeEvaluationOfOneRun(p);
		}
	}

	private void prepareQrels() {
		//first thing, we read all the qRels file 
		//and we keep track of the queries 
		//and the relevance judgments
		Path in = Paths.get(this.qRelsPath);
		try(BufferedReader reader = Files.newBufferedReader(in);) {
			String line = "";
			while((line = reader.readLine()) != null) {
				dealWithOneQrelsLine(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Executes the evaluation of one run file*/
	private void executeEvaluationOfOneRun(String runFile_) {
		//object representing the run
		RunFile runFile = new RunFile();
		
		//takes the name of the file and creates the corresponding result file with a similar name
		File f = new File(runFile_);
		String name = f.getName().split("\\.")[0];
		String evalPath = this.outputEvaluationDirectory + "/" + name + "_eval.txt";

		//now read the run file line by line
		Path in = Paths.get(runFile_);
		try(BufferedReader reader = Files.newBufferedReader(in)) {
			String line = "";
			while((line = reader.readLine()) != null) {
				this.dealWithOneRunLine(line, runFile);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("File read. Now evaluating...");
		//if we are here, we have all the elements we want in one run file
		runFile.runEvaluation(evalPath, this.qRels);
		System.out.println("Evaluated");
	}
	
	/** parses one line from the run file and it inserts it in the runFile object,
	 * so we sill have all the information about the run in that object.
	 * 
	 * NB: in the future we may have problems if we have to reach
	 * a number of queries or entry per queries above 10 millions,
	 * since I am using maps here. I hope never to see that day.
	 * 
	 * NB2: here we are assuming that the run files use the space \s
	 * or the tab \t as delimiter among elements. No commas
	 * */
	private void dealWithOneRunLine(String line, RunFile runFile) {
		for(int i = 0; i < this.stringsToConvert.length; ++i) {
			line = line.replaceAll(stringsToConvert[i], coveringStrings[i]);
		}
		
		
		String[] parts = line.split(" ");
		System.out.println(line);
		if(parts.length == 1)
			parts = line.split("\t");

		
		String queryId = parts[0];
		String docId = "";
		try {
			docId = parts[2].replaceAll("<", "").replaceAll(">", "");
		} catch(Exception e) {
			System.out.println(line);
			System.out.println(parts.length);
		}
		String rank = parts[3];
		String score = parts[4];
		String model = parts[4];
		
		RunElement runElement = new RunElement();
		runElement.setQueryId(queryId);
		runElement.setDocId(docId);
		runElement.setRank(Integer.parseInt(rank));
		runElement.setScore(Double.parseDouble(score));
		runElement.setModel(model);
		
		//get the relevance assessment for this element
		int relevance = this.qRels.check(runElement);
		runElement.setRelevance(relevance);
		
		//insert it in the run file
		runFile.addElement(runElement);
		
	}

	
	/**NB: this method considers the qrels to be files
	 * with lines where information is separated by tabs.
	 * */
	private void dealWithOneQrelsLine(String line) {
		for(int i = 0; i < this.stringsToConvert.length; ++i) {
			line = line.replaceAll(stringsToConvert[i], coveringStrings[i]);
		}
		
		//the wrels are usually divided by a tab
		String[] parts = line.split("\t");
		//create a new QRelEntry
		QRelEntry entry = new QRelEntry();
		entry.setQueryId(parts[0]);
		//get rid of the <, >, they are stupid anyways
		entry.setDocId(parts[2].replaceAll("<", "").replaceAll(">", ""));
		entry.setRelevance(Integer.parseInt(parts[3]));
		
		this.qRels.insertEntry(entry);
	}


	public static void main(String[] args) {
		TrecEvaluation execution = new TrecEvaluation();

		execution.run();


	}

}
