package it.unipd.dei.ims.rum.utilities;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;

//a class to generate loggers 
public class LogManagement {

	
	/** Given the path of a directory and the name of the file, this method creates a writer in append
	 * of a file. It also appends at the end of the filename the current date and time to make the file unique.
	 * <p>
	 * it returns null if unable to setup the writer.
	 * 
	 * @param format the format that you want for your file (e.g. txt, csv etc.)
	 * */
	public static BufferedWriter getAWriterWithTimestamp(String directoryPath, String fileName, String format) {
		FileWriter fWriter;
		BufferedWriter bWriter = null;
		
		LocalDateTime now = LocalDateTime.now();
		int year = now.getYear();
		int month = now.getMonthValue();
		int day = now.getDayOfMonth();
		int hour = now.getHour();
		int minute = now.getMinute();
		
		
		
		String filePath = directoryPath + "/" + fileName + "_" + year + "_" + month + "_" + day + "_" + hour + "_" + minute + "." + format; 
		
		//open the file writer in append
		try {
			fWriter = new FileWriter(filePath, false);
			bWriter = new BufferedWriter(fWriter);
		} catch (IOException e) {
			System.err.println("errors with the directory " + directoryPath);
			e.printStackTrace();
		}
		
		return bWriter;
	}
}
