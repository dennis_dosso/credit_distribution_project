package it.unipd.dei.ims.rum.convertion;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.StringUsefulMethods;

/** Provides methods to insert a list of files inside a directory into a 
 * table in a database. To be used when you have a lot of files to deal with, in the order of millions, 
 * and a database can be useful to deal with so many lines of text.
 * <p>
 * This class was first though to store a lot of paths of RDF (graph) files
 * and their id inside a database. They were too many to be read directly from 
 * a directory every time. It required too time to look them up from the directory and subdirectories.
 * <p>
 * This class saves the files inside a directory called paths(path_string, path_id)
 * inside a relational database passed as parameter.
 * */
public class InsertFilesPathInsideRDBDatabase {

	/**Main directory where to read files and files in subdirectories.
	 * */
	private String mainDirectory;

	/** String to connect jdbc to the database.*/
	private String jdbcConnectingString;

	/** Driver to be used by jdbc.*/
	private String jdbcDriver;

	private String sqlInsert = "INSERT INTO public.paths(" + 
			"	path_string, path_id)" + 
			"	VALUES (?, ?);";

	/** This constructor saves the information of the paths
	 * of the cluster inside a RDB database to easy access.
	 * 
	 * @param directory the directory where the clusters are located. Even in subdirectories
	 * @param the connection string to the database where to save them.*/
	public InsertFilesPathInsideRDBDatabase (String directory, String connectingString, String driver) {
		mainDirectory = directory;
		jdbcConnectingString = connectingString;
		jdbcDriver = driver;
	}


	/** Insert the files inside the main directory (specifide in the constructor 
	 * with the parameter mainDirectory) inside the database (parameter jdbcConnectingString
	 * in the constructor).
	 * */
	public void insertFilesIntoDatabase () {

		//open the database
//		try {
//			Class.forName(jdbcDriver);
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace();
//		}
		Connection rdbConnection = null;

		//open RDB connection
		try {
			//open the connection
			rdbConnection = DriverManager.getConnection(jdbcConnectingString);
			//take the main directory
			File mainDirectoryFile = new File(mainDirectory);

			if(! mainDirectoryFile.isDirectory()) {
				throw new IllegalArgumentException("provied path is not a directory");
			}

			Queue<File> directoryQueue = new LinkedList<File>();
			directoryQueue.add(mainDirectoryFile);
			//in case an ID is not found, we use negative id to identify the files
			int inverseCounter = -1;
			int mapCounter = 0;
			int totalCounter = 0;
			PreparedStatement preparedInsert = rdbConnection.prepareStatement(sqlInsert);

			while(! directoryQueue.isEmpty()) {
				//while still files to explore

				File f = directoryQueue.remove();
				//the first time it is a directory, then we can only have directories
				File[] files = f.listFiles();

				for(File file : files) {

					if(file.isDirectory())
						//sub-directory
						directoryQueue.add(file);

					else if (file.isFile() && (! file.getName().startsWith(".DS_Store"))){
						//it is a legitimate file
						//get the id
						String path = file.getAbsolutePath();
						String docId = "";
						try {
							docId = StringUsefulMethods.getIdFromFile(file);
						}
						catch(Exception e) {
							System.err.println("Error in the regular expression");
						}

						if(docId.equals("") || docId==null)
							docId = (inverseCounter--) + "";

						mapCounter++;
						//prepare the insert statement
						preparedInsert.setString(1, path);
						preparedInsert.setInt(2, Integer.parseInt(docId));

						preparedInsert.addBatch();

						//flush the data inside the database
						if(mapCounter >= 100000) {
							totalCounter+=mapCounter;
							mapCounter=0;

							preparedInsert.executeBatch();
							preparedInsert.clearBatch();
							System.out.println("Inserted " + totalCounter + " files path");
						}
					}
				}
			}
			if(mapCounter>0) {
				totalCounter+=mapCounter;
				mapCounter=0;

				preparedInsert.executeBatch();
				preparedInsert.clearBatch();
				System.out.println("Last insertion, inserted " + totalCounter + " files path");
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		} finally {
			if(rdbConnection!=null) {
				try {
					rdbConnection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//#################################

	public static void main(String[] args) throws IOException {
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/path_insertion.properties"); 
		
		String mainDirectory = map.get("cluster.main.directory");
		String jdbcConnection = map.get("jdbc.connecting.string");;
		String jdbcDriver = map.get("jdbc.driver");
		
		
		//XXX DEBUG
//		String mainDirectory = "/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/clusters/clusters";
//		String jdbcConnection = "jdbc:postgresql://localhost:5432/disgenet?user=postgres&password=Ulisse92";
//		String jdbcDriver = "com.mysql.jdbc.Driver"; 
//		
		
		InsertFilesPathInsideRDBDatabase inserter = new InsertFilesPathInsideRDBDatabase (mainDirectory, jdbcConnection, jdbcDriver);

		inserter.insertFilesIntoDatabase();
	}

	//#################################


	public String getMainDirectory() {
		return mainDirectory;
	}

	public void setMainDirectory(String mainDirectory) {
		this.mainDirectory = mainDirectory;
	}

	public String getJdbcConnectingString() {
		return jdbcConnectingString;
	}

	public void setJdbcConnectingString(String jdbcConnectingString) {
		this.jdbcConnectingString = jdbcConnectingString;
	}

	public String getJdbcDriver() {
		return jdbcDriver;
	}

	public void setJdbcDriver(String jdbcDriver) {
		this.jdbcDriver = jdbcDriver;
	}

	public String getSqlInsert() {
		return sqlInsert;
	}

	public void setSqlInsert(String sqlInsert) {
		this.sqlInsert = sqlInsert;
	}
}
