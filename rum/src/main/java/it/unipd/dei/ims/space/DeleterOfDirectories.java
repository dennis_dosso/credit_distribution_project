package it.unipd.dei.ims.space;

import java.io.File;

import org.apache.commons.io.FileUtils;


/** This class provides useful methods to clean directories and other things
 * 
 * */
public class DeleterOfDirectories {

	
	/** Deletes all the directories whose paths are included in the provided array.
	 * 
	 * @param directories which we want to delete.
	 * */
	public static void deleteAllTheseDirectories(String[] directories) {
		for(String dir : directories) {
			File delendumEst = new File(dir);
			//delete the directory, even if it is not empty, and all the subdirs
			boolean deleted = FileUtils.deleteQuietly(delendumEst);
			if(deleted) 
				System.out.println("deleted directory " + dir);
		}
	}
}
