package it.unipd.dei.ims.rum.convertion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.impl.PropertyImpl;
import org.apache.jena.tdb.TDBFactory;
import org.apache.jena.tdb.store.bulkloader.BulkLoader;
import org.apache.jena.util.FileManager;

import it.unipd.dei.ims.rum.utilities.StringUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;

/** This class converts a TSV file formatted with the IMDB format
 * in an RDF file in N-TRIPLE syntax.
 * <p>
 * The syntax goes something like this:
 * <ul>
 * <li> first row: the attributes compairing on the subsequents rows. It is the equivalent
 * of the attributes of the relational databases. THey are used as predicates to connect the nodes.</li>
 * <li> All the other rows: the values of the entities. THey are the tuples of the relational database. 
 * Every value is a node in RDF.
 * </li>
 * </ul>
 * </p>
 * */
@Deprecated
public class FromTSVToRDFConverter {

	/**Path of the file to be converted.*/
	private String originalFilePath;

	/**path of the file to be written.*/
	private String outputFilePath;
	
	private static int lineCounter, tripleCounter = 0;

	/** Given a tsv file describing a dataset in IMDB format, it trasforms it in 
	 * an RDF graph in turtle syntax.
	 * <p>
	 * This method should only be used with small tsv files.
	 * 
	 * @param inputFile
	 * @param outputFile the RDF file to be written.
	 * 
	 * */
	public static void convertion(String inputFile, String outputFile) {

		//list of predicates given by the first line
		List<String> predicates = new ArrayList<String>();

		//prepare the paths to read and to write
		Path inputPath = Paths.get(inputFile);

		//model to be written
		Model model = ModelFactory.createDefaultModel();


		//open and read the file with a BufferedReader
		try (BufferedReader reader = Files.newBufferedReader(inputPath, UsefulConstants.CHARSET_ENCODING)){
			//read the first line

			//need the first line to tell us the predicates
			String firstLine = reader.readLine();
			assert(firstLine != null);

			//take the attributes
			String[] firstLineParts = firstLine.split("\t");
			//save them in the list. We don't need the first parameter, nconst, because it simply states the fact
			//that the first value is an ID. It is not used as attribute.
			for(int i = 1; i<firstLineParts.length; ++i) {
				predicates.add(firstLineParts[i]);
			}

			//read the rest
			String line="";
			while((line = reader.readLine())!= null) {
				//split the line and get the objects
				String[] lineParts = line.split("\t");

				//the first element is always the ID
				String subject = lineParts[0];

				//for each predicate
				for(int i = 0; i < predicates.size(); ++i) {
					Property property = new PropertyImpl(predicates.get(i));
					//take all the object corresponding to the i-th predicate
					String[] objects = lineParts[i+1].split(",");
					for(String obj : objects) {
						//add the triple to the model
						model.createResource(subject).addProperty(property, obj);
					}
				}

			}

			OutputStream out = new FileOutputStream(new File(outputFile));
			model.write(out, "TTL", null);
			out.close();
			System.out.println("done");

		} catch (IOException e) {
			System.out.print("Unable to read file " + inputFile);
			e.printStackTrace();

		}
	}


	/** Given a tsv file describing a dataset in IMDB format, it trasforms it in 
	 * an RDF graph in N-triple syntax, writing with the {@link BulkLoader} into 
	 * a TDB repository.
	 * <p>
	 * This method reads from a tsv files and creates jena model files than then 
	 * saves into a TDB. The intermediate passage of creating little Jena models
	 * is due to the fact that reading all the file and creating one huge Jena model
	 * in central memory could bring to very slow and innefficient code.
	 * <p>
	 * NB: this method writes and saves the models in the turtle syntax.
	 * <p>
	 * NB: this method uses the transactional write in TDB, and not the BulkLoader.
	 * THis is due to the fact that we have experienced corruption in the TDB file
	 * when loading this kind of small files.
	 * 
	 * @param inputFile
	 * @param outputFile the TDB repository where to write the model.
	 * @param supportModel Due to the fact that when we have huge files we cannot write them
	 * using simply the write method, and also due to the fact that it is not
	 * possible to use TDBLoader to write into disk a model in RAM, we need to write repeatedly
	 * chunk of models that are then added to the TDB iterativerly. supportModel is the path to the file 
	 * model of support in this operation.
	 * 
	 * */
	public void convertionToTDB(String inputFile, String outputTDBDirectory, String supportModel) {

		//list of predicates given by the first line
		List<String> predicates = new ArrayList<String>();

		//prepare the paths to read and to write
		Path inputPath = Paths.get(inputFile);

		//dataset to be written
		Dataset dataset = TDBFactory.createDataset(outputTDBDirectory);

		//model to be written
		Model model = ModelFactory.createDefaultModel();


		//open and read the file with a BufferedReader
		try (BufferedReader reader = Files.newBufferedReader(inputPath, UsefulConstants.CHARSET_ENCODING)){

			//need the first line to tell us the predicates
			String firstLine = reader.readLine();
			assert(firstLine != null);

			//take the attributes
			String[] firstLineParts = firstLine.split("\t");
			//save them in the list. We don't need the first parameter, nconst, because it simply states the fact
			//that the first value is an ID. It is not used as attribute.
			for(int i = 1; i<firstLineParts.length; ++i) {
				predicates.add(
						StringUsefulMethods.camelCaseBreakerToLowerCaseStringForURL(firstLineParts[i]));
			}

			//read the rest
			String line="";
			int counter = 0;
			lineCounter = 0;

			//a thread to take notice of the progression
			final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
			scheduler.scheduleAtFixedRate(new MonitorThread(), 10, 20, TimeUnit.SECONDS);
			
			while((line = reader.readLine())!= null) {
				lineCounter++;
				//split the line and get the objects
				String[] lineParts = line.split("\t");

				//the first element is always the ID
				String subject = lineParts[0];

				//for each predicate
				for(int i = 0; i < predicates.size(); ++i) {
					Property property = new PropertyImpl(predicates.get(i));
					//take all the object corresponding to the i-th predicate
					String[] objects = lineParts[i+1].split(",");
					for(String obj : objects) {
						//add the triple to the model
						model.createResource(subject).addProperty(property, obj);
//						tripleCounter++;
					}
					counter++;
				}

				//deal with the overhead writing part of the model each time
				if(counter > 4096 ) {
					//save the current model in the directory

					OutputStream out = new FileOutputStream(new File(supportModel));
					model.write(out, "TTL", null);
					out.close();
					
					dataset.begin(ReadWrite.WRITE);
					try {
						Model writingModel = dataset.getDefaultModel();
						InputStream in = FileManager.get().open(supportModel);
						writingModel.read(in, null, "TTL");
						in.close();//TODO check if this is necessary
						dataset.commit();
					} finally {
						dataset.end();
					}

					//the TDBLoader creates problems inside the TDB, the files
					//results corrupted
//					TDBLoader.loadModel(dataset.getDefaultModel(), supportModel);
					
					model.removeAll();
					
					counter = 0;
					
//					System.out.println("done " + lineCounter + " lines of the tsv file and " + 
//					tripleCounter + " triples");
				}

			}
			//deal with the remaining queue
			if(counter > 0) {
				//something remains to be written
				OutputStream out = new FileOutputStream(new File(supportModel));
				model.write(out, "TTL", null);
				out.close();

				dataset.begin(ReadWrite.WRITE);
				try {
					Model writingModel = dataset.getDefaultModel();
					InputStream in = FileManager.get().open(supportModel);
					writingModel.read(in, null, "TTL");
					dataset.commit();
				} finally {
					dataset.end();
				}
				
//				TDBLoader.loadModel(dataset.getDefaultModel(), supportModel);
				
				model.removeAll();
				counter = 0;
			}

			System.out.println("done with number of lines: " + lineCounter);
			scheduler.shutdownNow();

		} catch (IOException e) {
			System.out.print("Unable to read file " + inputFile);
			e.printStackTrace();

		}
	}
	
	//]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]//
	private class MonitorThread extends Thread {
		public void run() {
			System.out.println("done " + lineCounter + " lines of the tsv file");
		}
		
	}


	//++++++++++++++++++++++++++++++++++//


	public String getOriginalFilePath() {
		return originalFilePath;
	}

	public void setOriginalFilePath(String originalFilePath) {
		this.originalFilePath = originalFilePath;
	}

	public String getOutputFilePath() {
		return outputFilePath;
	}

	public void setOutputFilePath(String outputFilePath) {
		this.outputFilePath = outputFilePath;
	}


}
