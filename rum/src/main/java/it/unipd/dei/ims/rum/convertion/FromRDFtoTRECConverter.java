package it.unipd.dei.ims.rum.convertion;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.openrdf.model.Literal;
import org.openrdf.model.Value;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.RDFParser;
import org.openrdf.rio.Rio;
import org.openrdf.rio.helpers.StatementCollector;

import it.unipd.dei.ims.rum.utilities.StringUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UrlUtilities;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;

/** This class converts massive amount of files RDF (graphs) in turtle format
 * into TREC documents. It uses a support database RDB where the
 * paths of the files are stored.
 * <p>
 * Originally, this class was used to convert RDF graphs to text documents.
 * <p>
 * We use the Blazegraph library to deal with the graphs.
 * <p>
 * This class utilizes a table in a database with all the paths to convert.
 * For the on-the -fly version, go to {@link FromSubgrapfRDFToTRECPhase}
 * */
@Deprecated
public class FromRDFtoTRECConverter {



	/** Path of the directory where to put the xml files*/
	private String outputDirectory;

	/** String to connect jdbc to the database.*/
	private String jdbcConnectingString;

	/** Driver to be used by jdbc.*/
	private String jdbcDriver;

	private String sqlSelect = "SELECT path_string, path_id" + 
			"	FROM public.paths order by path_id;";

	public FromRDFtoTRECConverter (String out, String connectingString, String driver) {
		outputDirectory = out;
		jdbcConnectingString = connectingString;
		jdbcDriver = driver;
	}

	/** Reads paths of files from a database. These files are RDF graph supposed to 
	 * be in turtle syntax. It writes them in TREC files in the output directory.
	 * */
	public void translateGraphsToTRECDocuments () {

//		//open the database
//		try {
//			Class.forName(jdbcDriver);
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace();
//		}
		Connection rdbConnection = null;
		Statement statement = null;
		//this map contains couple (id of the graph, collection of triples of the graph)
		//we take a bunch of paths and then write the corresponding documents trec in a single file
		Map<Integer, Collection<org.openrdf.model.Statement>> statementsMap = new HashMap<Integer, Collection<org.openrdf.model.Statement>>();

		int documentsCounter = 0; 
		int fileCounter = 1;

		int id = 0;
		
		//open RDB connection
		try {
			//open the connection
			rdbConnection = DriverManager.getConnection(jdbcConnectingString);

			//clean the directory where we are going to write the clusters
			try {
				FileUtils.cleanDirectory(new File(this.outputDirectory));
			} catch (IOException e1) {
				e1.printStackTrace();
				System.err.println("unable to clean the directory");
			}

			//interrogate the database
			statement = rdbConnection.createStatement();
			ResultSet rs = statement.executeQuery(sqlSelect);
			
			while(rs.next()) {
				//for each line, take the file it represents
				String path = rs.getString(1);
				id = rs.getInt(2);

				//now read the file
				//open the input stream to the file
				InputStream inputStream = new FileInputStream(new File(path));
				//prepare a collector to contain the triples
				StatementCollector collector = new StatementCollector();
				//read the file
				RDFParser rdfParser = Rio.createParser(RDFFormat.TURTLE);
				//link the collector to the parser
				rdfParser.setRDFHandler(collector);
				try {
					//parse the file
					rdfParser.parse(inputStream, "");
					
					//now get the triples/statements composing the graph
					Collection<org.openrdf.model.Statement> statements = collector.getStatements();
					
					//add the statements to a map
					statementsMap.put(id, statements);
					
					documentsCounter++;
					inputStream.close();
				}catch (RDFParseException e) {
					System.err.println("Error: lost document " + id);
					e.printStackTrace();
				} catch (RDFHandlerException e) {
					System.err.println("Error: lost document " + id);
					e.printStackTrace();
				}  catch(Exception e) {
					System.err.println("Error: lost document " + id);
					e.printStackTrace();
				}
				
				
				if(documentsCounter >= 4096) {
					documentsCounter=0;
					//write all the graphs into a file
					this.printAFile(statementsMap, fileCounter++);

					statementsMap.clear();
				}
			}
			if(documentsCounter > 0) {
				documentsCounter=0;
				//write all the graphs ito a file
				this.printAFile(statementsMap, fileCounter++);

				statementsMap.clear();
			}

		} catch (SQLException e1) {
			e1.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			System.err.println(id);
			if(rdbConnection!=null) {
				try {
					rdbConnection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	/**Prints all the clusters inside the map into a xml file*/
	private void printAFile(Map<Integer, Collection<org.openrdf.model.Statement>> statementsMap, int fileCounter) {
		//name of the output file
		String outputFile = outputDirectory + fileCounter + ".trec";
		System.out.println("printing the file " + outputFile);
		Path outputPath = Paths.get(outputFile);

		try(BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING);) {
			//write the documents
			for(Entry<Integer, Collection<org.openrdf.model.Statement>> entry: statementsMap.entrySet()) {
				//for every graph
				writer.write("<DOC>");
				writer.newLine();

				writer.write("<DOCNO>" + entry.getKey() + "</DOCNO>");
				writer.newLine();
				this.writeADocument(writer, entry.getValue());

				writer.write("</DOC>");
				writer.newLine();
			}
			writer.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** Given a graph, it uses the provided writer to convert this graph in a TREC
	 * document and print it in memory.
	 * */
	private void writeADocument(BufferedWriter writer, Collection<org.openrdf.model.Statement> graph) throws IOException {
		for(org.openrdf.model.Statement t : graph) {
			//for each triple, we print subject, predicate and object
			Value subject = t.getSubject();
			Value predicate = t.getPredicate();
			Value object = t.getObject();
			
			//subject
			String subjectString = UrlUtilities.takeWordsFromIri(subject.toString());
			subjectString = StringUsefulMethods.checkCharacterInStringForTREC(subjectString);
			writer.write( subjectString + " ");
			
			//predicate
			String predicateString = UrlUtilities.takeWordsFromIri(predicate.toString());
			predicateString = StringUsefulMethods.checkCharacterInStringForTREC(predicateString);
			writer.write(predicateString + " ");
			
			//object
			if(object instanceof Literal) {
				String objectString = StringUsefulMethods.checkCharacterInStringForTREC(object.stringValue());
				writer.write(objectString + " ");
			}
			else {
				String objectString = UrlUtilities.takeWordsFromIri(object.toString());
				objectString = StringUsefulMethods.checkCharacterInStringForTREC(objectString);
				writer.write(objectString.trim() + " ");
			}

		}
	}

	//##################################



	//##################################
	
	public String getOutputDirectory() {
		return outputDirectory;
	}

	public void setOutputDirectory(String outputDirectory) {
		this.outputDirectory = outputDirectory;
	}

	public String getJdbcConnectingString() {
		return jdbcConnectingString;
	}

	public void setJdbcConnectingString(String jdbcConnectingString) {
		this.jdbcConnectingString = jdbcConnectingString;
	}

	public String getJdbcDriver() {
		return jdbcDriver;
	}

	public void setJdbcDriver(String jdbcDriver) {
		this.jdbcDriver = jdbcDriver;
	}

	public String getSqlSelect() {
		return sqlSelect;
	}

	public void setSqlSelect(String sqlSelect) {
		this.sqlSelect = sqlSelect;
	}
}
