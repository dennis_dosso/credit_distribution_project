package it.unipd.dei.ims.rum.zip;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class UnzipFile {

	/** Unzip a zip file. The idea is that inside the zip there are single files.
	 * Subdirectories cannot be unzipped with this method.
	 * 
	 * @param fileZip the path of the zip file
	 * @param dest the destination directory where to save the files 
	 * */
	public void unzipFile(String fileZip, String dest) {
		File destDir = new File(dest);
		destDir.mkdirs();
		
		byte[] buffer = new byte[1024];
        ZipInputStream zis;
		try {
			zis = new ZipInputStream(new FileInputStream(fileZip));
			ZipEntry zipEntry = zis.getNextEntry();
			//for every wntry in the zip file
			while (zipEntry != null) {
				//obtain the file from the zip
	            File newFile = newFile(destDir, zipEntry);
	            //write it
	            FileOutputStream fos = new FileOutputStream(newFile);
	            int len;
	            while ((len = zis.read(buffer)) > 0) {
	                fos.write(buffer, 0, len);
	            }
	            fos.close();
	            zipEntry = zis.getNextEntry();
	        }
	        zis.closeEntry();
	        zis.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
	}
	
	public void unzip(String zipFilePath, String destDir) {
        File dir = new File(destDir);
        // create output directory if it doesn't exist
        if(!dir.exists()) dir.mkdirs();
        FileInputStream fis;
        //buffer for read and write data to file
        byte[] buffer = new byte[1024];
        try {
            fis = new FileInputStream(zipFilePath);
            ZipInputStream zis = new ZipInputStream(fis);
            ZipEntry ze = zis.getNextEntry();
            while(ze != null){
                String fileName = ze.getName();
                File newFile = new File(destDir + File.separator + fileName);
                System.out.println("Unzipping to "+newFile.getAbsolutePath());
                //create directories for sub directories in zip
                new File(newFile.getParent()).mkdirs();
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
                }
                fos.close();
                //close this ZipEntry
                zis.closeEntry();
                ze = zis.getNextEntry();
            }
            //close last ZipEntry
            zis.closeEntry();
            zis.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
	
	/** 
	 * 
	 *  @param destinationDir the path of the directory where to put the unzipped files
	 *  @param zipEntry one entry in the zip file
	 *  */
	private static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
		//create the file in the dir with the name of the entry
        File destFile = new File(destinationDir, zipEntry.getName());
         
        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();
         
        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }
         
        return destFile;
    }
	
	
	/** Test main */
	public static void main(String[] args) {
		UnzipFile execution = new UnzipFile();
		String zipFile = "/Users/dennisdosso/Desktop/zip_test/compressedDir.zip";
		String dest = "/Users/dennisdosso/Desktop/zip_test/out";
		
		execution.unzipFile(zipFile, dest);
//		execution.unzip(zipFile, dest);
	}
}
