package it.unipd.dei.ims.rum.query;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** This class produces parts of a construct query starting from a list of
 * entities and predicates. To be used in conjunction with {@link ProgressiveConstructQueryBuilder}.
 * <p>
 * This class was originally created to work with the DBpedia task.
 * <p>
 * Write all your entities in the entities.file, the predicate in the predicate file in the
 *data/ directory. The resulting body of the construct query will be written in the 
 *queryPartsFile, also in the data/ directory
 * */
public class QueryPartsBuilder {

	/**File containing the entities to use*/
	private String entitiesFile;
	
	/** File containing the predicates to print*/
	private String predicateFile;
	
	/** File with the objects. Put a # if you do not want an object.*/
	private String objectFile;
	
	/** File where to write the output*/
	private String outputFile;
	
	private Map<String, String> map;
	
	public QueryPartsBuilder() {
		try {
			map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/QueryPartsBuilder.properties");
			this.entitiesFile = map.get("entities.file");
			this.predicateFile = map.get("predicate.file");
			this.objectFile = map.get("object.file");

			this.outputFile = map.get("output.file");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** Uses the entities and the properties to build the query*/
	public void buildTheQueryBody() {
		//take all the entities
		List<String> entities = this.takeAllLines(this.entitiesFile);
		List<String> predicates = this.takeAllLines(this.predicateFile);
		List<String> objects = this.takeAllLines(this.objectFile);
		
		//now prepare the query
		String queryDoc = this.prepareTheQuery(entities, predicates, objects);
//		String queryDoc = this.prepareTheQueryWithUnion(entities, predicates);
		
		Path out = Paths.get(this.outputFile);
		try {
			BufferedWriter writer = Files.newBufferedWriter(out);
			writer.write(queryDoc);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** Takes every entity and every predicate and produces a query of the form:
	 * 
	 * entity1 pred1 ?o1; pred2 ?o2. 
	 * entity2 pred1 ?o3; pred2 ?o4 .
	 * etc.
	 * <p>
	 * Then I modifyed the method in order to deal also with objects
	 * */
	private String prepareTheQuery(List<String> entities, List<String> predicates, List<String> objects) {
		String query = "";
		int objCounter = 0;
		
		/** For every entuty, which usually is central subject of a subgraph
		 * */
		for(int i = 0; i < entities.size(); ++i) {
			//take the entity/subject
			String entity = entities.get(i);
			query += "<" + entity + ">";
			
			//now take all the predicates that we want to connect to this subject
			for(int j = 0; j < predicates.size(); j++) {
				//take the predicate and the potential object
				String predicate = predicates.get(j);
				String object = objects.get(j);
				//build the triple
				if(object.equals("#")) {
					//case in which the object is not specified, that is, it is a variable
					//it is not fixed
					query += " " +predicate + " " + "?o" + objCounter;
					objCounter++;
				} else {
					//the object is specified
					query += " " +predicate + " " + object;
				}
				
				if(j == predicates.size() - 1) {
					//last one triple of this entity, need to conclude
					query += " .\n";
				} else {
					//add the ; because we are connecting new triples to this entity
					query += " ; ";
				}
			}
		}
		return query;
	}
	
	/**Not used, do not bother*/
	private String prepareTheQueryWithUnion(List<String> entities, List<String> predicates) {
		String query = "";
		int objCounter = 0;
		
		for(int i = 0; i < entities.size(); ++i) {
			if(i > 0) {
				query += " UNION ";
			}
			query += "{";
			String entity = entities.get(i);
			query += "<" + entity + ">";
			for(int j = 0; j < predicates.size(); j++) {
				String predicate = predicates.get(j);
				query += " " +predicate + " " + "?o" + objCounter;
				objCounter++;
				if(j == predicates.size() - 1) {
					query += " .\n";
				} else {
					query += " ;\n";
				}
			}
			
			//before the end of this entity
			query += " }";
		}
		return query;
	}
	
	
	
	/** Simply reads, line by line, all the strings inside a file and builds a 
	 * list of Strings containing these lines.*/
	private List<String> takeAllLines(String file) {
		List<String> entities = new ArrayList<String>();
		Path in = Paths.get(file);
		try {
			BufferedReader reader = Files.newBufferedReader(in);
			String line = "";
			while((line = reader.readLine()) != null) {
				entities.add(line.trim());
			}
			
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return entities;
	}
	
	
	
	
	
	public static void main(String[] args) {
		QueryPartsBuilder exec = new QueryPartsBuilder();
		exec.buildTheQueryBody();
		System.out.println("done");
	}
	
	
}
