package it.unipd.dei.ims.rum.insight;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/** First class to extrapolate time information from text
 * 
 * */
public class TimeReaderFromText {

	private String filename;
	
	
	public void readTimesFromFile() {
		Path inPath = Paths.get(this.filename);
		double totalTime = 0;
		int counter = 0;
		try(BufferedReader reader = Files.newBufferedReader(inPath)) {
			String line = "";
			while((line = reader.readLine()) != null) {
//				boolean b = line.contains("complexively  required");
//				boolean b = line.contains("the total time required to compute");
				boolean b = line.contains("ended in");
				if(b) {
					counter++;
					
					String[] parts = line.split(":");
					String tTime = parts[1];
					String time = tTime.split(" ")[1];
					String measure = tTime.split(" ")[2];
					System.out.println(tTime);
					if(measure.equals("min")) {
						double thisScore = Double.parseDouble(time);
						totalTime += thisScore;
					} else if(measure.equals("s")) {
						double thisScore = (double) Double.parseDouble(time) / 60;
						totalTime += thisScore;
					}
					
					
				}
 			}
			double meanTime = (double) totalTime / (counter);
			System.out.println("counter: " + counter + "\n totalTime in minutes: " + totalTime + "\n average in minutes : " + meanTime);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**New method that reads on-line times
	 * in the form TIME + [algorithm] + [query number] + time*/
	public void readTimesFromFiles2() {
		Path inPath = Paths.get(this.filename);
		double totalTime = 0;
		int counter = 0;
		try(BufferedReader reader = Files.newBufferedReader(inPath)) {
			String line = "";
			while((line = reader.readLine()) != null) {
				boolean b = line.contains("TIME TSA+VDP") && !line.contains("off-line"); 
				
				if(b) {
					
					counter++;
					
					String[] parts = line.split(" ");
					String tTime = parts[3];
					String measure = parts[4];
					//System.out.println(tTime);
					double thisScore = 0;
					if(measure.equals("min")) {
						thisScore = Double.parseDouble(tTime);
						totalTime += thisScore;
					} else if(measure.equals("s")) {
						thisScore = (double) Double.parseDouble(tTime) / 60;
						totalTime += thisScore;
					} else if(measure.equals("ms")) {
						thisScore = (double) Double.parseDouble(tTime) / 1000;
						totalTime += thisScore;
					}
					System.out.println(thisScore);
				}
 			}
			double meanTime = (double) totalTime / (counter);
			System.out.println("counter: " + counter + "\n totalTime in minutes: " + totalTime + "\n average in minutes : " + meanTime);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		TimeReaderFromText execution = new TimeReaderFromText();
		execution.setFilename("/Users/dennisdosso/Documents/RDF_DATASETS/LUBM1M/systems/TSA/log/lubm_1m_times.txt");
		execution.readTimesFromFiles2();
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
}
