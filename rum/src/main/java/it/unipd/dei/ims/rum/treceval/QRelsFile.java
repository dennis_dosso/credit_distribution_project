package it.unipd.dei.ims.rum.treceval;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.unipd.dei.ims.rum.comparator.QRelComparator;
import it.unipd.dei.ims.rum.utilities.UsefulMathFunctions;

/** Represents a QRels file, with all its entries*/
public class QRelsFile {

	/** This map associates to every query id a list of
	 * entries connected to that entry*/
	private Map<String, List<QRelEntry>> entryMap;
	
	public QRelsFile() {
		this.entryMap = new HashMap<String, List<QRelEntry>>();
	}
	
	/** Inserts a new qRel element*/
	public void insertEntry(QRelEntry entry) {
		//take the query id of this entry
		String queryId = entry.getQueryId();
		if(! this.entryMap.containsKey(queryId)) {
			//a new query
			//create a new list of entries
			List<QRelEntry> list = new ArrayList<QRelEntry>();
			list.add(entry);
			entryMap.put(queryId, list);
		} else {
			List<QRelEntry> list  = entryMap.get(queryId);
			list.add(entry);
			this.entryMap.put(queryId, list);
		}
	}
	
	
	public int getTotalNumberOfQueries() {
		return this.entryMap.size();
	}
	
	/** Given an element representing an entry in a run file,
	 * the method returns the relevance of that document based on 
	 * this QRel*/
	public int check(RunElement element) {
		String queryId = element.getQueryId();
		List<QRelEntry> list =  this.entryMap.get(queryId);
		
		//look among the documents regarding this query in the QRels
		for(QRelEntry q : list) {
			if(q.getDocId().equals(element.getDocId()))
				return q.getRelevance();
		}
		
		return 0;//not found, so not relevant
	}
	
	/** Returns the number of relevant documents for one
	 * query (denominator of recall).
	 * We count every document which is not 'non relevant'.
	 * 
	 * */
	public int getTotalNumberOfRelevantDocsForOneQuery(String query) {
		List<QRelEntry> list = this.entryMap.get(query);
		int counter = 0;
		
		for(QRelEntry r : list) {
			if(r.getRelevance() > 0)
				counter ++;
		}
		return counter;
	}
	
	
	/** Computes the discounted cumulative gain of the ideal run.
	 * In this way you can compute the NDCG of other runs.
	 * 
	 * @returns an array with the values of idcg commonly used: @10 and @100
	 * */
	public double[] computeIDCG(String query) {
		//take a copy of the list since we are going to reorder it depending on the 
		//relevance of documents
		List<QRelEntry> list = new ArrayList<QRelEntry>(this.entryMap.get(query));
		//order the list
		Collections.sort(list, new QRelComparator());
		
		double dcg = 0, dcg10 = 0, dcg100 = 0;
		
		
		//now compute the dcg of this beauty
		for(int i = 1; i <= list.size(); ++i) {
			QRelEntry entry = list.get(i - 1);
			int relevance = entry.getRelevance();
			
			double discountedGain = 0;
			if(i <= 2) {
				//no discount
				discountedGain = relevance;
			} else {
				//apply discount
				discountedGain = (double) relevance / UsefulMathFunctions.logBase2Of(i);
			}
			
			dcg += discountedGain;
			
			if(i == 10) {
				dcg10 = dcg;
			} 
			
			if(i == 100) {
				dcg100 = dcg;
			}
			
		}
		
		//check the cases in which we may not reach 100 or 100
		if(dcg10 == 0 && dcg > 0) {
			dcg10 = dcg;
		}
		
		if(dcg100 == 0 && dcg > 0) {
			dcg100 = dcg;
		}
		
		double[] dcgValues = {dcg10, dcg100};
		return dcgValues;
		
	}
	
}
