package it.unipd.dei.ims.rum.treceval;

/** Represents one entry in a qRels file, 
 * thus a query ID, a document-if and a relevance judgment
 * */
public class QRelEntry {
	
	private String docId;
	
	private String queryId;
	
	/** The basic idea here is that 0 is non relevant, 1 is relevant.
	 * But if we are going with the graded, we will have 0, 1, 2 and 3 or whatever.
	 * */
	private int relevance;

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId_) {
		this.docId = docId_;
	}

	public String getQueryId() {
		return queryId;
	}

	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}

	public int getRelevance() {
		return relevance;
	}

	public void setRelevance(int relevance) {
		this.relevance = relevance;
	}
	
	
}
