package it.unipd.dei.ims.datastructure;

/**Class to represent information about an execution. That is, information
 * about the query, the time required, precision obtained, recall obtained etc.
 * */
public class PerformanceInformation {
	
	/** id of the query we are working with*/
	private int queryId;
	
	/** Unit of measure of the time, e.g. seconds (s) or minutes (min)*/
	private String unitOfMeasureTime;
	
	/** Set to true if this execution was completed in time or not.
	 * Set to 1 if completed, 0 otherwise*/
	private int completed;
	
	/** time of execution*/
	private int time;
	
	/** The value of tb-DCG obtained by this query*/
	private double tbDCG;
	
	/** The system used in this query*/
	private String system;
	
	/** The name of the database used for this query*/
	private String db;
	
	/** precision obtained in this query*/
	private double precision;
	
	/** Syze in bytes of the memory required by this algorithm*/
	private long size;
	
	/** size in more human readable format*/
	private String sizeHR;
	
	private double tbPrecision, sbPrecision, redunantPrecision, recall;
	
	private double precAt1, precAt5, precAt10;
	
	
	public PerformanceInformation() {
		
	}

	public int getQueryId() {
		return queryId;
	}

	public void setQueryId(int queryId) {
		this.queryId = queryId;
	}

	public String getUnitOfMeasureTime() {
		return unitOfMeasureTime;
	}

	public void setUnitOfMeasureTime(String unitOfMeasureTime) {
		this.unitOfMeasureTime = unitOfMeasureTime;
	}

	public int getCompleted() {
		return completed;
	}

	public void setCompleted(int completed) {
		this.completed = completed;
	}

	public double getTbDCG() {
		return tbDCG;
	}

	public void setTbDCG(double tbDCG) {
		this.tbDCG = tbDCG;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getDb() {
		return db;
	}

	public void setDb(String db) {
		this.db = db;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public double getPrecision() {
		return precision;
	}

	public void setPrecision(double precision) {
		this.precision = precision;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getSizeHR() {
		return sizeHR;
	}

	public void setSizeHR(String sizeHR) {
		this.sizeHR = sizeHR;
	}

	public double getTbPrecision() {
		return tbPrecision;
	}

	public void setTbPrecision(double tbPrecision) {
		this.tbPrecision = tbPrecision;
	}

	public double getSbPrecision() {
		return sbPrecision;
	}

	public void setSbPrecision(double sbPrecision) {
		this.sbPrecision = sbPrecision;
	}

	public double getRedunantPrecision() {
		return redunantPrecision;
	}

	public void setRedunantPrecision(double redunantPrecision) {
		this.redunantPrecision = redunantPrecision;
	}

	public double getRecall() {
		return recall;
	}

	public void setRecall(double recall) {
		this.recall = recall;
	}

	public double getPrecAt1() {
		return precAt1;
	}

	public void setPrecAt1(double precAt1) {
		this.precAt1 = precAt1;
	}

	public double getPrecAt5() {
		return precAt5;
	}

	public void setPrecAt5(double precAt5) {
		this.precAt5 = precAt5;
	}

	public double getPrecAt10() {
		return precAt10;
	}

	public void setPrecAt10(double precAt10) {
		this.precAt10 = precAt10;
	}
	
	
	
	
	
}
