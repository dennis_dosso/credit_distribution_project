package it.unipd.dei.ims.rum.consolidated;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.jena.ext.com.google.common.base.Stopwatch;
import org.openrdf.OpenRDFException;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParseException;

import com.bigdata.journal.Options;
import com.bigdata.rdf.sail.BigdataSail;
import com.bigdata.rdf.sail.BigdataSailRepository;

import it.unipd.dei.ims.rum.utilities.PathUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** This class inserts RDF files in turtle format inside a directory
 * in a triple store database (a Blazegraph .jnl file).
 * 
 * It uses the properties file ImportFilesIntoTripleRepository
 * 
 * */
public class ImportFilesIntoTripleRepository {


	/** Path of the RDF repository where to 
	 * write data.
	 * */
	private String outputRepositoryFilePath;

	/** Path of the directory where to find the RDF files.
	 * */
	private String directoryWithTheFilesPath;
	
	/**The language we are using to read RDF. 
	 * E.g. TURTLE, N3, XML
	 * */
	private String rdfFormat;

	public ImportFilesIntoTripleRepository() {

	}
	
	/** Fill the field with the information contained in the
	 * property file passed as parameter.
	 * @throws IOException 
	 * */
	public void setupFromPropertyFile(String propertyFilePath) throws IOException {
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap(propertyFilePath);
		
		this.outputRepositoryFilePath = map.get("output.repository.file.path");
		this.directoryWithTheFilesPath = map.get("directory.with.the.files.path");
		this.rdfFormat = map.get("rdf.format");
	}

	/** Loads the files found in the field directoryWithTheFilesPath in a RDF repository
	 * found at the path indicated by the outputRepositoryFilePath field
	 * */
	public void loadDirectoryOfFiles () {
		//take all the files inside the directory
		List<String> listOfFiles = PathUsefulMethods.getListOfFiles(directoryWithTheFilesPath);
		
		//set the RDFFormat we use to read
		RDFFormat format = null;
		if(rdfFormat.equals("N3"))
			format = org.openrdf.rio.RDFFormat.N3;
		else if(rdfFormat.equals("TURTLE"))
			format = org.openrdf.rio.RDFFormat.TURTLE;
		else if(rdfFormat.equals("RDFXML"))
			format = org.openrdf.rio.RDFFormat.RDFXML;
		

		//open the database
		final Properties props = new Properties();
		props.put(Options.BUFFER_MODE, "DiskRW"); // persistent file system located journal
		props.put(Options.FILE, outputRepositoryFilePath);
		final BigdataSail sail = new BigdataSail(props); // instantiate a sail
		final Repository repo = new BigdataSailRepository(sail); // create a Sesame repository

		RepositoryConnection cxn = null;

		try {
			//initialize the repository file
			repo.initialize();

			Stopwatch timer = Stopwatch.createStarted();
			//open the connection to the repository
			cxn = repo.getConnection();
			try {
				//for every file
				for(String file : listOfFiles) {
					System.out.println("Now loading file " + file);
					cxn.begin();

					//add the file to the repository
					cxn.add(new File(file), null, format);

					System.out.println("File read in " + timer);
					timer.reset().start();
					System.out.println("Now committing...");
					cxn.commit();
					System.out.println("Commit done in " + timer);
					timer.reset().start();
				}

				timer.stop();
				System.out.print("All done");

			} catch (OpenRDFException ex) {
				cxn.rollback();
				throw ex;
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				// close the repository connection
				cxn.close();
			}
		} catch (RepositoryException e) {
			System.err.print("Error initializing the repository " + outputRepositoryFilePath);
			e.printStackTrace();
		} catch (RDFParseException e) {
			System.out.print("DEBUG: error in parsing RDF");
			e.printStackTrace();
		}
		finally {
			try {
				repo.shutDown();
			} catch (RepositoryException e) {
				e.printStackTrace();
			}
		}
	}


	/** Test main.
	 * <p>
	 * NB: rememeber to include ALL the ground truth files in the conversion.
	 * */
	public static void main(String[] args) {
		
		ImportFilesIntoTripleRepository phase = new ImportFilesIntoTripleRepository();
		try {
			phase.setupFromPropertyFile("properties/ImportFilesIntoTripleRepository.properties");
			phase.loadDirectoryOfFiles();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public String getOutputRepositoryFilePath() {
		return outputRepositoryFilePath;
	}

	public void setOutputRepositoryFilePath(String outputRepositoryFilePath) {
		this.outputRepositoryFilePath = outputRepositoryFilePath;
	}

	public String getDirectoryWithTheFilesPath() {
		return directoryWithTheFilesPath;
	}

	public void setDirectoryWithTheFilesPath(String directoryWithTheFilesPath) {
		this.directoryWithTheFilesPath = directoryWithTheFilesPath;
	}

}
