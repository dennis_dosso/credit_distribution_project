package it.unipd.dei.imd.rum.anova.utilities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import it.unipd.dei.ims.rum.utilities.UsefulConstants;

/** Script to prepare the array with the data required by the ANOVA test
 * */
public class PrepareAnovaData {

	/** Path of the file where to save the matrix.
	 * */
	private String outputFile;

	/** Directory where to find our databases*/
	private String databaseDirectory;

	/** Arrays with the levels to deal with the ANOVA*/
	private String[] firstLevel, secondLevel, thirdLevel;

	public PrepareAnovaData(String outP) {
		this.outputFile = outP;
	}

	/** Reads all the recap files and creates a matrix in a csv file with
	 * the contained data
	 * */
	public void createMatrixFileForAnova3Ways() {
		//create our matrix
		List<List<String>> matrix = new ArrayList<List<String>>();

		//create 50 rows for the matrix, that is 50 lists
		for(int i = 0; i< 120; ++i) {
			matrix.add(new ArrayList<String>(50));
		}

		int counter = 0;

		for(String database : firstLevel) {
			for(String algorithm : secondLevel) {
				//create the path to the recap directory
				String path = this.databaseDirectory + "/" + database + "/" + algorithm;
				File f = new File(path);
				String[] recaps = f.list();

				for(String lambda : thirdLevel) {
					//find the file corresponding to our lambda
					for(String recap : recaps) {
						if(recap.contains(lambda)) {
							this.dealWithOneRecapFile(path + "/" + recap, matrix, counter);
							counter++;
						}
					}
				}
			}
		}
		//if we are here, our matrix has all the needed value
		//now we print the matrix
		this.printTheMatrix(matrix);
	}

	/** Reads one recap file and uses it to populate one row of
	 * the matrix at the provided index.*/
	private void dealWithOneRecapFile(String recapFilePath,
			List<List<String>> matrix,
			int index) {
		//get the row
		List<String> row = matrix.get(index);
		Path inputPath = Paths.get(recapFilePath);
		try(BufferedReader reader = Files.newBufferedReader(inputPath) ) {
			//read the first two lines, they are without useful information for us
			reader.readLine();
			reader.readLine();
			String line = "";
			for(int i = 0; i <50; ++i) {
				//read a line
				line = reader.readLine();
				String[] parts = line.split(",");
				
				//XXX
				//take the tb-dcg
//				String value = parts[1];
				//take the precision
				String value = parts[2];
				
				//insert the value in the row
				row.add(i, value);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void printTheMatrix(List<List<String>> matrix) {
		Path outputPath = Paths.get(this.outputFile);
		try(BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING); ) {
			for(int i = 0; i<matrix.size(); ++i) {//for each row of the matrix
				int columnCounter = 0;
				List<String> row = matrix.get(i);
				for(String value : row) {
					writer.write(value);
					if(columnCounter < 49) {
						writer.write(", ");
						columnCounter++;
					}
				}
				writer.newLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void createMatrixFileForAnova2Ways(int rows, int columns) {//rows: 60, columns: 50
		//create our matrix
		List<List<String>> matrix = new ArrayList<List<String>>();

		for(int i = 0; i < rows; ++i) {
			matrix.add(new ArrayList<String>(columns));
		}

		int counter = 0;

		for(String algorithm: firstLevel) {
			//create the path to the recap directory
			String path = this.databaseDirectory + "/" + algorithm;
			File f = new File(path);
			String[] recaps = f.list();//list of all the recap files, one for each lambda

			for(String lambda : secondLevel) {
				//find the file corresponding to our lambda
				//i need to do this little trick becuase I didn't used a nice
				//way to name the files. So I cannot
				//compose the path, but I need to look for the number inside
				//the name of the recap file
				for(String recap : recaps) {
					if(recap.contains(lambda)) {
						this.dealWithOneRecapFile(path + "/" + recap, matrix, counter);
						counter++;
					}
				}
			}
		}

		//if we are here, our matrix has all the needed value
		//now we print the matrix
		this.printTheMatrix(matrix);

	}


	public static void main(String[] args) {
		
		//********** execution for a 3 way ANOVA **********
		
		
		//level of the databases
		//in this array we put the names of the directories representing the database we are dealing with
		//the first level of the 3 way ANOVA
		 String[] firstLevel = {"rLinkedMDB", "LinkedMDB", "rIMDB", "IMDB"};
		
		//level of the algoroithms
		 //here we put the subpath of the algorithms we are going to use
		 //in these sub directories we will find the recap lambda files
		String[] secondLevel = { 
				"algorithms/TSA/recap_TSA_IR",
				"algorithms/TSA/recap_TSA_PRUNEfly"};
		//level of the ƛ
		String[] thirdLevel = {"0.0", "0.1", "0.2", "0.3",
				"0.4", "0.5", "0.6", "0.7", "0.8", "0.9"}; 

		//where to write the matrix
		String outP = "/Users/dennisdosso/Desktop/ANOVA tests/anova_precision_big_vs_small.csv";
		//the starting directory
		String databaseDirectory = "/Users/dennisdosso/Documents/RDF_DATASETS";

		PrepareAnovaData execution = new PrepareAnovaData(outP);

		execution.setDatabaseDirectory(databaseDirectory);

		execution.setFirstLevel(firstLevel);
		execution.setSecondLevel(secondLevel);
		execution.setThirdLevel(thirdLevel);

		execution.createMatrixFileForAnova3Ways();
		
		
		
		//********* 2 ways ANOVA execution **********
		
		/*
		//output path
		String outP = "/Users/dennisdosso/Desktop/ANOVA tests/2_ways/anova_data_precision_linkedmdb.csv";
//		String outP = "/Users/dennisdosso/Desktop/ANOVA tests/2_ways/anova_precision_data_imdb.csv";
		String databaseDirectory = "/Users/dennisdosso/Documents/RDF_DATASETS/rLinkedMDB";
//		String databaseDirectory = "/Users/dennisdosso/Documents/RDF_DATASETS/rIMDB";
		
		//level of the algorithm
		String[] firstLevel = {"algorithms/BLANCO/recap", 
				"algorithms/YOSI/recap_extended",
				"algorithms/TSA/recap_TSA_IR",
				"algorithms/TSA/recap_TSA_BLANCO",
				"algorithms/TSA/recap_TSA_YOSI",
				"algorithms/TSA/recap_TSA_PRUNEfly"};
		//level of the ƛ
		String[] secondLevel = {"0.0", "0.1", "0.2", "0.3",
				"0.4", "0.5", "0.6", "0.7", "0.8", "0.9"};
		
		PrepareAnovaData execution = new PrepareAnovaData(outP);
		execution.setDatabaseDirectory(databaseDirectory);
		
		execution.setFirstLevel(firstLevel);
		execution.setSecondLevel(secondLevel);
		
		execution.createMatrixFileForAnova2Ways(60, 50);
		*/
		System.out.print("all done");

	}

	public String getOutputFile() {
		return outputFile;
	}

	public void setOutputFile(String outputFile) {
		this.outputFile = outputFile;
	}

	public String getDatabaseDirectory() {
		return databaseDirectory;
	}

	public void setDatabaseDirectory(String databaseDirectory) {
		this.databaseDirectory = databaseDirectory;
	}

	public String[] getFirstLevel() {
		return firstLevel;
	}

	public void setFirstLevel(String[] firstLevel) {
		this.firstLevel = firstLevel;
	}

	public String[] getSecondLevel() {
		return secondLevel;
	}

	public void setSecondLevel(String[] secondLevel) {
		this.secondLevel = secondLevel;
	}

	public String[] getThirdLevel() {
		return thirdLevel;
	}

	public void setThirdLevel(String[] thirdLevel) {
		this.thirdLevel = thirdLevel;
	}
}
