package test;

import it.unipd.dei.ims.rum.utilities.UrlUtilities;

/** A class to test my ethods to extrapolate strings from the IRI
 * of an RDF resource.
 * 
 * */
public class UrlElaborationTest {

	public static void main(String[] args) {
		String test = "http://swat.cse.lehigh.edu/onto/univ-bench.owl#takesCourse";
		
		System.out.println(UrlUtilities.takeWordsFromIRIForLUBM(test));
	}

}
