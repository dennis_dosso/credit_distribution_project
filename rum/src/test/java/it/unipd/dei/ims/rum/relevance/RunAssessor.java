package it.unipd.dei.ims.rum.relevance;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.openrdf.model.Statement;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.RDFParser;
import org.openrdf.rio.Rio;
import org.openrdf.rio.helpers.StatementCollector;

import it.unipd.dei.ims.rum.utilities.PathUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;

public class RunAssessor {

	private final static String SQL_SELECT_CLUSTER_PATH = "SELECT path_string, path_id" + 
			"	FROM public.paths where path_id = ?;";

	/**Assess one run.
	 * 
	 * <p>
	 * NB: we suppose the run file to be a standard Terrier file. That is, each line is in the form:
	 * </p>
	 * <p>
	 * query_no Qxy docID rank score model_name
	 * </p>
	 * <p>
	 * Where query_no means query number. xy is the number of the query (when the run is 
	 * composed by more than one query). docID is the id of the document/graph. rank is the rank and it is
	 * meant in ascending order. Score is the score of the document provided by the model. The last
	 * element is the name of the model.
	 * </p>
	 * 
	 * @param groundTruth string with the path of the ground truth graph.
	 * @param connection a Connection object to a RDB database where the paths of the graph file 
	 * to assess are located
	 * @param runFile file .res with the ranking of the documents.
	 * @param writingFile where to write the results
	 * */

	public static void assessRun(Collection<org.openrdf.model.Statement> groundTruth, Connection connection, String runFilePath, String writingFile) {
		//open the run file
		Path inputPath = Paths.get(runFilePath);
		Path outputPath = Paths.get(writingFile);

		//read the run file
		try(BufferedReader reader = Files.newBufferedReader(inputPath, UsefulConstants.CHARSET_ENCODING)) {
			//writer
			BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING);
			/*read one line at the time.*/
			String line = "";
			Collection<org.openrdf.model.Statement> graph = null;
			writer.write(GraphAssessment.attributeLine());
			writer.newLine();
			while((line = reader.readLine())!= null) {
				//for each ranked graph
				String graphPathString = parseOneTrecLineAndGetGraphPath(line, connection);
				graph = getGraphFromPathWithBlazegraph(graphPathString);

				String[] parts = line.split(" ");
				int docId = Integer.parseInt(parts[2]);
				int rank = Integer.parseInt(parts[3]);
				GraphAssessment assessment = assess(groundTruth, graph, docId, rank);

				//write down
				writer.write(assessment.toString());
				writer.newLine();
				writer.flush();
			}
			writer.close();
		} catch (IOException e) {
			System.err.println("Unable to read " + runFilePath);
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (RDFParseException e) {
			e.printStackTrace();
		} catch (RDFHandlerException e) {
			e.printStackTrace();
		}
	}


	/**Assess one run.
	 * This method uses one hash function to identify the paths of a graph
	 * <p>
	 * NB: we suppose the run file to be a standard Terrier file. That is, each line is in the form:
	 * </p>
	 * <p>
	 * query_no Qxy docID rank score model_name
	 * </p>
	 * <p>
	 * Where query_no means query number. xy is the number of the query (when the run is 
	 * composed by more than one query). docID is the id of the document/graph. rank is the rank and it is
	 * meant in ascending order. Score is the score of the document provided by the model. The last
	 * element is the name of the model.
	 * </p>
	 * 
	 * @param groundTruth string with the path of the ground truth graph.
	 * @param connection a Connection object to a RDB database where the paths of the graph file 
	 * to assess are located
	 * @param runFile file .res with the ranking of the documents.
	 * @param writingFile where to write the results
	 * */
	public static void assessRun(Collection<org.openrdf.model.Statement> groundTruth, 
			int step, 
			String subgraphsDirectory,
			String runFilePath, 
			String writingFile) {
		//open the run file
		Path inputPath = Paths.get(runFilePath);
		//open the file where to write
		Path outputPath = Paths.get(writingFile);

		//read the run file
		try(BufferedReader reader = Files.newBufferedReader(inputPath, UsefulConstants.CHARSET_ENCODING) ; 
				BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING)) {
			/*read one line at the time.*/
			String line = "";
			Collection<org.openrdf.model.Statement> graph = null;
			writer.write(GraphAssessment.attributeLine());
			writer.newLine();
			
			int counter = 0;

			//for each ranked graph
			while((line = reader.readLine())!= null && counter < 1000) {
				counter++;
				//get the informations from the line
				String[] parts = line.split(" ");
				int docId = Integer.parseInt(parts[2]);
				int rank = Integer.parseInt(parts[3]);
				
				if(rank%100==0) {
					System.out.println("checked " + rank + " documents");
				}
				//find the path to the graph
				int dirId = (int) Math.ceil((double)docId / step);
				String graphPathString = subgraphsDirectory + "/" + dirId + "/" + docId + ".ttl";
				
				//get the graph
				graph = getGraphFromPathWithBlazegraph(graphPathString);
				
				//do the assessment
				GraphAssessment assessment = assess(groundTruth, graph, docId, rank);
				
				//write down
				writer.write(assessment.toString());
				writer.newLine();
				writer.flush();
			}
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (RDFParseException e) {
			e.printStackTrace();
		} catch (RDFHandlerException e) {
			e.printStackTrace();
		}

	}


	private static Collection<org.openrdf.model.Statement> getGraphFromPathWithBlazegraph(String graphPath) throws RDFParseException, RDFHandlerException, IOException {
		//read the graph
		//open the input stream to the file
		File file = new File(graphPath);
		InputStream inputStream = new FileInputStream(file);
		//prepare a collector to contain the triples
		StatementCollector collector = new StatementCollector();
		RDFParser rdfParser = Rio.createParser(RDFFormat.TURTLE);
		//link the collector to the parser
		rdfParser.setRDFHandler(collector);
		//parse the file
		rdfParser.parse(inputStream, "");
		//now get the statements composing the graph
		return collector.getStatements();
	}

	/** Given a String representing one line of a trec result file, e.g.
	 * <p>
	 * 1 Q0 1244620 4 -3.982640822322769 BM25b0.75
	 * <p>
	 * where the meaning is
	 * <p>
	 * query_number, Q0, id of the graph, rank position, score, model used
	 * <p>
	 * returns a string representing the path of the graph. 
	 * This method is used for the clustering algorithm.
	 * */
	private static String parseOneTrecLineAndGetGraphPath(String line, Connection connection) throws SQLException {
		//read one line of the trec .res file
		//eg: 1 Q0 1244620 4 -3.982640822322769 BM25b0.75
		String[] parts = line.split(" ");
		int docId = Integer.parseInt(parts[2]);

		//use the id to retrieve the graph
		PreparedStatement statement = connection.prepareStatement(SQL_SELECT_CLUSTER_PATH);
		statement.setInt(1, docId);

		ResultSet rs = statement.executeQuery();
		String graphPathString = "";
		if(rs.next()) {
			//get path of the graph
			graphPathString = rs.getString(1);
		}
		return graphPathString;
	}




	/** Returns an object representing the assessment of a graph given a ground truth.
	 * */
	public static GraphAssessment assess(Collection<Statement> groundTruth, 
			Collection<Statement> model,
			int id,
			int rank) {
		GraphAssessment assessment = new GraphAssessment();

		//get an iterator over the graph
		Iterator<Statement> iterator = model.iterator();
		//triple counter
		int tp = 0;
		int modelSize = model.size();
		int gtSize = groundTruth.size();

		while(iterator.hasNext()) {
			Statement t = iterator.next();
			if(groundTruth.contains(t)) {
				tp++;
			}
		}

		//precision
		double precision = (double) tp / modelSize;

		//recall
		double recall = (double) tp / gtSize;

		Collection<Statement> union = new ArrayList<Statement>();
		union.addAll(groundTruth);
		union.addAll(model);
		int unionSize = union.size();
		double fMeasure = 2 * (precision * recall) / (precision + recall);
		double jaccard = (double) tp / unionSize;
		
		assessment.setPrecision(precision);
		assessment.setRecall(recall);
		assessment.setJaccard(jaccard);
		assessment.setfMeasure(fMeasure);
		assessment.setModelId(id);
		assessment.setModelRank(rank);
		assessment.setTotalMatchingTriples(tp);
		assessment.setGroundTruthTriples(groundTruth.size());

//		decideTheRelevanceByPrecision(assessment);
		decideTheRelevanceByFMeasure(assessment);

		return assessment;
	}

	protected static void decideTheRelevanceByPrecision(GraphAssessment ass) {
		double precision = ass.getPrecision();

		if(precision<0.1)
			ass.setAssessment("nr");
		else
			ass.setAssessment("r");
	}
	
	protected static void decideTheRelevanceByFMeasure(GraphAssessment ass) {
		Double fMeasure = ass.getfMeasure();

		if(fMeasure<0.1 || fMeasure.isNaN())
			ass.setAssessment("nr");
		else
			ass.setAssessment("r");
	}



}
