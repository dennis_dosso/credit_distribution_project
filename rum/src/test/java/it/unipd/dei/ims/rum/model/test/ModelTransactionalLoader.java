package it.unipd.dei.ims.rum.model.test;

import java.io.InputStream;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.tdb.TDBFactory;
import org.apache.jena.util.FileManager;

/**Load a model in memory using the transactional read and write and WITHOUT bulk loader. 
 * For small files only
 * */
public class ModelTransactionalLoader {

	public static void main(String[] args) {
		String supportModel = "/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/support/supportModel.ttl";
		String outputTDBDirectory = "/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/TDB_test/";
		
		Dataset dataset = TDBFactory.createDataset(outputTDBDirectory);
		dataset.begin(ReadWrite.WRITE);
		try {
			Model model = dataset.getDefaultModel();
			InputStream in = FileManager.get().open(supportModel);
			model.read(in, null, "N-TRIPLE");
			dataset.commit();
		} finally {
			dataset.end();
		}
		System.out.println("done");
	}
}
