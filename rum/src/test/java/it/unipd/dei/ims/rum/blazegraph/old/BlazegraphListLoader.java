package it.unipd.dei.ims.rum.blazegraph.old;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import org.apache.jena.ext.com.google.common.base.Stopwatch;
import org.openrdf.OpenRDFException;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFParseException;

import com.bigdata.journal.Options;
import com.bigdata.rdf.sail.BigdataSail;
import com.bigdata.rdf.sail.BigdataSailRepository;

/** Class to load a list of path representing file with RDF graph
 * in a triple store managed by Blazegraph.
 * */
public class BlazegraphListLoader {

	public static void main(String[] args) {

	}

	/** @param outputFile file .jnl where to store the triples.
	 * @param pathMap map with the files to load in the jnl storage
	 * */
	public static void loadListOfFilesInRDFDataset(String outputFile, Map<String, String> pathMap) {
		//open the database
		final Properties props = new Properties();
		props.put(Options.BUFFER_MODE, "DiskRW"); // persistent file system located journal
		props.put(Options.FILE, outputFile);
		final BigdataSail sail = new BigdataSail(props); // instantiate a sail
		final Repository repo = new BigdataSailRepository(sail); // create a Sesame repository

		RepositoryConnection cxn = null;


		try {
			//initialize the repository file
			repo.initialize();
			Stopwatch timer = Stopwatch.createStarted();
			//open the connection to the repository
			cxn = repo.getConnection();
			try {
				//for every file
				for( Map.Entry<String, String> entry : pathMap.entrySet()) {
					System.out.println("Now loading file " + entry.getKey());
					cxn.begin();
//					cxn.add(new File(entry.getValue()), null, org.openrdf.rio.RDFFormat.TURTLE);
					cxn.add(new File(entry.getValue()), null, org.openrdf.rio.RDFFormat.N3 );
					System.out.println("File read in " + timer);
					timer.reset().start();
					System.out.println("Now committing...");
					cxn.commit();
					System.out.println("Commit done in " + timer);
					timer.reset().start();
				}
				timer.stop();
				System.out.print("All done");
			} catch (OpenRDFException ex) {
				cxn.rollback();
				throw ex;
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				// close the repository connection
				cxn.close();
			}
		} catch (RepositoryException e) {
			System.err.print("Error initializing the repository " + outputFile);
			e.printStackTrace();
		} catch (RDFParseException e) {
			System.out.print("DEBUG: error in parsing RDF");
			e.printStackTrace();
		}  catch(OpenRDFException ex) {
			ex.printStackTrace();
		}
		finally {
			try {
				repo.shutDown();
			} catch (RepositoryException e) {
				e.printStackTrace();
			}
		}
	}
}
