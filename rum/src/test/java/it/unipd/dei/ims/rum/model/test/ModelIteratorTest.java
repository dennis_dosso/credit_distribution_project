package it.unipd.dei.ims.rum.model.test;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.tdb.TDBFactory;

/** In this class I test that a model read from TDB is correctly saved in memory
 * as triple (s, p, o).
 * 
 * What I have learnt: the use of the BulkLoader usually creates
 * some problem at the end of the database. That is: when you have a rdf file,
 * both ttl or n-triple, it doesn't matter, when you import it via TDBLoader
 * it tends to create a database that is corrupted at the end. That is, the last triple
 * is not saved correctly in the TDB graph. Thus, the iterator sees it like there 
 * is a next, but when you access it it returns error. 
 * 
 * 
 * 
 * */
public class ModelIteratorTest {

	public static void main(String[] args) {
		String directory = "/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/TDB";
		Dataset dataset = TDBFactory.createDataset(directory);

		dataset.begin(ReadWrite.READ);
		try {
			Model model = dataset.getDefaultModel();
			StmtIterator iterator = model.listStatements();
			int triples = 0;
			while(iterator.hasNext()) {
				try {
					Statement t = iterator.next();
					Resource s = t.getSubject();
					Resource p = t.getPredicate();
					RDFNode o = t.getObject();
					System.out.println(++triples + " " + t);


				}
				catch (Exception e) {
					e.printStackTrace();
					System.err.println("DEBUGGONE!!!!!!!");
				}
			}

//			try (QueryExecution qExec = QueryExecutionFactory.create(
//					"SELECT ?s ?p ?o WHERE { ?s ?p ?o}", 
//					dataset.getNamedModel("ciccio"))) {
//				ResultSet rs = qExec.execSelect() ;
//				ResultSetFormatter.out(rs) ;
//			}
//			
//			String queryString = "CONSTRUCT WHERE{ ?s ?p ?o }";
//			Query query = QueryFactory.create(queryString);
//			QueryExecution qe = QueryExecutionFactory.create(query, dataset.getDefaultModel());
//
//			Model resultModel = qe.execConstruct();
//			resultModel.write(System.out, "TTL");
		} finally {
			dataset.close();
		}

		System.out.println("done");
	}



}
