package it.unipd.dei.ims.rum.clustering.test;

import java.util.Map;

import org.openrdf.query.QueryEvaluationException;
import org.openrdf.repository.RepositoryException;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** In this class we read a dataset with Bzegraph and we create its clusters.
 * 
 * The things that I've learnt: even if the other projects seems to be
 * on 'DEBUG' mode, this doesn't affect the efficientcy.
 * 
 * */
public class BlazegraphClusteringExecutableTest {
	
	public static void main(String[] args) throws RepositoryException, QueryEvaluationException {
		Map<String, String> propertyMap = PropertiesUsefulMethods.getProperties();
		
		//get the database path
//		String databaseFile = propertyMap.get("blazegraph.database.file");
		String databaseFile = "/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/TDB_sesame/test.jnl";
		String jdbcString = "jdbc:postgresql://localhost:5432/disgenet?user=postgres&password=Ulisse92";
		String jdbcDriver = "com.mysql.jdbc.Driver";
		
		try {
			//compute the statistics
			System.out.println("start computing the statistics");
			BlazegraphStatisticsWithDatabaseSupportTest stat = new BlazegraphStatisticsWithDatabaseSupportTest();
			stat.getGraphStatistics(databaseFile, jdbcString, jdbcDriver);
			System.out.println("Statistics done");
			
//			//compute connectivity list
//			int k = Integer.parseInt(propertyMap.get("k"));
//			String measure = propertyMap.get("measure");
//			List<String> connectivityList = stat.getKConnectivityList(k, measure);
//			System.out.println("connectivity list computed");
//			
//			//create the cluster engine and find the central nodes
//			int lambda_in = Integer.parseInt(propertyMap.get("lambda.in"));
//			int lambda_out = Integer.parseInt(propertyMap.get("lambda.out"));
//			BlazegraphClusterEngine engine = new BlazegraphClusterEngine(databaseFile, lambda_in, lambda_out);
//			engine.computeTerminalAndSourceNodesSets(stat);
//			System.out.println("engine setup completed, now clustering...");
//			
//			//now clustering
//			String destinationDirectory = propertyMap.get("clusters.directory");	
//			int tau = Integer.parseInt(propertyMap.get("tau"));
//			engine.clustering(tau, connectivityList, destinationDirectory);
//			System.out.print("clustering has been completed");
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			
		}
		
	}
}
