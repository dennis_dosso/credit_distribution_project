package it.unipd.dei.ims.rum.models;

import java.util.Map;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.tdb.TDBFactory;
import org.apache.jena.tdb.TDBLoader;

public class ModelBulkLoader {

	/**Saves in memory in the databaseDirectory all the datasets contained in the pathMap.
	 * The name of the dataset will be the key of the entry, the path will be the value.
	 * 
	 * This class uses Jena.
	 * 
	 * */
	public static void listExecution(String datasetDirectory,  Map<String, String> pathMap) {
		Dataset dataset = TDBFactory.createDataset(datasetDirectory); 

		for( Map.Entry<String, String> entry : pathMap.entrySet()) {
			dataset.begin(ReadWrite.WRITE);
			
			System.out.println("starting the load of: " + entry.getKey());
			TDBLoader.loadModel(dataset.getNamedModel(entry.getKey()), entry.getValue(), true);
			dataset.commit();
		}
	}
}
