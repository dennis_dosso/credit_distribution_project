package it.unipd.dei.ims.query;

import org.apache.jena.ext.com.google.common.base.Stopwatch;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.tdb.TDBFactory;

/**Query a TDB asking how many triples it has.
 * 
 * Using Jena.
 * */
public class NumberOfTriplesQuery {
	public static void main(String[] args) {
		String directory = "/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/TDB";
		
		System.out.println("apertura del database");
		Stopwatch timer = Stopwatch.createStarted();

		Dataset dataset = TDBFactory.createDataset(directory);
		System.out.println("Database aperto in " + timer.stop());
		
		timer.reset().start();

		System.out.println("Opening the database in read mode");
		String path = null;
		
		dataset.begin(ReadWrite.READ);
		try {
			
			System.out.println("total number of triples in the TDB");
			try (QueryExecution qExec = QueryExecutionFactory.create(
					"SELECT (count(*) AS ?count) { ?s ?p ?o}", 
					dataset.getDefaultModel())) {
				ResultSet rs = qExec.execSelect() ;
				ResultSetFormatter.out(rs) ;
			}
			
		} finally {
			dataset.end();
		}

		System.out.println("everything done in "+ timer.stop());


	}
}
