package it.unipd.dei.ims.convertion.executable;

import it.unipd.dei.ims.rum.convertion.FromTSVToRDFConverter;

/**A class to be used when converting a file from TSV to RDF
 * */
public class FromTSVtoRDFExecution {

	public static void main(String[] args) {

		String inputFile = "/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/raw_data/title.episode.tsv";
		String outputdTDBDirectory = "/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/TDB/";
		String supportModel= "/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/support/supportModel.ttl";

		FromTSVToRDFConverter converter = new FromTSVToRDFConverter();
		converter.convertionToTDB(inputFile, outputdTDBDirectory, supportModel);

	}
}
