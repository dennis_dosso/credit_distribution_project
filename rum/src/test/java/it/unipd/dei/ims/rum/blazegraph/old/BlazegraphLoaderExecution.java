package it.unipd.dei.ims.rum.blazegraph.old;

import java.io.IOException;
import java.util.Map;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** Utilizes Blazegraph to load text rdf file in turtle syntax in a jnl repository.
 * */
public class BlazegraphLoaderExecution {

	public static void main(String[] args) {

		try {
		
			Map<String, String> pathMap = 
					PropertiesUsefulMethods.getSinglePropertyFileMap("properties/files_to_load.properties");
		
			Map<String, String> propertyMap = PropertiesUsefulMethods.getProperties();
			String outputFile = propertyMap.get("output.sesame.file");
			
			BlazegraphListLoader.loadListOfFilesInRDFDataset(outputFile, pathMap);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
