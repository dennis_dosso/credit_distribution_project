package it.unipd.dei.ims.rum.models;

import java.io.IOException;
import java.util.Map;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

public class ListOfModelsLoadingExecution {

	public static void main(String[] args) {
		try {
			Map<String, String> datasetMap = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/disgenet_paths.properties");
			Map<String, String> propertymap = PropertiesUsefulMethods.getProperties();
			String datasetDirectory = propertymap.get("dataset.directory");
			
			ModelBulkLoader.listExecution(datasetDirectory, datasetMap);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
