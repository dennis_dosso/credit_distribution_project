package it.unipd.dei.ims.files.executable;

import it.unipd.dei.ims.rum.utilities.PathUsefulMethods;

/**Main class to produce a list of files inside a directory.
 * 
 * Here you can put the strings to produce the various file lists.
 * 
 * */
public class FileListCreateExecutor {

	public static void main(String[] args) {
		
		//here you put the directory where to take the files and the file where to print them.
		//NB: don't put the file inside the same directory of the graphs, otherwise the program will read it too.
		String mainDirectory = "/Users/dennisdosso/Documents/RDF_DATASETS/RDF_CLUSTER/IMDB/clusters";
		String outputFile = "/Users/dennisdosso/Documents/RDF_DATASETS/RDF_CLUSTER/IMDB/clusters/list.txt";
		
		PathUsefulMethods.getPathsAndIDsOfAllFilesInsideDirectoryRecursivelyAndPrint(mainDirectory, outputFile);
	}
}
