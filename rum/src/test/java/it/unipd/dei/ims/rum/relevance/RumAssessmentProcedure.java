package it.unipd.dei.ims.rum.relevance;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.openrdf.model.Statement;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.RDFParser;
import org.openrdf.rio.Rio;
import org.openrdf.rio.helpers.StatementCollector;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** This class is the starting point to assess the performance of the queries performed on a certain database.
 * 
 * <p>
 * It deals with all the queries and all the methods that we are implemented in our procedure.
 * */
public class RumAssessmentProcedure {

	private String jdbcDriver;
	private String jdbcConnectingString;

	/** Path to the directory where we have the queries.
	 * */
	private String queriesDirectory;

	/** See {@link assessRuns}*/
	public void assessRuns() {
		RumAssessmentProcedure.assessRuns(this.jdbcConnectingString, this.queriesDirectory);
	}

	/** Method to assess the run correlated to a query.
	 * 
	 * @param driver the string describing what driver jdbc to use.
	 * @param connectionString the string with id and pasword to connect to database
	 * @param queriesDir directory where are contained the queries with all the files that we need.
	 * 
	 * */
	public static void assessRuns( String connectionString, String queriesDir) {
		Connection connection = null;

		try {
			//open connection to the database
			connection = DriverManager.getConnection(connectionString);

			//open the query directory
			File mainDirectory = new File(queriesDir);
			File[] files = mainDirectory.listFiles();
			for(File query_folder : files) {
				//each directory corresponds to a query
				if(query_folder.isDirectory()) {
					RumAssessmentProcedure.dealWithAQueryDirectory(query_folder, connection);
				}//end of the operations inside a query_folder
			}


		} catch (SQLException e) {
			e.printStackTrace();
		}  finally {
			if(connection!=null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}


	/**Explores a query folder and produces the results needed.
	 * <p>
	 * The structure of the query directory is expected to be as follows:
	 * </p>
	 * <p>
	 * A file ground_truth.ttl containing the ground truth graph
	 * <p>
	 * A file query.trec containing the query to read
	 * <p>
	 * a directory rankings
	 * <p>
	 * A directory Assessment
	 * */
	private static void dealWithAQueryDirectory(File queryFolder, Connection connection) {
		File[] files = queryFolder.listFiles();
		//file box to deal with all the data
		FileBox fileBox = new FileBox();
		
		//first of all, fill the object fileBox with all the pointers that we need
		for(File f : files) {
			RumAssessmentProcedure.dealWithSingleFileInsideQueryDirectory(f, connection, fileBox);
		}
		
		//now, we have all the pointers that we need. Let's roll
		executeEvaluation(connection, fileBox);
	}

	/** Given a file inside a query directory, it deal with the various
	 * possibilities and inserts it in the FileBox object.
	 * 
	 * @param file A generic file inside the query directory. It can be the ground truth graph, 
	 * the query file, the ranking directory, the assessment directory where to save the results*/
	private static void dealWithSingleFileInsideQueryDirectory(File file, Connection connection, FileBox fileBox) {
		
		Collection<Statement> groundTruth = null;
		try {
			//ground truth
			if(file.getName().equals("ground_truth.ttl")) {
				//read the graph
				//open the input stream to the file
				System.out.println("reading ground truth: " + file );
				InputStream inputStream = new FileInputStream(file);
				//prepare a collector to contain the triples
				StatementCollector collector = new StatementCollector();
				RDFParser rdfParser = Rio.createParser(RDFFormat.TURTLE);
				//link the collector to the parser
				rdfParser.setRDFHandler(collector);
				//parse the file
				rdfParser.parse(inputStream, "");
				//now get the statements composing the graph
				groundTruth = collector.getStatements();
				fileBox.setGroundTruth(groundTruth);
				System.out.println("ground truth of file " + file + " read");
			}
			//query file
			else if(file.getName().equals("query.trec")) {
				fileBox.setQueryFile(file);
			}
			//ranking directory
			else if(file.isDirectory() && file.getName().equals("rankings")) {
				dealWithRankingDirectory(file, connection, fileBox);
			}
			//assessment directory
			else if(file.isDirectory() && file.getName().equals("evaluations")) {
				fileBox.setEvaluationsDirectory(file);
			}
			//blanco support directory
			else if(file.getName().equals("blanco_support")) {
				fileBox.setBlancoSupportDirectory(file);;
			}
			
		} catch (FileNotFoundException e) {
			System.err.println("File " + file + " not found");
			e.printStackTrace();
		} catch (RDFParseException e) {
			System.err.println("Error parsing RDF file" + file );
			e.printStackTrace();
		} catch (RDFHandlerException e) {
			System.err.println("Error RDF in file " + file);
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Hub method to deal with the different possible methods that we are comparing*/
	public static void dealWithRankingDirectory(File file, Connection connection, FileBox fileBox) {
		File[] methods = file.listFiles();
		for(File method: methods) {
			if(method.getName().equals("clustering")) {
				dealWithClusteringResults(method, connection, fileBox);
			} else if(method.getName().equals("yosi")) {
				//TODO
			} else if(method.getName().equals("BANKS")) {
				//TODO
			} else if(method.getName().equals("blanco")) {
				dealWithBlancoResults(method, connection, fileBox);
			} 
		}
	}
	
	/** Method to save the blanco directory result files.
	 * */
	private static void dealWithBlancoResults(File clusteringDirectory, Connection connection, FileBox fileBox) {
		//read the files inside the directory
		//(there can be more than 1 file, if we have used different parameters for the method)
		File[] clusterRankings = clusteringDirectory.listFiles();
		fileBox.setBlancoResults(clusterRankings);
	}
	
	/** Method to save the clustering directory result files.
	 * */
	private static void dealWithClusteringResults(File clusteringDirectory, Connection connection, FileBox fileBox) {
		//read the files inside the directory
		File[] clusterRankings = clusteringDirectory.listFiles();
		fileBox.setClusteringResults(clusterRankings);
	}
	
	/** Given the information contained in the provided file box, executes the evaluation of the results*/
	private static void executeEvaluation(Connection connection, FileBox fileBox) {
		//execute evaluation of the clustering algorithm
		//TODO togliere commento dopo test
		executeClusteringEvaluation(connection, fileBox);
		//TODO gli altri metodi per gli altri algoritmi. Yosi, Blanco etc.
		executeBlancoEvalutation(connection, fileBox);
	}
	
	private static void executeBlancoEvalutation(Connection connection, FileBox fileBox) {
		//get the files .res with the informations
		File[] results = fileBox.getBlancoResults();
		if(results!=null) {
			for(File result : results) {
				if(result.getName().equals(".DS_Store"))
					continue;
				executeSingleResultBlancoEvaluation(connection, fileBox, result);
			}
		}
	}
	
	private static void executeClusteringEvaluation(Connection connection, FileBox fileBox) {
		//get the cluster.res files
		File[] results = fileBox.getClusteringResults();
		if(results!=null) {
			for(File result : results) {
				executeSingleResultClusteringEvaluation(connection, fileBox, result);
			}
		}
	}
	
	
	/** Execute the evaluation of a single clustering.res file.
	 * */
	private static void executeSingleResultClusteringEvaluation(Connection connection, FileBox fileBox, File runFile) {
		//to call the GraphRelevanceAssessor, we need: the run file, the ground truth, 
		//the writing file and the place where to take our graphs
		
		String writingFile = generateWritingFile(fileBox, runFile, "clustering");
		Collection<Statement> groundTruth = fileBox.getGroundTruth();
		if(groundTruth!=null) {
			RunAssessor.assessRun(groundTruth, connection, runFile.getAbsolutePath(), writingFile);
		}
		else {
			System.out.println("[WARNING]: Unable to produce results for file " + runFile + ": groundTruth not available");
		}
	}
	
	/** Execute the evaluation of a single blanco.res file.
	 * */
	private static void executeSingleResultBlancoEvaluation(Connection connection, FileBox fileBox, File runFile) {
		//to call the GraphRelevanceAssessor, we need: the run file, the ground truth, 
				//the writing file and the place where to take our graphs
		String writingFile = generateWritingFile(fileBox, runFile, "blanco");
		Collection<Statement> groundTruth = fileBox.getGroundTruth();
		if(groundTruth!=null) {
			String subgraphDirectory = fileBox.getBlancoSupportDirectory().getAbsolutePath() + "/subgraphs";
			RunAssessor.assessRun(groundTruth, 2048, subgraphDirectory, runFile.getAbsolutePath(), writingFile);
		}
		else {
			System.out.println("[WARNING]: Unable to produce results for file " + runFile + ": groundTruth not available");
		}
	}
	
	/** This methods creates and returns a string representing the path of a new file where to write the results
	 * of a ranking. 
	 * 
	 * @param runFile the file where we are taking the ranking. Its name is necessary becaue if it is example.res, the writing file will be 
	 * named example.eval
	 * @param extension THe name of the directory where the file has to be created inside the evaluation directory. 
	 * E.g. clustering or blanco.*/
	private static String generateWritingFile(FileBox fileBox, File runFile, String extension) {
		//take the evaluation directory
		File evalDirectory = fileBox.getEvaluationsDirectory();
		if(evalDirectory == null) {
			//create the evaluations directory
			File fileString = fileBox.getQueryFile();
			File queryDirectory = fileString.getParentFile();
			evalDirectory = new File(queryDirectory.getAbsolutePath() + "/evaluations");
			evalDirectory.mkdir();
		}
		
		//check the directory for clustering
		String evalPath = evalDirectory.getAbsolutePath() + "/" + extension;
		File subEvalDirectory  = new File(evalPath);

		if(!subEvalDirectory.exists()) {
			subEvalDirectory.mkdir();
		}

		//generate writing file
		String filePath = FilenameUtils.removeExtension(runFile.getName());
		filePath = evalPath + "/" + filePath + ".eval";
		return filePath;
	}

	
	
	
	//#############################

	public static void main(String[] args) throws IOException {
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/assessment.properties");
		
		String queriesDirectory = map.get("queries.directory");
		String jdbcConnection = map.get("jdbc.connection.string");
		
		//XXX DEBUG
//		queriesDirectory = "/Users/dennisdosso/Documents/RDF_DATASETS/LinkedMDB/queries";
//		queriesDirectory = "/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/queries_test";
//		jdbcConnection = "jdbc:postgresql://localhost:5432/LinkedMDB?user=postgres&password=Ulisse92";

		RumAssessmentProcedure.assessRuns(jdbcConnection, queriesDirectory);
		System.out.println("done");
	}
	
	
	





	//#############################

	public String getJdbcDriver() {
		return jdbcDriver;
	}

	public void setJdbcDriver(String jdbcDriver) {
		this.jdbcDriver = jdbcDriver;
	}

	public String getJdbcConnectingString() {
		return jdbcConnectingString;
	}

	public void setJdbcConnectingString(String jdbcConnectingString) {
		this.jdbcConnectingString = jdbcConnectingString;
	}

	public String getQueriesDirectory() {
		return queriesDirectory;
	}

	public void setQueriesDirectory(String queriesDirectory) {
		this.queriesDirectory = queriesDirectory;
	}
}
