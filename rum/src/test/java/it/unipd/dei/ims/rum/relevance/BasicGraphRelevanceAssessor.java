package it.unipd.dei.ims.rum.relevance;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;

/** Contains the methods to assess a graph, i.e. understand if it is relevant or not. 
 * 
 * <p>
 * It uses Jena as support library and reads pah from memory, not from database.
 * <p>
 * 
 * @author Dennis D.
 * 
 * */
@Deprecated
public class BasicGraphRelevanceAssessor {
	
	/** The Ground Truth against which we can assess*/
	private Model gt;
	
	public BasicGraphRelevanceAssessor (Model t) {
		gt = t;
	}
	
	public BasicGraphRelevanceAssessor (String st, String lang) {
		Model m = ModelFactory.createDefaultModel();
		m.read(st, null, lang);
		gt = m;
	}
	
	/** Checks if the parameter model m is relevant given the ground truth.
	 * 
	 * @param m A model to assess.
	 * */
	public GraphAssessment assess(Model m, int id, int rank) {
		//list all of the triples in the ground truth
		StmtIterator gtIter = gt.listStatements();
		//iterator over the answer graph. Usually it can be simpler of the GT.
		StmtIterator mIter = m.listStatements();
		Statement mTriple;
		//counter to check how many triples of the ground truth are contained in the graph (true positive)
		int tp = 0;
		
		while(mIter.hasNext()) {
			//get an answer graph triple
			mTriple = mIter.next();
			//check if the answer triple is contained in the ground truth
			if( gt.contains(mTriple) ) {
				tp++;
			}
		}
		
		//precision
		double precision = (double) tp / m.size();
		
		//recall
		double recall = (double) tp / gt.size();
		
		//index of jaccard
		Model union = ModelFactory.createDefaultModel();
		union.add(gt).add(m);
		double jaccard = (double) tp / union.size();
		
		GraphAssessment ass = new GraphAssessment();
		ass.setGt(gt);
		ass.setModel(m);
		ass.setPrecision(precision);
		ass.setRecall(recall);
		ass.setJaccard(jaccard);
		ass.setModelId(id);
		ass.setModelRank(rank);
		ass.setTotalMatchingTriples(tp);
		
		decideForTheRelevanceByPrecision(ass);
		
		return ass;
	}
	
	/** Given the informations in the object GraphAssessment, decides if this
	 * graph is relevant and writes the relevance assessment based 
	 * on the value of the Jaccard index. 
	 * */
	protected void decideForTheRelevanceByJaccard(GraphAssessment ass) {
		
		double jacc = ass.getJaccard();
		
		if(jacc < 0.1)
			ass.setAssessment("nr");
		else if (jacc >= 0.1 && jacc < 0.3)
			ass.setAssessment("pr");
		else if(jacc >= 0.3 && jacc < 0.4)
			ass.setAssessment("fr");
		else if(jacc > 0.4)
			ass.setAssessment("hr");
	}
	
	protected void decideForTheRelevanceByPrecision(GraphAssessment ass) {
		double precision = ass.getPrecision();
		
		if(precision<0.5)
			ass.setAssessment("nr");
		else
			ass.setAssessment("r");
	}
	
	
	
	
	
	
	
	
	public Model getGt() {
		return gt;
	}

	public void setGt(Model gt) {
		this.gt = gt;
	}
	
	
}
