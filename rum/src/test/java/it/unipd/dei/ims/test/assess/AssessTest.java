package it.unipd.dei.ims.test.assess;

import it.unipd.dei.ims.rum.relevance.BasicRunAssessor;
import it.unipd.dei.ims.rum.utilities.PathUsefulMethods;

public class AssessTest {

	/**Test class to see how we are doing with the assessment.
	 * */
	public static void main(String[] args) {
		//this is a test
		String groundTruthPath = "/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/ground_truth/5.ttl";
		
//		String graphDirectory = "/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/results/query_5/elbassuoni_graphs_5/";
//		String runPathString = "/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/results/query_5/ranking_elbassuoni_5.res";
		
		String graphDirectory = "/Users/dennisdosso/Documents/RDF_DATASETS/RDF_CLUSTER/IMDB/clusters";
		String runPathString = "/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/results/query_5/ranking_cluster_5.res";
		
		String writingFile = "/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/results/query_5/assessment.txt";
		
		BasicRunAssessor.assessRun(groundTruthPath, graphDirectory, runPathString, writingFile);
		
	}
}
