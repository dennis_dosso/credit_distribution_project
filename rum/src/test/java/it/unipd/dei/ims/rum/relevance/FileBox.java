package it.unipd.dei.ims.rum.relevance;

import java.io.File;
import java.util.Collection;

import org.openrdf.model.Statement;

/** This class represents a collection of other objects that are found inside
 * a query directory. It is used to collect all the necessary data from the directory
 * and then use them to perform the assessment script.
 * 
 * */
public class FileBox {
	/**The collection of RDF triples representing the ground truth graph.
	 * */
	protected Collection<Statement> groundTruth;
	
	/** Path of the ground truth.*/
	protected String groundTruthPath;
	
	/**File to the query used inside a query directory.*/
	protected File queryFile;

	/** Array of files representing the results given by the clustering algorithm
	 * contained in the clustering directory.
	 * */
	protected  File[] clusteringResults;
	
	/** Array of files representing the results given by the BE_LM
	 * contained in the blanco result directory.
	 * */
	protected File[] blancoResults;
	
	/** Directory where we have the evaluations of our algorithms.*/
	protected File evaluationsDirectory;
	
	/** Directory where we have all the support informations for the 
	 * performance of the BE_LM algorithm.
	 * */
	protected File blancoSupportDirectory;
	
	//@@@@@@@@@@@@@@@@
	
	public Collection<Statement> getGroundTruth() {
		return groundTruth;
	}

	public void setGroundTruth(Collection<Statement> groundTruth) {
		this.groundTruth = groundTruth;
	}

	public File getQueryFile() {
		return queryFile;
	}

	public void setQueryFile(File queryFile) {
		this.queryFile = queryFile;
	}

	public File[] getClusteringResults() {
		return clusteringResults;
	}

	public void setClusteringResults(File[] clusteringResults) {
		this.clusteringResults = clusteringResults;
	}

	public File getEvaluationsDirectory() {
		return evaluationsDirectory;
	}

	public void setEvaluationsDirectory(File evaluationsDirectory) {
		this.evaluationsDirectory = evaluationsDirectory;
	}

	public String getGroundTruthPath() {
		return groundTruthPath;
	}

	public void setGroundTruthPath(String groundTruthPath) {
		this.groundTruthPath = groundTruthPath;
	}

	public File[] getBlancoResults() {
		return blancoResults;
	}

	public void setBlancoResults(File[] blancoResults) {
		this.blancoResults = blancoResults;
	}

	public File getBlancoSupportDirectory() {
		return blancoSupportDirectory;
	}

	public void setBlancoSupportDirectory(File blancoSupportDirectory) {
		this.blancoSupportDirectory = blancoSupportDirectory;
	}
	
	
	
}
