package it.unipd.dei.ims.blazegraph.test;

import java.util.Properties;

import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.sail.SailException;

import com.bigdata.journal.Options;
import com.bigdata.rdf.sail.BigdataSail;
import com.bigdata.rdf.sail.BigdataSailRepository;

public class BlazegraphNumberOfTriplesQueryTest {

	public static void main(String[] args) throws RepositoryException, SailException {

		final Properties props = new Properties();
		props.put(Options.BUFFER_MODE, "DiskRW"); // persistent file system located journal
		props.put(Options.FILE, "/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/rdf_database/dataset.jnl"); // journal file location

		final BigdataSail sail = new BigdataSail(props); // instantiate a sail
		final Repository repo = new BigdataSailRepository(sail); // create a Sesame repository

		repo.initialize();

		try {
			// open repository connection
			RepositoryConnection cxn;

			// open connection
			if (repo instanceof BigdataSailRepository) {
				cxn = ((BigdataSailRepository) repo).getReadOnlyConnection();
			} else {
				cxn = repo.getConnection();
			}

			// evaluate sparql query
			try {

				final TupleQuery tupleQuery = cxn
						.prepareTupleQuery(QueryLanguage.SPARQL,
//								"select ?s ?p ?o where { ?s ?p ?o . } LIMIT 400 OFFSET 1000000");//to list all the triples
//								"select ?s ?p ?o where { <http://rdf.disgenet.org/v5.0.0/void/HPO> ?p ?o . }");
//								"SELECT (count(*) AS ?count) { ?s ?p ?o .}");//to have the count of all the triples
//								"SELECT DISTINCT (?p AS ?DistinctEdges)  { ?s ?p ?o } LIMIT 200");//to have the list of all distinct labels
//								"SELECT p? (count(distinct ?p) as ?count) {?s ?p ?o}");
//								"select ?s ?p ?o where { ?s ?p <IMDB:/Miss_Jerry> . } LIMIT 50");//query about Audrey Hepburn in IMDB 
//								"select ?s ?p ?o where { <IMDB:/nm0000030> ?p ?o FILTER (str(?o) = \"IMDB:/Audrey Hepburn\") . } LIMIT 50");//query about Audrey Hepburn in IMDB 
								"select ?p ?o where { <https://www.imdb.com/ordering/tt0017048/1> ?p ?o . } LIMIT 100");
//								"select ?s ?p where { ?s ?p <https://www.imdb.com/title/tt0024150> . } LIMIT 100");
			
								TupleQueryResult result = tupleQuery.evaluate();
			int counter = 0;
			try {
				while (result.hasNext()) {
					BindingSet bindingSet = result.next();
					counter++;
					System.err.println(bindingSet);
				}
				System.out.println("triples: " + counter);
			} finally {
				result.close();
			}

			} catch (MalformedQueryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (QueryEvaluationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				// close the repository connection
				cxn.close();
			}

		} finally {
			repo.shutDown();
		}
	}

}
