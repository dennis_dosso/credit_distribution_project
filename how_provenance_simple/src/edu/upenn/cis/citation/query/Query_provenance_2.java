package edu.upenn.cis.citation.query;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;
import org.roaringbitmap.RoaringBitmap;
import edu.upenn.cis.citation.Corecover.Argument;
import edu.upenn.cis.citation.Corecover.Query;
import edu.upenn.cis.citation.Corecover.Subgoal;
import edu.upenn.cis.citation.Operation.Conditions;
import edu.upenn.cis.citation.citation_view1.Head_strs;
//import edu.upenn.cis.citation.multi_thread.Loading_base_relations;
import edu.upenn.cis.citation.views.Query_converter;
import edu.upenn.cis.citation.views.Single_view;

public class Query_provenance_2 {
  
  
  
  
  public static ResultSet get_query_provenance(Query query, Connection c, PreparedStatement pst) throws SQLException
  {
    String sql = null;
    
//    if(!test_case)
    sql = Query_converter.data2sql_with_provenance_col(query);
//    else
//      sql = Query_converter.data2sql_with_provenance_col_test(query);
    
    System.out.println(sql);
    
    pst = c.prepareStatement(sql);
    
    ResultSet rs = pst.executeQuery();
    
    return rs;
    
  }
  
  public static HashMap<String, String[]>[] get_query_related_base_content(Query query, HashMap<String, HashMap<String, String[]>> base_relation_conteny)
  {
    HashMap<String, String[]>[] related_base_content = new HashMap[query.body.size()];
    
    for(int i = 0; i < query.body.size(); i++)
    {
      Subgoal subgoal = (Subgoal) query.body.get(i);
      
      String origin_name = query.subgoal_name_mapping.get(subgoal.name);
      
      HashMap<String, String[]> base_content = base_relation_conteny.get(origin_name);
      
      related_base_content[i] = base_content;
      
    }
    
    return related_base_content;
    
    
  }
  
  public static ResultSet get_query_provenance(Single_view view, Connection c, PreparedStatement pst) throws SQLException
  {
    String sql = null;
    
    sql = Query_converter.data2sql_with_provenance_col(view);
    
    pst = c.prepareStatement(sql);
    
//    System.out.println(sql);
    
    ResultSet rs = pst.executeQuery();
    
    return rs;
    
  }
  
  public static ResultSet get_query_provenance_with_condition(Single_view view, String condition_string, Connection c, PreparedStatement pst) throws SQLException
  {
    String sql = null;
    
    sql = Query_converter.data2sql_with_provenance_col_with_condition_string(view, condition_string);//(view);
    
    pst = c.prepareStatement(sql);
    
//    System.out.println(sql);
    
    ResultSet rs = pst.executeQuery();
    
    return rs;
    
  }
  
  public static ResultSet get_query_provenance_materialized(Single_view view, Connection c, PreparedStatement pst) throws SQLException
  {
    String sql = null;
    
    sql = Query_converter.data2sql_with_provenance_col_materialized(view);
    
    pst = c.prepareStatement(sql);
    
//    System.out.println(sql);
    
    ResultSet rs = pst.executeQuery();
    
    return rs;
    
  }

  public static void retrieve_query_instance_provenance(ResultSet rs, Query query, HashMap<Head_strs, String[][]> group_value_prov_mappings, HashMap<Head_strs, Head_strs> query_instance, HashMap<String, HashMap<Head_strs, HashSet<Integer>>>[] query_prov_index) throws SQLException
  {
    
    while(rs.next())
    {
      Vector<String> grouping_values = new Vector<String>();
      for(int i = 0; i<query.head.args.size(); i++)
      {
        grouping_values.add(rs.getString(i+1));
      }
      
      Head_strs grouping_value_strings = new Head_strs(grouping_values);
      
      int start_pos = query.head.args.size();

      Array array = rs.getArray(start_pos + 1);
      
      
      String[][] prov_lists = (String[][]) array.getArray();
      
      group_value_prov_mappings.put(grouping_value_strings, prov_lists);

      Build_query_prov_index.build_index_for_query_prov(query, query_prov_index, grouping_value_strings, prov_lists);
      
      if(query.head.has_agg)
      {
                
        start_pos = query.head.args.size() + 1;
        
        Vector<String> agg_results = new Vector<String>(); 
        
        for(int i = start_pos; i<start_pos + query.head.agg_args.size(); i++)
        {
          agg_results.add(rs.getString(i+1));
        }
        
        Head_strs agg_res = new Head_strs(agg_results);
        
        query_instance.put(grouping_value_strings, agg_res);
      }
      else
      {
        query_instance.put(grouping_value_strings, null);
      }
    }
  }
  
  public static void retrieve_query_instance_provenance2(ResultSet rs, Query query, HashMap<Head_strs, Integer> group_value_prov_count_mappings, HashMap<Head_strs, String[][]> group_value_prov_mappings, HashMap<Head_strs, Head_strs> query_instance, HashMap<String, HashSet<Integer>>[] query_prov_index) throws SQLException
  {
    
    while(rs.next())
    {
      Vector<String> grouping_values = new Vector<String>();
      for(int i = 0; i<query.head.args.size(); i++)
      {
        grouping_values.add(rs.getString(i+1));
      }
      
      Head_strs grouping_value_strings = new Head_strs(grouping_values);
      
      int start_pos = query.head.args.size();

      Array array = rs.getArray(start_pos + 1);
      
      
      String[][] prov_lists = (String[][]) array.getArray();
      
      group_value_prov_mappings.put(grouping_value_strings, prov_lists);

      Build_query_prov_index.build_index_for_query_prov2(query, query_prov_index, grouping_value_strings, prov_lists);
      
      group_value_prov_count_mappings.put(grouping_value_strings, prov_lists.length);
      
      if(query.head.has_agg)
      {
                
        start_pos = query.head.args.size() + 1;
        
        Vector<String> agg_results = new Vector<String>(); 
        
        for(int i = start_pos; i<start_pos + query.head.agg_args.size(); i++)
        {
          agg_results.add(rs.getString(i+1));
        }
        
        Head_strs agg_res = new Head_strs(agg_results);
        
        query_instance.put(grouping_value_strings, agg_res);
        
        
      }
      else
      {
        query_instance.put(grouping_value_strings, null);
      }
    }
  }
  
  
  static void retrieve_provenance_sets(HashSet<String>[] query_prov_sets, String[][] provenance_sets)
  {
    for(int i = 0; i<provenance_sets.length; i++)
    {
      String[] provenance_set = provenance_sets[i];
      
      for(int j = 0; j<provenance_set.length; j++)
      {
        query_prov_sets[j].add(provenance_set[j]);
      }
    }
  }
  
  
  public static void retrieve_query_instance_with_pure_provenance(ResultSet rs, Query query, HashMap<String, String[][]> group_value_prov_mappings, HashMap<String, Head_strs> query_instance) throws SQLException
//public static void retrieve_query_instance_provenance3(ResultSet rs, Query query, HashMap<Head_strs, Integer> group_value_prov_count_mappings, HashMap<Head_strs, String[][]> group_value_prov_mappings, HashMap<Head_strs, Head_strs> query_instance, HashMap<String, long[]>[] query_prov_index, HashMap<Head_strs, Integer> query_group_value_initial_count) throws SQLException
{
  
  StringBuilder stringbuilder = new StringBuilder();
  
  double index_time = 0;
  
  int rid = 0;
  
  while(rs.next())
  {
    String grouping_value_strings = new String();
//    Vector<String> grouping_values = new Vector<String>();
    for(int i = 0; i<query.head.args.size(); i++)
    {
      if(i >= 1)
        grouping_value_strings = Head_strs.concatenate_strings(stringbuilder, grouping_value_strings, rs.getString(i+1));
      else
        grouping_value_strings = rs.getString(i+1);
    }
    
//    Head_strs grouping_value_strings = new Head_strs(grouping_values);
    
//    query_group_value_initial_count.put(grouping_value_strings, 0);
    
    int start_pos = query.head.args.size();

    Array array = rs.getArray(start_pos + 1);
    
//    if(query.head.has_agg)
//    {
      String[][] prov_lists = (String[][]) array.getArray();
      
      group_value_prov_mappings.put(grouping_value_strings, prov_lists);
//    }
    
//    else
//    {
//      String[] prov_list = (String[]) array.getArray();
//      
//      String[][] prov_lists = new String[1][];
//      
//      prov_lists[0] = prov_list;
//      
//      group_value_prov_mappings.put(grouping_value_strings, prov_lists);
//    }
    
    

//    double t1 = System.nanoTime();
    
//    Build_query_prov_index.build_index_for_query_prov3(stringbuilder, query, query_prov_index, grouping_value_strings, prov_lists);
    
    
    
//    double t2 = System.nanoTime();
    
//    SizeOf.skipStaticField(true); //java.sizeOf will not compute static fields
//    SizeOf.skipFinalField(true); //java.sizeOf will not compute final fields
//    SizeOf.skipFlyweightObject(true); //java.sizeOf will not compute well-known flyweight objects
//    System.out.println(SizeOf.humanReadable(SizeOf.deepSizeOf(query_prov_index))); //this will print the object size in bytes
//    System.out.println(SizeOf.humanReadable(SizeOf.deepSizeOf(group_value_prov_mappings))); 
//    index_time += (t2 - t1);
    
//    group_value_prov_count_mappings.put(grouping_value_strings, prov_lists.length);
    
    if(query.head.has_agg)
    {
              
      start_pos = query.head.args.size() + 1;
      
      Vector<String> agg_results = new Vector<String>(); 
      
      for(int i = start_pos; i<start_pos + query.head.agg_args.size(); i++)
      {
        agg_results.add(rs.getString(i+1));
      }
      
      Head_strs agg_res = new Head_strs(agg_results);
      
      query_instance.put(grouping_value_strings, agg_res);
      
      
    }
    else
    {
      query_instance.put(grouping_value_strings, null);
    }
    
    rid++;
    
//    System.out.println(grouping_value_strings);
  }
  
  index_time = index_time * 1.0/1000000000;
  
  System.out.println(index_time);
}
  
  
  
  public static void retrieve_query_instance_provenance3(ResultSet rs, Query query, HashMap<String, Integer> group_value_prov_count_mappings, HashMap<String, String[][]> group_value_prov_mappings, HashMap<String, Head_strs> query_instance, HashMap<String, HashMap<String, RoaringBitmap>>[] query_prov_index, HashMap<String, Integer> query_group_value_initial_count) throws SQLException
//  public static void retrieve_query_instance_provenance3(ResultSet rs, Query query, HashMap<Head_strs, Integer> group_value_prov_count_mappings, HashMap<Head_strs, String[][]> group_value_prov_mappings, HashMap<Head_strs, Head_strs> query_instance, HashMap<String, long[]>[] query_prov_index, HashMap<Head_strs, Integer> query_group_value_initial_count) throws SQLException
  {
    
    StringBuilder stringbuilder = new StringBuilder();
    
    double index_time = 0;
    
    int rid = 0;
    
    while(rs.next())
    {
      String grouping_value_strings = new String();
//      Vector<String> grouping_values = new Vector<String>();
      for(int i = 0; i<query.head.args.size(); i++)
      {
        if(i >= 1)
          grouping_value_strings = Head_strs.concatenate_strings(stringbuilder, grouping_value_strings, rs.getString(i+1));
        else
          grouping_value_strings = rs.getString(i+1);
      }
      
//      Head_strs grouping_value_strings = new Head_strs(grouping_values);
      
      query_group_value_initial_count.put(grouping_value_strings, 0);
      
      int start_pos = query.head.args.size();

      Array array = rs.getArray(start_pos + 1);
      
      
      String[][] prov_lists = (String[][]) array.getArray();
      
      group_value_prov_mappings.put(grouping_value_strings, prov_lists);

      double t1 = System.nanoTime();
      
      Build_query_prov_index.build_index_for_query_prov3(stringbuilder, query, query_prov_index, grouping_value_strings, prov_lists);
      
      
      
      double t2 = System.nanoTime();
      
//      SizeOf.skipStaticField(true); //java.sizeOf will not compute static fields
//      SizeOf.skipFinalField(true); //java.sizeOf will not compute final fields
//      SizeOf.skipFlyweightObject(true); //java.sizeOf will not compute well-known flyweight objects
//      System.out.println(SizeOf.humanReadable(SizeOf.deepSizeOf(query_prov_index))); //this will print the object size in bytes
//      System.out.println(SizeOf.humanReadable(SizeOf.deepSizeOf(group_value_prov_mappings))); 
      index_time += (t2 - t1);
      
      group_value_prov_count_mappings.put(grouping_value_strings, prov_lists.length);
      
      if(query.head.has_agg)
      {
                
        start_pos = query.head.args.size() + 1;
        
        Vector<String> agg_results = new Vector<String>(); 
        
        for(int i = start_pos; i<start_pos + query.head.agg_args.size(); i++)
        {
          agg_results.add(rs.getString(i+1));
        }
        
        Head_strs agg_res = new Head_strs(agg_results);
        
        query_instance.put(grouping_value_strings, agg_res);
        
        
      }
      else
      {
        query_instance.put(grouping_value_strings, null);
      }
      
      rid++;
      
//      System.out.println(grouping_value_strings);
    }
    
    index_time = index_time * 1.0/1000000000;
    
    System.out.println(index_time);
  }
  
  public static void retrieve_query_instance_provenance3(ResultSet rs, Query query, HashMap<String, Integer> group_value_prov_count_mappings, HashMap<String, String[][]> group_value_prov_mappings, HashSet<String>[] query_provenance_sets, HashMap<String, Head_strs> query_instance, HashMap<String, Integer> query_group_value_initial_count) throws SQLException
//public static void retrieve_query_instance_provenance3(ResultSet rs, Query query, HashMap<Head_strs, Integer> group_value_prov_count_mappings, HashMap<Head_strs, String[][]> group_value_prov_mappings, HashMap<Head_strs, Head_strs> query_instance, HashMap<String, long[]>[] query_prov_index, HashMap<Head_strs, Integer> query_group_value_initial_count) throws SQLException
{
  
  StringBuilder stringbuilder = new StringBuilder();
  
  while(rs.next())
  {
    String grouping_value_strings = new String();
//    Vector<String> grouping_values = new Vector<String>();
    for(int i = 0; i<query.head.args.size(); i++)
    {
      if(i >= 1)
        grouping_value_strings = Head_strs.concatenate_strings(stringbuilder, grouping_value_strings, rs.getString(i+1));
      else
        grouping_value_strings = rs.getString(i+1);
    }
    
//    Head_strs grouping_value_strings = new Head_strs(grouping_values);
    
    query_group_value_initial_count.put(grouping_value_strings, 0);
    
    int start_pos = query.head.args.size();

    Array array = rs.getArray(start_pos + 1);
    
    
    String[][] prov_lists = (String[][]) array.getArray();
    
    retrieve_provenance_sets(query_provenance_sets, prov_lists);
    
    group_value_prov_mappings.put(grouping_value_strings, prov_lists);
    
    group_value_prov_count_mappings.put(grouping_value_strings, prov_lists.length);
    
    if(query.head.has_agg)
    {
              
      start_pos = query.head.args.size() + 1;
      
      Vector<String> agg_results = new Vector<String>(); 
      
      for(int i = start_pos; i<start_pos + query.head.agg_args.size(); i++)
      {
        agg_results.add(rs.getString(i+1));
      }
      
      Head_strs agg_res = new Head_strs(agg_results);
      
      query_instance.put(grouping_value_strings, agg_res);
      
      
    }
    else
    {
      query_instance.put(grouping_value_strings, null);
    }
  }
  
}

//  public static void retrieve_query_instance_provenance4(ResultSet rs, Query query, HashMap<Head_strs, Integer> group_value_prov_count_mappings, HashMap<Head_strs, String[][]> group_value_prov_mappings, HashMap<Head_strs, Head_strs> query_instance, Vector<String>[] query_provs, Vector<long[]>[] indexes) throws SQLException
//  {
//    
//    int prov_length = 0;
//    
//    while(rs.next())
//    {
//      Vector<String> grouping_values = new Vector<String>();
//      for(int i = 0; i<query.head.args.size(); i++)
//      {
//        grouping_values.add(rs.getString(i+1));
//      }
//      
//      Head_strs grouping_value_strings = new Head_strs(grouping_values);
//      
//      int start_pos = query.head.args.size();
//
//      Array array = rs.getArray(start_pos + 1);
//      
//      
//      String[][] prov_lists = (String[][]) array.getArray();
//      
//      prov_length += prov_lists.length;
//      
//      group_value_prov_mappings.put(grouping_value_strings, prov_lists);
//
//      group_value_prov_count_mappings.put(grouping_value_strings, prov_lists.length);
//      
//      if(query.head.has_agg)
//      {
//                
//        start_pos = query.head.args.size() + 1;
//        
//        Vector<String> agg_results = new Vector<String>(); 
//        
//        for(int i = start_pos; i<start_pos + query.head.agg_args.size(); i++)
//        {
//          agg_results.add(rs.getString(i+1));
//        }
//        
//        Head_strs agg_res = new Head_strs(agg_results);
//        
//        query_instance.put(grouping_value_strings, agg_res);
//        
//        
//      }
//      else
//      {
//        query_instance.put(grouping_value_strings, null);
//      }
//    }
//    
//    System.out.println("query_done");
//    
//    double time1 = System.nanoTime();
//    
//    Build_query_prov_index.build_index_for_query_prov4(query, query_provs, indexes, group_value_prov_mappings, prov_length);
//    
//    double time2 = System.nanoTime();
//
//    double time = (time2 - time1)*1.0/1000000000;
//    
//    System.out.println("time::" + time);
//    
//    System.out.println(query_provs[0].size());
//    System.out.println(indexes[0].size());
//  }

  
  public static void retrieve_query_instance(ResultSet rs, Query query, HashMap<String, Head_strs> query_instance) throws SQLException
  {
    
    StringBuilder stringbuilder = new StringBuilder();
    
    while(rs.next())
    {
      String grouping_value_strings = new String();
//      Vector<String> grouping_values = new Vector<String>();
      for(int i = 0; i<query.head.args.size(); i++)
      {
        if(i >= 1)
          grouping_value_strings = Head_strs.concatenate_strings(stringbuilder, grouping_value_strings, rs.getString(i+1));
        else
          grouping_value_strings = rs.getString(i+1);
        
//        grouping_values.add(rs.getString(i+1));
      }
      
//      Head_strs grouping_value_strings = new Head_strs(grouping_values);
      
      int start_pos = query.head.args.size();

//      Array array = rs.getArray(start_pos + 1);
//      
//      
//      String[][] prov_lists = (String[][]) array.getArray();
//      
//      group_value_prov_mappings.put(grouping_value_strings, prov_lists);

      
      if(query.head.has_agg)
      {
                
//        start_pos = query.head.args.size() + 1;
        
        Vector<String> agg_results = new Vector<String>(); 
        
        for(int i = start_pos; i<start_pos + query.head.agg_args.size(); i++)
        {
          agg_results.add(rs.getString(i+1));
        }
        
        Head_strs agg_res = new Head_strs(agg_results);
        
        query_instance.put(grouping_value_strings, agg_res);
      }
      else
      {
        query_instance.put(grouping_value_strings, null);
      }
    }
  }
  
  public static HashMap<String, String[]> construct_base_relation_sets(String sql, ArrayList<String> attr_ids, Set<String> prov_sets, Connection c, PreparedStatement pst) throws SQLException
  {
    pst = c.prepareStatement(sql);
    
//    pst.setFetchSize(10000);
    
    
    System.out.println(sql);
    
    double r1 = System.nanoTime(); 
    
    ResultSet rs = pst.executeQuery();
    
    double r2 = System.nanoTime();
    
    double query_time = (r2 - r1)*1.0/1000000000;
    
    System.out.println("query_time::" + query_time);
    
    HashMap<String, String[]> base_relation_content = new HashMap<String, String[]>();
    
    double t1 = System.nanoTime();
    
    double t = 0.0;
    
    while(rs.next())
    {
      String prov = rs.getString(attr_ids.size() + 1);
      
      double t11 = System.nanoTime();
      
      if(prov_sets.contains(prov))
      {
        double t22 = System.nanoTime();
        
        t += t22 - t11;
        
        String[] tuple = new String[attr_ids.size()];
        
        for(int i = 0; i<tuple.length; i++)
        {
          tuple[i] = rs.getString(i + 1);
        }
        
        base_relation_content.put(prov, tuple);
      }
      else
      {
        double t22 = System.nanoTime();
        
        t += t22 - t11;
      }
    }
    
    double t2 = System.nanoTime();
    
    double time = (t2 - t1)*1.0/1000000000;
    
    t = t*1.0/1000000000;
    
    System.out.println("find_time::" + t);
    
    System.out.println("time::" + time);
    
    return base_relation_content;
  }
  
  public static HashMap<String, String[]> construct_base_relation_sets(String sql, ArrayList<String> attr_ids, Connection c, PreparedStatement pst) throws SQLException
  {
    pst = c.prepareStatement(sql);
    
//    pst.setFetchSize(10000);
    
    
    System.out.println(sql);
    
    ResultSet rs = pst.executeQuery();
    
    HashMap<String, String[]> base_relation_content = new HashMap<String, String[]>();
    
    while(rs.next())
    {
      String prov = rs.getString(attr_ids.size() + 1);
      
      
        String[] tuple = new String[attr_ids.size()];
        
        for(int i = 0; i<tuple.length; i++)
        {
          tuple[i] = rs.getString(i + 1);
        }
        
        base_relation_content.put(prov, tuple);
    }
    
    return base_relation_content;
  }
  
  static int construct_relation_attr_maps(Single_view view, Vector<String> subgoals1, Vector<Argument> args1, HashMap<String, HashMap<String, Integer>> relation_attr_index, HashMap<String, ArrayList<String>> relation_attrs_list)
  {
//    for(int i = 0; i<args1.size(); i++)
    
    Argument arg = args1.get(0);
    
    String subgoal1 = subgoals1.get(0);
      
    String relation_name = view.subgoal_name_mappings.get(subgoal1);
      
      String attr_name = arg.attribute_name;
      
      HashMap<String, Integer> attr_index = relation_attr_index.get(relation_name);
      
      if(attr_index == null)
      {
        attr_index = new HashMap<String, Integer>();
        
        ArrayList<String> attr_list = new ArrayList<String>();
        
        attr_index.put(attr_name, 0);
        
        relation_attr_index.put(relation_name, attr_index);
        
        attr_list.add(attr_name);
        
        relation_attrs_list.put(relation_name, attr_list);
        
        return 0;
      }
      else
      {
        if(attr_index.get(attr_name) == null)
        {
          int size = attr_index.size();
          
          attr_index.put(attr_name, size);
          
          relation_attrs_list.get(relation_name).add(attr_name);
          
          return size;
        }
        else
          return attr_index.get(attr_name);
        
      }
      
  }
  
  public static HashMap<String, ArrayList<String>> get_necessary_attributes(Vector<Single_view> views, HashSet<String> relations)
  {
    HashMap<String, ArrayList<String>> relation_attr_list = new HashMap<String, ArrayList<String>>();
    
    HashMap<String, HashMap<String, Integer>> relation_attr_mappings = new HashMap<String, HashMap<String, Integer>>();
    
    for(Single_view view:views)
    {
      if(!view.head.has_agg)
      {
        for(Conditions condition: view.conditions)
        {
          Vector<String> subgoals1 = condition.subgoal1;
          
          Vector<String> subgoals2 = condition.subgoal2;
          
          int[] ids = new int[2];
          
          ids[0] = -1;
          
          ids[1] = -1;
          
          if(subgoals1.size() == 1)
          {
            Vector<Argument> args1 = condition.arg1;
            
            ids[0] = construct_relation_attr_maps(view, subgoals1, args1, relation_attr_mappings, relation_attr_list);
            
          }
          
          if(subgoals2.size() == 1)
          {
            Vector<Argument> args1 = condition.arg2;
            
            ids[1] = construct_relation_attr_maps(view, subgoals2, args1, relation_attr_mappings, relation_attr_list);
            
          }
          
          view.condition_arg_ids.add(ids);
          
        }
      }
    }
    
    relations.addAll(relation_attr_list.keySet());
    
    return relation_attr_list;
  }
  
  
  public static HashMap<String, String[]>[] retrieve_base_relations(HashMap<String, ArrayList<String>> relation_attr_index, String db_name, HashMap<String, HashMap<String, RoaringBitmap>>[] query_prov_index, Query query, Connection c, PreparedStatement pst) throws SQLException, InterruptedException
  {
    
    
    HashMap<String, String[]>[] base_relation_content = new HashMap[query_prov_index.length];
    
//    Vector<Loading_base_relations> threads = new Vector<Loading_base_relations>();
    
    for(int i = 0; i<query_prov_index.length; i++)
    {
//      Loading_base_relations thread = new Loading_base_relations(db_name, query_prov_index[i], query, i);
//      
//      
//      thread.start();
//      
//      threads.add(thread);
      
      StringBuilder sb = new StringBuilder();
      
      Set<String> curr_query_prov_sets = query_prov_index[i].keySet();
      
      Subgoal subgoal = (Subgoal) query.body.get(i);
      
      String origin_name = query.subgoal_name_mapping.get(subgoal.name);
      
      String sql = Query_converter.construct_query_base_relations(relation_attr_index, sb, query.subgoal_name_mapping.get(subgoal.name));
      
      base_relation_content[i] = construct_base_relation_sets(sql, relation_attr_index.get(origin_name), curr_query_prov_sets, c, pst);
    }
    
//    for(int i = 0; i<threads.size(); i++)
//    {
//      threads.get(i).join();
//    }
//    
//    for(int i = 0; i<threads.size(); i++)
//    {
//      base_relation_content[i] = threads.get(i).retrieve_base_relation_content();
//    }
    
    return base_relation_content;
  }
  
  public static HashMap<String, HashMap<String, String[]>> retrieve_base_relations(HashMap<String, ArrayList<String>> relation_attr_index, String db_name, HashSet<String> relations, Connection c, PreparedStatement pst) throws SQLException
  {
    
    int i = 0;
    
    HashMap<String, HashMap<String, String[]>> base_relation_content = new HashMap<String, HashMap<String, String[]>>(); 
    
    for(String relation: relations)
    {
//      Loading_base_relations thread = new Loading_base_relations(db_name, query_prov_index[i], query, i);
//      
//      
//      thread.start();
//      
//      threads.add(thread);
      
      StringBuilder sb = new StringBuilder();
      
      String sql = Query_converter.construct_query_base_relations(relation_attr_index, sb, relation);
      
      base_relation_content.put(relation, construct_base_relation_sets(sql, relation_attr_index.get(relation), c, pst));
      
      i++;
    }
    
    
    return base_relation_content;
  }
  
//  public static HashMap<String, String[]>[] retrieve_base_relations_multi_thread(HashMap<String, ArrayList<String>> relation_attr_mappings, String db_name, HashMap<String, HashMap<String, RoaringBitmap>>[] query_prov_index, Query query, Connection c, PreparedStatement pst) throws SQLException, InterruptedException
//  {
//    StringBuilder sb = new StringBuilder();
//    
//    HashMap<String, String[]>[] base_relation_content = new HashMap[query_prov_index.length];
//    
//    Vector<Loading_base_relations> threads = new Vector<Loading_base_relations>();
//    
//    for(int i = 0; i<query_prov_index.length; i++)
//    {
//      Loading_base_relations thread = new Loading_base_relations(relation_attr_mappings, db_name, query_prov_index[i].keySet(), query, i);
//      
//      
//      thread.start();
//      
//      threads.add(thread);
//      
////      Set<String> curr_query_prov_sets = query_prov_index[i].keySet();
////      
////      Subgoal subgoal = (Subgoal) query.body.get(i);
////      
////      String sql = Query_converter.construct_query_base_relations(sb, curr_query_prov_sets, subgoal, query.subgoal_name_mapping.get(subgoal.name));
////      
////      base_relation_content[i] = construct_base_relation_sets(sql, subgoal, curr_query_prov_sets, c, pst);
//    }
//    
//    for(int i = 0; i<threads.size(); i++)
//    {
//      threads.get(i).join();
//    }
//    
//    for(int i = 0; i<threads.size(); i++)
//    {
//      base_relation_content[i] = threads.get(i).retrieve_base_relation_content();
//    }
//    
//    return base_relation_content;
//  }
//  
//  public static HashMap<String, String[]>[] retrieve_base_relations_multi_thread(HashMap<String, ArrayList<String>> relation_attr_mappings,String db_name, HashSet<String>[] query_prov_index, Query query, Connection c, PreparedStatement pst) throws SQLException, InterruptedException
//  {
//    StringBuilder sb = new StringBuilder();
//    
//    HashMap<String, String[]>[] base_relation_content = new HashMap[query_prov_index.length];
//    
//    Vector<Loading_base_relations> threads = new Vector<Loading_base_relations>();
//    
//    for(int i = 0; i<query_prov_index.length; i++)
//    {
//      Loading_base_relations thread = new Loading_base_relations(relation_attr_mappings, db_name, query_prov_index[i], query, i);
//      
//      
//      thread.start();
//      
//      threads.add(thread);
//      
////      Set<String> curr_query_prov_sets = query_prov_index[i].keySet();
////      
////      Subgoal subgoal = (Subgoal) query.body.get(i);
////      
////      String sql = Query_converter.construct_query_base_relations(sb, curr_query_prov_sets, subgoal, query.subgoal_name_mapping.get(subgoal.name));
////      
////      base_relation_content[i] = construct_base_relation_sets(sql, subgoal, curr_query_prov_sets, c, pst);
//    }
//    
//    for(int i = 0; i<threads.size(); i++)
//    {
//      threads.get(i).join();
//    }
//    
//    for(int i = 0; i<threads.size(); i++)
//    {
//      base_relation_content[i] = threads.get(i).retrieve_base_relation_content();
//    }
//    
//    return base_relation_content;
//  }
//  
}
