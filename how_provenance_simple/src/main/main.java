package main;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;
import edu.upenn.cis.citation.Corecover.Query;
import edu.upenn.cis.citation.citation_view1.Head_strs;
import edu.upenn.cis.citation.query.Query_provenance_2;
import main.Load_views_and_citation_queries;
import main.init;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;

//import edu.upenn.cis.citation.query.Query_provenance;

public class main {
  
  
  public static void main(String[] args) throws SQLException, ClassNotFoundException
  {
    String query_file = args[0];
    
    String db_name = args[1];
    
    String usr_name = args[2];
    
    
    String passwd = args[3];
    
    init.usr_name = usr_name;
    
    init.passwd = passwd;
    
    Properties props = new Properties();
    
    props.setProperty("prepareThreshold", "10");
    
    Class.forName("org.postgresql.Driver");
    Connection c = DriverManager
        .getConnection(init.db_url_prefix + db_name + "?user="+ init.usr_name + "&password="+init.passwd, props);
    HashMap<String, String[][]> grouping_values_prov_mappings = new HashMap<String, String[][]>();
    
    HashMap<String, Head_strs> query_instance = new HashMap<String, Head_strs>();
    
    PreparedStatement pst = null;
    
    Query query = Load_views_and_citation_queries.get_query_test_case(query_file, c, pst).get(0);
    
    
    ResultSet rs = Query_provenance_2.get_query_provenance(query, c, pst);
    
    Query_provenance_2.retrieve_query_instance_with_pure_provenance(rs, query, grouping_values_prov_mappings, query_instance);
    
    print_results(grouping_values_prov_mappings);
  }
  
  public static void print_results(HashMap<String, String[][]> grouping_values_prov_mappings)
  {
    Set<String> keys = grouping_values_prov_mappings.keySet();
    
    
    for(String key: keys)
    {
      String[][] values = grouping_values_prov_mappings.get(key);
      
      System.out.print(key + Head_strs.parser);
      
      for(int i = 0; i<values.length; i++)
      {
        System.out.print("&");
        
        for(int j = 0; j<values[0].length; j++)
        {
          if(j >= 1)
            System.out.print(Head_strs.parser);
          
          System.out.print(values[i][j]);
        }
        
        
        System.out.print("&");
      }
      
      System.out.println();
      
    }
    
    
    
  }
  
  
  
  

}
